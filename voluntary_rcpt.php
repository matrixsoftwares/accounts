<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();
$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
require_once 'daily_cash_entry.php';
$pg_link = 'Volun. Contri.';
$user_id = $_SESSION[USER_ID];

// create a unique hash for the transaction
$len = mt_rand(4, 10);
$form_id = '';
for ($i = 0; $i < $len; $i++) {
  $d = rand(1, 30) % 2;
  $form_id .= $d ? chr(rand(65, 90)) : chr(rand(48, 57));
}

$form_id = md5(time() . $form_id);

if (isset($_POST['print'])) {
  $FileNo = $_POST['FileNo'];
  $name = $_POST['name'];
  $amount = str_replace(',', '', $_POST['amount']);
  $type = $_POST['type'];
  $bank = $_POST['bankname'];
  $cheque = $_POST['cheque'];
  $chk_date = $_POST['cheque_date'];
  $result = $cls_receipt->add_receipt($FileNo, $name, $amount, $type, $bank, $cheque, $chk_date, $user_id, $form_id, $_POST['acct_heads']);
  header('Location: print_voluntary_receipt.php?fId=' . $FileNo . '&id=' . $result);
}
if (isset($_POST['add_receipt'])) {
  $FileNo = $_POST['FileNo'];
  $name = $_POST['name'];
  $amount = str_replace(',', '', $_POST['amount']);
  $type = $_POST['type'];
  $bank = $_POST['bankname'];
  $cheque = $_POST['cheque'];
  $chk_date = $_POST['cheque_date'];
  $result = $cls_receipt->add_receipt($FileNo, $name, $amount, $type, $bank, $cheque, $chk_date, $user_id, $form_id, $_POST['acct_heads']);
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Receipt has been added successfully.";
    header('location: voluntary_rcpt_book.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Try again";
    header('location: voluntary_rcpt_book.php');
    exit();
  }
}
$heads = $cls_receipt->get_account_heads();
$title = 'Add Receipt';
$active_page = 'account';

require_once 'includes/header.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Credit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">Acct. Heads</label>
              <div class="col-md-4">
                <select class="form-control" name="acct_heads" id="remarks">
                  <option value="0">--Select One--</option>
      <?php if ($heads) {
        foreach ($heads as $head) { ?>
                      <option value="<?php echo $head['id']; ?>"><?php echo $head['master_head_name'] . ' / ' . $head['head']; ?></option>
                    <?php }
                  } else { ?>
                    <option value="">No heads to show.</option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group" id="File">
              <label class="col-md-3 control-label"><?php echo THALI_ID; ?></label>
              <div class="col-md-4">
                <input type="text" name="FileNo" id="FileNo" class="form-control">
              </div>
              <div class="col-md-1" id="loader">

              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Name</label>
              <div class="col-md-4">
                <input type="text" name="name" id="name" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Amount</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="amount" id="amount">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Type</label>
              <div class="col-md-4">
                <input type="radio" id="radio" name="type" value="cash" checked>&nbsp;Cash&emsp;
                <input type="radio" id="radio" name="type" value="cheque">&nbsp;Cheque&emsp;
                <input type="radio" id="radio" name="type" value="neft">&nbsp;NEFT
              </div>
            </div>

            <div id="cheque_detail" style="display: none">
              <div></div>
              <div class="form-group" >
                <label class="control-label col-md-3">Bank Name</label>
                <div class="col-md-4">
                  <input type="text" name="bankname" id="bankname" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3" id="cheque_type">Cheque No.</label>
                <div class="col-md-4">
                  <input type="text" name="cheque" id="cheque" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Date</label>
                <div class="col-md-4">
                  <input type="date" name="cheque_date" class="form-control" id="dob" placeholder="Date of Birth">
                </div>
              </div>
            </div>
            <input type="submit" class="btn btn-success col-md-offset-3 validate" name="add_receipt" id="add_receipt" value="Add">
            <input type="submit" class="btn btn-primary validate" id="print" name="print" id="print" value="Print">
            <a href="voluntary_rcpt_book.php" class="btn btn-danger"> Cancel</a>
            <input type="hidden" id="exist">
          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          $('#FileNo').change(function() {
            var upper_case = $(this).val().toUpperCase();
            $(this).val(upper_case);
            var fId = $(this).val();
            $('#loader').html('<img src="asset/img/loader.gif">');
            $.ajax({
              url: "ajax.php",
              type: "POST",
              data: 'fId=' + fId + '&cmd=get_name',
              success: function(data)
              {
                $('#loader').text('');
                if (data != 'invalid') {
                  $('#exist').val('0');
                  $('#name').val(data);
                  $('#File').attr('class', 'form-group has-success');
                  $('#amount').focus();
                } else {
                  alert('Invalid thali ID');
                  $('#exist').val('1');
                  $('#File').attr('class', 'form-group has-error');
                  $('#FileNo').focus();
                  return false;
                }
              }
            });
          });
          $("#FileNo").keydown(function(e) {
            var exist = $('#exist').val();
            if (e.keyCode === 9) {
              if (exist === 1) {
                $('#FileNo').focus();
                return false;
              } else
                return true;
            }
          });

          $('.validate').click(function() {
            var rad = $("input:radio[name=type]").val();
            var error = '';
            var validate = true;
            var file = $('#FileNo').val();
            var name = $('#name').val();
            var ex = $('#exist').val();
            var amount = $('#amount').val();
            if ((file == '') && (name == ''))
            {
              error += 'Enter either FileNo or Name to proceed..\n';
              validate = false;
            }
            if (ex == '1')
            {
              error += 'Invalid thali ID\n';
              validate = false;
            }

            if (amount == '')
            {
              error += 'Enter Amount\n';
              validate = false;
            }

            if (validate == false)
            {
              alert(error);
              return validate;
            }

          });
          $("input:radio[name=type]").click(function() {
            var radio = $(this).val();
            switch (radio) {
              case 'cheque':
                $('#cheque_detail').show();
                $('#cheque_type').text('Cheque No.');
                break;
              case 'neft':
                $('#cheque_detail').show();
                $('#cheque_type').text('NEFT No.');
                break;
              default:
                $('#cheque_detail').hide();
            }
          });
          $(document).ready(function() {
            $('#FileNo').focus(function() {
              $(this).select();
            });
          });

        </script>


      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>