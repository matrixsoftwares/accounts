<?php
    session_start();
    if(isset($_SESSION['logged_in'])) unset ($_SESSION[$_SESSION['logged_in']]);
    if(isset($_SESSION[USER_ID])) unset($_SESSION[$_SESSION[USER_ID]]);
    session_destroy();
    header('location: login.php');
?>
