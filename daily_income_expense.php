<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();

if(isset($_POST['search'])) {
  $view = $_POST['view'];
  $fromDate = $_POST['from_date'];
  $date = (array) explode('-', $fromDate);
  $fromMon = $date[1];
  $fromDay = $date[2];
  $fromYear = $date[0];
  $toDate = $_POST['to_date'];
  $date = explode('-', $toDate);
  $toMon = $date[1];
  $toDay = $date[2];
  $toYear = $date[0];
} else {
  $fromDate = $toDate = date('Y-m-d');
  $fromMon = $toMon = date('m');
  $fromDay = $toDay = date('d');
  $fromYear = $toYear = date('Y');
  $view = 'Both';
}


$currentStartTime = mktime(0, 0, 0, $fromMon, $fromDay, $fromYear);
$currentEndTime = mktime(23, 59, 59, $toMon, $toDay, $toYear);
$ary_income = $ary_debit = $ary_bankIncome = $ary_bankExpense = array();

//Income
$hubRcpts = $cls_receipt->get_daily_receipt('fmb_receipt_hub', $fromDate, $toDate, FALSE, FALSE, 'date');
$volReceipt = $cls_receipt->get_daily_receipt('receipt', FALSE, FALSE, $currentStartTime, $currentEndTime);
$creditVoucher = $cls_receipt->get_daily_receipt('credit_voucher', FALSE, FALSE, $currentStartTime, $currentEndTime);

$hubBankRcpts = $cls_receipt->get_daily_bank_receipt('fmb_receipt_hub', $fromDate, $toDate);
$volBankRcpts = $cls_receipt->get_daily_bank_receipt('receipt', $fromDate, $toDate);
$creditBank = $cls_receipt->get_daily_bank_receipt('credit_voucher', $fromDate, $toDate);

$debitBank = $cls_receipt->get_daily_bank_receipt('debit_voucher', $fromDate, $toDate);
$bankBills = $cls_receipt->get_daily_bank_receipt('account_bill', $fromDate, $toDate);
$bankDirectBills = $cls_receipt->get_daily_bank_receipt('direct_purchase', $fromDate, $toDate);

//Expense
$debitVoucher = $cls_receipt->get_daily_receipt('debit_voucher', $fromDate, $toDate, FALSE, FALSE, 'cheque_clear_date');
$bills = $cls_receipt->get_daily_receipt('account_bill', $fromDate, $toDate, FALSE, FALSE, 'cheque_clear_date');
$directBills = $cls_receipt->get_daily_receipt('direct_purchase', FALSE, FALSE, $currentStartTime, $currentEndTime);

$opening_data = $cls_receipt->get_balance_by_date($fromDate);
$closing_data = $cls_receipt->get_balance_by_date($toDate);

$title = 'Daily Income and Expense Report';
$active_page = 'account';

function no_results_found($label) {
  $div = '<div class="panel-body">No results found.</div>';
  panel($label, $div);
}

function panel($tbl_name, $table) {
  $panel = <<<PANEL
          <div class="panel panel-info">
            <div class="panel-heading">{$tbl_name}</div>
            $table
          </div>
PANEL;
  echo $panel;
}

function create_tbody($array, $id_link, $panelTitle, $slug) {
  global $ary_income, $ary_debit, $ary_bankIncome, $ary_bankExpense;
  $start = <<<START
          <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr>
                <th>ReceiptID</th>
                <th>FileNo</th>
                <th>Name</th>
                <th class='text-right'>Amount</th>
              </tr>
            </thead>
          <tbody>
START;
  $tr = '';
  $rcpt_id = $total = 0;
  foreach ($array as $rcpt) {
    $fileNo = isset($rcpt['FileNo']) ? $rcpt['FileNo'] : '--';
    $name = isset($rcpt['name']) ? $rcpt['name'] : $rcpt['hubber_name'];
    $name = ucwords(strtolower($name));
    $amount = number_format($rcpt['amount'], 2);
    $rcpt_id += 1;
    $total += $rcpt['amount'];
    $tr .= <<<TABLE
          <tr>
            <td>{$rcpt_id}</td>
            <td>{$fileNo}</td>
            <td>{$name}</td>
            <td class='text-right'>{$amount}</td>
          </tr>
TABLE;
  }

  switch ($slug) {
    case 'cash_income': $ary_income[] = $total;
      break;
    case 'cash_expense': $ary_debit[] = $total;
      break;
    case 'bank_income': $ary_bankIncome[] = $total;
      break;
    case 'bank_expense': $ary_bankExpense[] = $total;
      break;
  }
  $total = number_format($total, 2);
  $totalTR = <<<TR
          <tr>
            <td colspan='3' class='text-right'><strong>Total:</strong></td>
            <td class='text-right'>{$total}</td>
          </tr>
TR;
  $end = "</tbody></table>";
  $table = $start . $tr . $totalTR . $end;
  panel($panelTitle, $table);
}

require_once 'includes/header.php';

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" class="form-horizontal">
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-2">
                <input type="date" name="from_date" class="form-control" value="<?php echo $fromDate; ?>" placeholder="From Date">
              </div>

              <label class="col-md-1 control-label">To</label>
              <div class="col-md-2">
                  <input type="date" name="to_date" class="form-control" id="to_date" value="<?php echo $toDate; ?>" placeholder="To Date">
              </div>

              <label class="col-md-1 control-label">View</label>
              <div class="col-md-2">
                <select class="form-control" name="view">
                  <option value="Both" <?php echo ($view == 'Both') ? 'selected' : ''; ?>>Both</option>
                  <option value="income" <?php echo ($view == 'income') ? 'selected' : ''; ?>>Only Income</option>
                  <option value="expense" <?php echo ($view == 'expense') ? 'selected' : ''; ?>>Only Expense</option>
                </select>
              </div>

              <input type="submit" class="btn btn-success" name="search" id="search" value="Search">
              <a href="print_daily_income_expense.php?from_date=<?php echo $fromDate; ?>&to_date=<?php echo $toDate; ?>&view=<?php echo $view; ?>" target="_blank" class="btn btn-primary">Print</a>
            </div>
          </form>

          <?php if($view == 'Both' || $view == 'income') { ?>
          <h1>Cash Income</h1>
          <?php
          $panelTitle = 'Hub Receipt Book';
          $slug = 'cash_income';
          $hubRcpts ? create_tbody($hubRcpts, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);
          $panelTitle = 'Voluntary Receipt Book';
          $slug = 'cash_income';
          $volReceipt ? create_tbody($volReceipt, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);

          $panelTitle = 'Credit Voucher';
          $slug = 'cash_income';
          $creditVoucher ? create_tbody($creditVoucher, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);
          ?>
          <?php }
          if($view == 'Both' || $view == 'expense') {
          ?>
          <?php
          echo '<h1>Cash Expense</h1>';
          $panelTitle = 'Debit Voucher';
          $slug = 'cash_expense';
          $debitVoucher ? create_tbody($debitVoucher, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);

          $panelTitle = 'Bill Payments';
          $slug = 'cash_expense';
          $bills ? create_tbody($bills, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);

          $panelTitle = 'Direct Purchase Bills';
          $slug = 'cash_expense';
          $directBills ? create_tbody($directBills, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);
          ?>
          <?php }
          if($view == 'Both' || $view == 'income') {
            ?>
          <h1>Bank Income</h1>
          <?php
          $panelTitle = 'Hub Receipt Book';
          $slug = 'bank_income';
          $hubBankRcpts ? create_tbody($hubBankRcpts, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);
          $panelTitle = 'Voluntary Receipt Book';
          $slug = 'bank_income';
          $volBankRcpts ? create_tbody($volBankRcpts, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);

          $panelTitle = 'Credit Voucher';
          $slug = 'bank_income';
          $creditBank ? create_tbody($creditBank, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);
          ?>
          <?php }
          if($view == 'Both' || $view == 'expense') {
            ?>
          <?php
          echo '<h1>Bank Expense</h1>';
          $panelTitle = 'Debit Voucher';
          $slug = 'bank_expense';
          $debitBank ? create_tbody($debitBank, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);

          $panelTitle = 'Bill Payments';
          $slug = 'bank_expense';
          $bankBills ? create_tbody($bankBills, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);

          $panelTitle = 'Direct Purchase Bills';
          $slug = 'bank_expense';
          $bankDirectBills ? create_tbody($bankDirectBills, FALSE, $panelTitle, $slug) : no_results_found($panelTitle);
          ?>
          <?php } ?>

          <div class="col-md-12">
            <div class="col-md-3 alert-info row">
              <p class="form-control-static text-center"><strong>Opening Cash Balance:</strong> <?php echo number_format($opening_data[0]['opening_cash'], 2); ?></p>
            </div>
            <div class="col-md-3 alert-info row">
              <p class="form-control-static text-center"><strong>Total Cash Income:</strong> <?php echo number_format(array_sum($ary_income), 2); ?></p>
            </div>
            <div class="col-md-3 alert-info row">
              <p class="form-control-static text-center"><strong>Total Cash Expense:</strong> <?php echo number_format(array_sum($ary_debit), 2); ?></p>
            </div>
            <div class="col-md-3 alert-info row">
              <p class="form-control-static text-center"><strong>Total Cash Balance:</strong> <?php echo number_format((($opening_data[0]['opening_cash'] + array_sum($ary_income)) - array_sum($ary_debit)), 2); ?></p>
            </div>
          </div>
          <div class="col-md-12">&nbsp;</div>
          <div class="clearfix"></div>

          <div class="col-md-12">
            <div class="col-md-3 alert-info row">
              <p class="form-control-static text-center"><strong>Opening Bank Balance:</strong> <?php echo number_format($opening_data[0]['opening_bank'], 2); ?></p>
            </div>
            <div class="col-md-3 alert-info row">
              <p class="form-control-static text-center"><strong>Total Bank Income:</strong> <?php echo number_format(array_sum($ary_bankIncome), 2); ?></p>
            </div>
            <div class="col-md-3 alert-info row">
              <p class="form-control-static text-center"><strong>Total Bank Expense:</strong> <?php echo number_format(array_sum($ary_bankExpense), 2); ?></p>
            </div>
            <div class="col-md-3 alert-info row">
              <p class="form-control-static text-center"><strong>Total Bank Balance:</strong> <?php echo number_format((($opening_data[0]['opening_bank'] + array_sum($ary_bankIncome)) - array_sum($ary_bankExpense)), 2); ?></p>
            </div>
          </div>

          <style>
            .row {
              padding: 4px;
              border-radius: 8px;
            }
          </style>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>