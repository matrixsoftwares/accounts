<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.billbook.php");
$cls_billbook = new Mtx_BillBook();

$shop_id = (isset($_GET['sid'])) ? $_GET['sid'] : FALSE;
if (isset($_POST['Add_shop'])) {
  $ary_post = post_values();

  $exist = $cls_billbook->shop_exist($ary_post[0]);
  if ($exist == 0) {
    $result = $cls_billbook->add_shop_name($ary_post);
    if ($result)
      $_SESSION[SUCCESS_MESSAGE] = 'New shop added successfully.';
    else
      $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing the request...';
  } else {
    $_SESSION[ERROR_MESSAGE] = "Duplicate entry found for shop name " . $ary_post[0];
  }
}

if (isset($_POST['Update_shop'])) {
  $ary_post = post_values();
  $update = $cls_billbook->update_shop($ary_post, $shop_id);
  if ($update)
    $_SESSION[SUCCESS_MESSAGE] = 'Shop Updated successfully.';
  else
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing the request...';
  window_location();
}

if (isset($_GET['cmd'])) {
  $delete = $cls_billbook->delete_shop($shop_id);
  if ($delete)
    $_SESSION[SUCCESS_MESSAGE] = 'Shop deleted successfully.';
  else
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing the request...';
  window_location();
}

function window_location() {
  header('Location: shop_entry.php');
  exit;
}

function post_values() {
  global $database;
  $post = $database->clean_data($_POST);
  $shop = ucwords(strtolower($post['shop_name']));
  $contact_person = ucfirst(strtolower($post['person']));
  $mob = $post['mobile'];
  $address = ucwords(strtolower($post['address']));
  return array($shop, $contact_person, $mob, $address);
}

if ($shop_id)
  $result = $cls_billbook->get_shops($shop_id);
$shops = $cls_billbook->get_shops();
$title = 'Add Company';
$active_page = 'settings';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-5">
            <form method="post" role="form" class="form-horizontal">
              <div></div>
              <div class="form-group">
                <label class="control-label col-md-3">Shop Name</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="shop_name" id="shop_name" value="<?php if ($shop_id) echo $result['ShopName']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Contact Person</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="person" id="person" value="<?php if ($shop_id) echo $result['ContactPerson']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Contact No#</label>
                <div class="col-md-7">
                  <input type="text" name="mobile" class="form-control" id="mobile" placeholder="" value="<?php if ($shop_id) echo ($result['ContactNo'] > 0) ? $result['ContactNo'] : FALSE;; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Address</label>
                <div class="col-md-7">
                  <textarea class="form-control" name="address" id="address" rows="5" placeholder="Enter address here"><?php if ($shop_id) echo $result['address']; ?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">&nbsp;</label>
                <div class="col-md-7">
                  <?php
                  $btn_name = ($shop_id) ? 'Update' : 'Add';
                  ?>
                  <button class="btn btn-success validate" type="submit" name="<?php echo $btn_name . '_shop'; ?>" id="add_shop"><?php echo $btn_name; ?></button>
                </div>
              </div>

            </form>
          </div>
          <?php if ($shops) { ?>
            <div class="col-md-7">
              <table class="table table-hover table-bordered table-condensed">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Shop Name</th>
                    <th>Contact Person</th>
                    <th>Contact No#</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i = 1;
                  foreach ($shops as $shop) {
                    ?>
                    <tr>
                      <td><a href="company_bills_details.php?company=<?php echo $shop['id']; ?>"><?php echo $i++; ?></a></td>
                      <td><a href="company_bills_details.php?company=<?php echo $shop['id']; ?>"><?php echo $shop['ShopName']; ?></a></td>
                      <td><?php echo $shop['ContactPerson']; ?></td>
                      <td><?php echo ($shop['ContactNo'] > 0) ? $shop['ContactNo'] : FALSE; ?></td>
                      <td><div class="btn-group">
                        <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="?sid=<?php echo $shop['id']; ?>">Update</a></li>
                          <li><a href="company_bills_details.php?company=<?php echo $shop['id']; ?>">View By Bill</a></li>
                          <li><a href="company_orders.php?company=<?php echo $shop['id']; ?>">View By Order</a></li>
                        </ul></div>
      <!--                                                                                                                                                           <a href="?sid=<?php echo $shop['id']; ?>" class="btn btn-success btn-xs">Update</a> | <a href="?sid=<?php echo $shop['id']; ?>&cmd=delete" id="delete" class="btn btn-danger btn-xs">Delete</a</td>-->
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          <?php } ?>
        </div>
        <!-- /Center Bar -->
        <script>
          $('.validate').click(function() {
            var shop_name = $('#shop_name').val();
            if (shop_name === '') {
              alert('Please enter shop name');
              return false;
            }
          });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>