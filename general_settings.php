<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.user.php");
require_once("classes/class.receipt.php");
$cls_user = new Mtx_User();
$cls_receipt = new Mtx_Receipt();
$id = 0;
$result = $cls_user->get_general_settings();
if($result) $id = $result[0]['id'];

if (isset($_POST['Save'])) {
  $data = $database->clean_data($_POST);
  $open = floatval($database->clean_currency($data['open_bal']));
  $lia = floatval($database->clean_currency($data['liabilities']));
  $email = $data['email'];
  $tanzeem = ucwords(strtolower($data['tanzeem']));
  $hub = floatval($database->clean_currency($data['hub']));
  $kisht = (int) $database->clean_currency($data['kisht']);
  $start_year = (int) $database->clean_currency($data['start_year']);
  $no_years = (int) $database->clean_currency($data['no_years']);
  $def_year = floatval($database->clean_currency($data['default_year']));
  $start_date = $database->clean_currency($data['start_date']);
  $end_date = $database->clean_currency($data['end_date']);
  $zabihat_inayat = $data['zabihat_inayat'];
  $mumineen_contri = $data['mumineen_contri'];
  $hub_pending_tpl = ucfirst(strtolower($data['hub_pending_tpl']));
  $senderId =  $data['senderID'];
  $msg_user_name = $data['msg_user_name'];
  $msg_password = $data['msg_password'];
  $masaar_sender_id = $data['masaar_sender_id'];
  $API_key = $data['API_key'];
  $route = (int) $data['route'];
  $country_code = (int) $data['country_code'];
  $default_sms_vendor = (int) $data['default_sms_vendor'];
  
  $result = $cls_user->add_general_settings($email, $tanzeem, $hub, $kisht, $start_year, $no_years, $def_year, $start_date, $end_date, $zabihat_inayat, $mumineen_contri, $open, $lia, $hub_pending_tpl, $senderId, $msg_user_name, $msg_password, $masaar_sender_id, $API_key, $route, $country_code, $default_sms_vendor);
  if ($result > 0) { 
    $id = (int) $result;
    $_SESSION[SUCCESS_MESSAGE] = 'General settings has been stored successfully.';
    header('Location: general_settings.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing...';
    header('Location: general_settings.php');
    exit();
  }
}

if (isset($_POST['Update'])) {
  $data = $database->clean_data($_POST);
  $open_cash = $database->clean_currency($data['open_cash']);
  $open_bank = $database->clean_currency($data['open_bank']);
  $lia_cash = $database->clean_currency($data['lia_cash']);
  $lia_bank = $database->clean_currency($data['lia_bank']);
  $email = $data['email'];
  $tanzeem = ucwords(strtolower($data['tanzeem']));
  $hub = $database->clean_currency($data['hub']);
  $kisht = $database->clean_currency($data['kisht']);
  $start_year = $database->clean_currency($data['start_year']);
  $no_years = $database->clean_currency($data['no_years']);
  $def_year = $database->clean_currency($data['default_year']);
  $start_date = $database->clean_currency($data['start_date']);
  $end_date = $database->clean_currency($data['end_date']);
  $zabihat_inayat = $data['zabihat_inayat'];
  $mumineen_contri = $data['mumineen_contri'];
  $hub_pending_tpl = ucfirst(strtolower($data['hub_pending_tpl']));
  $senderId = $data['senderID'];
  $msg_user_name = $data['msg_user_name'];
  $msg_password = $data['msg_password'];
  $masaar_sender_id = $data['masaar_sender_id'];
  $API_key = $data['API_key'];
  $route = $data['route'];
  $country_code = $data['country_code'];
  $default_sms_vendor = $data['default_sms_vendor'];
  
  $result = $cls_user->update_general_settings($id, $email, $tanzeem, $hub, $kisht, $start_year, $no_years, $def_year, $start_date, $end_date, $zabihat_inayat, $mumineen_contri, $open_cash, $open_bank, $lia_cash, $lia_bank, $hub_pending_tpl, $senderId, $msg_user_name, $msg_password, $masaar_sender_id, $API_key, $route, $country_code, $default_sms_vendor);
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'General settings has been updated successfully.';
    header('Location: general_settings.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing...';
    header('Location: general_settings.php');
    exit();
  }
}

$heads = $cls_receipt->get_account_heads();
$open_cash = $cls_receipt->get_balance('opening_cash');
$open_bank = $cls_receipt->get_balance('opening_bank');
$lia_cash = $cls_receipt->get_balance('lia_cash');
$lia_bank = $cls_receipt->get_balance('lia_bank');

$title = 'General Settings';
$active_page = 'settings';
include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">

          <form method="post" role="form" class="form-horizontal">
            <div class="col-md-6">
              <h3 class="text-center">General Settings</h3>
              <div class="form-group">
                <label class="control-label col-md-3">Opening Cash</label>
                <div class="col-md-5">
                  <input type="text" class="form-control" name="open_cash" id="open_cash"  value="<?php echo number_format($open_cash, 2); ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Opening Bank</label>
                <div class="col-md-5">
                  <input type="text" class="form-control" name="open_bank" id="open_bank"  value="<?php echo number_format($open_bank, 2); ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Liabilities Cash</label>
                <div class="col-md-5">
                  <input type="text" class="form-control" name="lia_cash" id="email"  value="<?php echo number_format($lia_cash, 2); ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Liabilities Bank</label>
                <div class="col-md-5">
                  <input type="text" class="form-control" name="lia_bank" id="email"  value="<?php echo number_format($lia_bank, 2); ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Back Up Email</label>
                <div class="col-md-5">
                  <input type="text" class="form-control" name="email" id="email" value="<?php echo $result[0]['back_up_email']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Tanzeem Name</label>
                <div class="col-md-5">
                  <input type="text" class="form-control" name="tanzeem" id="tanzeem" value="<?php echo $result[0]['tanzeem_name']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Niyaz Hub</label>
                <div class="col-md-5">
                  <input type="text" name="hub" class="form-control" id="hub" value="<?php echo number_format($result[0]['avg_niyaz_hub'], 2); ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Kisht</label>
                <div class="col-md-5">
                  <input type="text" name="kisht" class="form-control" id="kisht"  value="<?php echo $result[0]['default_kisht']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Start Year</label>
                <div class="col-md-5">
                  <input type="text" name="start_year" class="form-control" id="start_year"  value="<?php echo $result[0]['start_year']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">No. of Years</label>
                <div class="col-md-5">
                  <input type="text" name="no_years" class="form-control" id="no_years"  value="<?php echo $result[0]['no_of_years']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Default Year</label>
                <div class="col-md-5">
                  <select class="form-control get_record" name="default_year">
                    <option value ="">-- Select One --</option>
                    <?php
                      for($i = 1; $i <= $no_years; $i++) {
                      $year = (($i == 1) ? $default_year : ($default_year + ($i - 1))) . '-' . (substr($default_year, -2) + $i);
                    ?>
                    <option value="<?php echo $year;?>" <?php if($result[0]['default_year'] == $year){ echo 'selected'; } ?>><?php echo $year; ?></option>
                  <?php } ?>

                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Balance Sheet Start Date</label>
                <div class="col-md-5">
                  <input type="date" name="start_date" class="form-control" id="default_date" value="<?php echo $result[0]['start_date']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Balance Sheet End Date</label>
                <div class="col-md-5">
                  <input type="date" name="end_date" class="form-control" id="default_date" value="<?php echo $result[0]['end_date']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Zabihat Inayat</label>
                <div class="col-md-5">
                  <select class="form-control" name="zabihat_inayat" id="remarks">
                    <option value="0">--Select One--</option>
                    <?php if ($heads) {
                      foreach ($heads as $head) { ?>
                        <option value="<?php echo $head['id']; ?>" <?php if($result[0]['zabihat_inayat'] == $head['id']){ echo 'selected'; } ?>><?php echo $head['master_head_name'] . ' / ' . $head['head']; ?></option>
                      <?php }
                    } else { ?>
                      <option value="">No heads to show.</option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Mumineen Contri.</label>
                <div class="col-md-5">
                  <select class="form-control" name="mumineen_contri" id="remarks">
                    <option value="0">--Select One--</option>
                    <?php if ($heads) {
                      foreach ($heads as $head) { ?>
                        <option value="<?php echo $head['id']; ?>" <?php if($result[0]['mumineen_contri'] == $head['id']){ echo 'selected'; } ?>><?php echo $head['master_head_name'] . ' / ' . $head['head']; ?></option>
                      <?php }
                    } else { ?>
                      <option value="">No heads to show.</option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            
            <div class="col-md-6">
              <h3 class="text-center">SMS API Settings</h3>
              
              <div class="form-group">
                <label class="control-label col-md-4">Default SMS Vendor</label>
                <div class="col-md-5">
                  <select class="form-control get_record" name="default_sms_vendor">
                    <option value ="">-- Select One --</option>
                      <option value="1" <?php if($result[0]['default_sms_vendor'] == '1'){ echo 'selected'; } ?>>Computek</option>
                      <option value="2" <?php if($result[0]['default_sms_vendor'] == '2'){ echo 'selected'; } ?>>Al Masaar</option>
                  </select>
                </div>
              </div>

              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#computek" aria-controls="computek" role="tab" data-toggle="tab">Computek</a></li>
                <li role="presentation"><a href="#almasaar" aria-controls="almasaar" role="tab" data-toggle="tab">Al Masaar</a></li>
              </ul>

              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="computek">
                  <br>
                  <div class="form-group">
                    <label class="control-label col-md-4">Sender ID</label>
                    <div class="col-md-5">
                      <input type="text" name="senderID" class="form-control" id="senderID" value="<?php echo $result[0]['sender_id']; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-4">User Name</label>
                    <div class="col-md-5">
                      <input type="text" name="msg_user_name" class="form-control" id="msg_user_name" value="<?php echo $result[0]['user_name']; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-4">Password</label>
                    <div class="col-md-5">
                      <input type="text" name="msg_password" class="form-control" id="msg_password" value="<?php echo $result[0]['password']; ?>">
                    </div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="almasaar">
                  <br>
                  <div class="form-group">
                    <label class="control-label col-md-4">Sender ID</label>
                    <div class="col-md-5">
                      <input type="text" name="masaar_sender_id" class="form-control" id="masaar_sender_id" value="<?php echo $result[0]['masaar_sender_id']; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-4">API Key</label>
                    <div class="col-md-5">
                      <input type="text" name="API_key" class="form-control" id="API_key" value="<?php echo $result[0]['API_key']; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-4">Route</label>
                    <div class="col-md-5">
                      <input type="text" name="route" class="form-control" id="route" value="<?php echo $result[0]['route']; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-4">Country Code</label>
                    <div class="col-md-5">
                      <input type="text" name="country_code" class="form-control get_record" id="country_code" value="<?php echo $result[0]['country_code']; ?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group"><br>
                <label class="control-label col-md-4">Hub Pending Message</label>
                <div class="col-md-5">
                  <textarea name="hub_pending_tpl" class="form-control"><?php echo $result[0]['hub_pending_tpl']; ?></textarea>
                </div>
              </div>
          </div>
          <div class="clearfix"></div>
          <div class="col-md-10 col-md-offset-2">
            <?php
    $btn_name = ($id == 0) ? 'Save' : 'Update';
    ?>
            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-4">
                <button class="btn btn-success" type="submit" name="<?php echo $btn_name; ?>" id="save"><?php echo $btn_name; ?></button>
              </div>
            </div>
          </div>
        </form>

        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
<style>
  .tab-content {
    border: 1px solid #ddd;
  }
</style>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>