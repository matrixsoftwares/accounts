<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.billbook.php");
$cls_billbook = new Mtx_BillBook();

$from_date = $to_date = $companyID = FALSE;
if ($_GET) {
  $data = $database->clean_data($_GET);
  $companyID = $data['company'];
  if (isset($data['from_date']) && isset($data['to_date'])) {
  $from_date = $data['from_date'];
  $to_date = $data['to_date'];
  $date = explode('-', $from_date);
  $from_ts = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
  $date = explode('-', $to_date);
  $to_ts = mktime(0, 0, 0, $date[1], $date[2], $date[0]);

  $query = "SELECT  SUM(`quantity`) quantity, (SELECT `name` FROM `inventory` i WHERE i.`id` LIKE bd.item_name) Item, (IFNULL(SUM(`unit`), 0) / count(*)) AVERAGE_UNIT_PRICE FROM `bill_details` bd WHERE `bill_id` IN (SELECT `id`  FROM `account_bill` ab WHERE `name` LIKE '$companyID' AND `timestamp` BETWEEN '$from_ts' AND '$to_ts' AND `cancel` = '0') GROUP BY item_name";
  $accountBills = $database->query_fetch_full_result($query);

  $query = "SELECT SUM(`quantity`) quantity, item_name as Item, (IFNULL(SUM(`unit`), 0) / count(*)) AVERAGE_UNIT_PRICE FROM `direct_bill_details` WHERE `bill_id` IN (SELECT `id` FROM `direct_purchase` dp WHERE `name` LIKE '$companyID' AND `timestamp` BETWEEN '$from_ts' AND '$to_ts' AND `cancel` = '0') GROUP BY item_name";
  $directBills = $database->query_fetch_full_result($query);
  }
}
$shops = $cls_billbook->get_shops();
$title = 'Company Orders';
$active_page = 'settings';

require_once 'includes/header.php';

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="get" role="form" class="form-horizontal">
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-2">
                <input type="date" name="from_date" class="form-control" id="from_date" value="<?php echo $from_date; ?>" placeholder="From Date">
              </div>
              <label class="col-md-1 control-label">To</label>
              <div class="col-md-2">
                <input type="date" name="to_date" class="form-control" id="to_date" value="<?php echo $to_date; ?>" placeholder="To Date">
              </div>

              <label class="col-md-1 control-label">Company</label>
              <div class="col-md-3">
                <select class="form-control" name="company">
                  <option value="">--Select Company--</option>
                  <?php foreach ($shops as $shop) {
                    $selected = ($companyID == $shop['id']) ? 'selected' : '';
                    ?>
                    <option value="<?php echo $shop['id']; ?>" <?php echo $selected; ?>><?php echo $shop['ShopName']; ?></option>
                  <?php } ?>
                </select>
              </div>
              <input type="submit" class="btn btn-success" name="" value="Search">
            </div>
          </form>
          <?php if($from_date && $to_date && $companyID) { ?>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th colspan="4">Account Bills</th>
                </tr>
                <tr>
                  <th>No.</th>
                  <th>Item Name</th>
                  <th>Quantity</th>
                  <th class="text-right">Average Unit Price</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                if ($accountBills) {
                  foreach ($accountBills as $a) {
                    ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $a['Item']; ?></td>
                      <td><?php echo $a['quantity']; ?></td>
                      <td class="text-right"><?php echo number_format($a['AVERAGE_UNIT_PRICE'], 2); ?></td>
                    </tr>
                  <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="4" class="alert-danger">No results found.</td>
                  </tr>
      <?php } ?>
              </tbody>
              </tbody>
            </table>
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th colspan="4">Direct Purchase Bills</th>
                </tr>
                <tr>
                  <th>No.</th>
                  <th>Item Name</th>
                  <th>Quantity</th>
                  <th class="text-right">Average Unit Price</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                if ($directBills) {
                  foreach ($directBills as $d) {
                    ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $d['Item']; ?></td>
                      <td><?php echo $d['quantity']; ?></td>
                      <td class="text-right"><?php echo number_format($d['AVERAGE_UNIT_PRICE'], 2); ?></td>
                    </tr>
                  <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="4" class="alert-danger">No results found.</td>
                  </tr>
      <?php } ?>
              </tbody>
              </tbody>
            </table>
          </div>
          <?php } ?>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>