<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/class.user.php');
require_once('includes/inc.num2words.php');

$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();
$cls_user = new Mtx_User();

$debit = $cls_receipt->get_voucher($_GET['vid'], 'debit_voucher');
$setting = $cls_user->get_general_settings();
$db_tanzeem_name = $setting[0]['tanzeem_name'];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print Debit Voucher</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
    <style type="text/css">
      @media all {
        body{font-size: 14px; }
        .page-break  { display: none; }
      }

      @media print {
        .page-break  { display: block; page-break-before: always; }
      }
    </style>
  </head>
  <body onload="window.print();">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
        <div style="text-align: center;" class="col-md-12">
          <img src="includes/logo.jpg" width="200px">
        </div>
        <div class="col-md-12" style="text-align: center;">
          <h2><strong><?php echo $db_tanzeem_name; ?></strong></h2>
        </div>


        <div class="col-md-12">
          <span class="pull-left col-md-4"><strong>Date</strong> : <u><?php echo date('d/m/Y', $debit['timestamp']); ?></u></span>
          <span class="pull-right col-md-4"><strong>Receipt No.:</strong> <u><?php echo $debit['id']; ?></u></span>
        </div>

        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-4"><strong>Amount</strong> : <u><?php echo number_format($debit['amount'], 2, '.', ','); ?></u></span>
          <span class="col-md-4 pull-right"><strong>Account Head</strong> :<u><?php echo ucfirst($debit['acct_heads']); ?></u></span>
        </div>

        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="col-md-8"><strong>Reference Bill Id</strong> : <u><?php $debit['ref_bill_id']; ?></u></span>
        </div>

        <div class="col-md-12">&nbsp;</div>

        <!--div class="col-md-12">
          <span class="pull-left col-md-7">
            <strong>Yearly Takhmeen</strong> : <u><?php //echo number_format($yearly_takhmeen, 2, '.', ',');?></u>
          </span>
          <span class="pull-right col-md-4">
            <strong>Pending Amount</strong> : <u><?php //echo number_format($pending, 2, '.', ',');?></u>
          </span>
        </div-->

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <?php if($debit['payment_type'] == '' || $debit['payment_type'] == 'cash') $type = 'CASH';?>
        <?php if($debit['payment_type'] == 'cheque') $type = 'CHEQUE';?>
        <?php if($debit['payment_type'] == 'neft') $type = 'NEFT';?>
        <?php
switch ($debit['payment_type']) {
        case 'cheque': $card = 'Cheque No.'; break;
        case 'neft': $card = 'Neft Code'; break;
        default : $card = ''; break;
      }        ?>
        <div class="col-md-12" style="margin-left: 12px;">
          <span class="">Paid By <u><strong><?php echo ucfirst(strtolower($type)); ?></strong></u>, <?php if($type == 'CHEQUE' || $type == 'NEFT') { ?><?php echo $card; ?>:&nbsp;<?php echo '<u><strong>'.$debit['cheque_no'].'</strong></u>'; ?>, Bank: <?php echo '<u><strong>'.ucwords($debit['bank_name']).'</strong></u>'; ?><?php } ?> To <?php echo '<u><strong>'.$debit['name'].'</strong></u>'; ?> Rs. <strong><u><?php echo num2words($debit['amount']) . ' Only'; ?></u></strong></span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-12"><strong>Contact Person: </strong><?php echo $debit['contact_person']; ?></span>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12">
          <span class="pull-left col-md-4">Authorized By</span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-5">E & O.E.</span>
          <span class="pull-right col-md-4">ABDE SYEDNA(T.U.S)</span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <a href="index.php" class="btn btn-info pull-right no-print">Back to Home</a><br><br>
        </div>

        <div class="col-md-12">
          <a href="debit_voucher.php" class="btn btn-info pull-right no-print">Back to Voucher</a><br><br>
        </div>
      </div>

      <!-- /Center Bar -->

      <!-- Right Bar -->

      <!-- /Right Bar -->

    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
