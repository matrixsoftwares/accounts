<?php
include('session.php');
require_once('classes/class.database.php');

if (isset($_GET['item'])) {
  $item_id = $_GET['item'];
} else {
  header('location: index.php');
  exit();
}
$start_date = (isset($_GET['from_date'])) ? $_GET['from_date'] : '';
$end_date = (isset($_GET['to_date'])) ? $_GET['to_date'] : '';

if ($start_date && $end_date) {
  $query = "SELECT ab.id, 
(SELECT c.ShopName FROM companies c WHERE ab.name LIKE c.id) supplier, 
DATE(FROM_UNIXTIME(ab.timestamp)) bill_date, 
(SELECT i.name FROM inventory i WHERE bd.item_name = i.id) item, 
bd.unit unit_price, 
(SELECT i.unit FROM inventory i WHERE bd.item_name = i.id) unit
FROM 
account_bill ab 
LEFT JOIN bill_details bd ON ab.id = bd.bill_id
WHERE cancel = 0 
AND DATE(FROM_UNIXTIME(ab.timestamp)) BETWEEN '$date_from' AND '$date_to' 
AND bd.item_name LIKE '$item_id' 
ORDER BY ab.id DESC 
LIMIT 0,40";
} else {
  $query = "SELECT ab.id, 
(SELECT c.ShopName FROM companies c WHERE ab.name LIKE c.id) supplier, 
DATE(FROM_UNIXTIME(ab.timestamp)) bill_date, 
(SELECT i.name FROM inventory i WHERE bd.item_name = i.id) item, 
bd.unit unit_price, 
(SELECT i.unit FROM inventory i WHERE bd.item_name = i.id) unit
FROM 
account_bill ab 
LEFT JOIN bill_details bd ON ab.id = bd.bill_id
WHERE cancel = 0 
AND bd.item_name LIKE '$item_id' 
ORDER BY ab.id DESC 
LIMIT 0,40";
}

$history = $database->query_fetch_full_result($query);

$title = 'Item History';
$active_page = '';

if ($history) {
  $item = $history[0]['item'];
  $unit = $history[0]['unit'];
}

require_once 'includes/header.php';

$page_number = INVENTORY_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Inventory</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <h1>Latest Bills for <?php echo $item . ' per ' . $unit; ?></h1>
      <div class="row">
        <div class="col-md-8">
          <table class="table table-responsive  table-hover table-bordered">
            <thead>
              <tr>
                <th>Sr.</th>
                <th>Bill ID</th>
                <th>Supplier</th>
                <th>Bill Date</th>
                <th class="text-right">Unit Price</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $i = 0;
              foreach ($history as $bill) {
                $i++;
                ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><a href="upd_bill_details.php?cmd=show_bills&bid=<?php echo $bill['id']; ?>" target="_blank"><?php echo $bill['id']; ?></a></td>
                  <td><?php echo $bill['supplier']; ?></td>
                  <td><?php echo $bill['bill_date']; ?></td>
                  <td class="text-right"><?php echo $bill['unit_price']; ?></td>
                </tr>
                <?php
              }
              ?>
            </tbody>
          </table>  
        </div>
      </div>
    </section>
  </div>
