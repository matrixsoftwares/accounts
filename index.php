<?php
ob_start();
include('session.php');
require_once('classes/class.barcode.php');
require_once("classes/class.menu.php");

$title = 'Dashboard';
$active_page = 'dashboard';
include 'includes/header.php';

$cls_barcode = new Mtx_Barcode();
$cls_menu = new Mtx_Menu();
$date = date('Y-m-d');

//Thali Count
$total_family_nb = count_total_family();
$total_fly = $cls_family->get_total_family();
$total_issued_tiffin = $cls_barcode->get_issued_tiffin($date);

//Daily Menu
$date = strtotime($date);
$date2 = strtotime("+1 day", $date);
$get_today_menu = $cls_menu->get_single_daily_menu($date);
$get_tomorrow_menu = $cls_menu->get_single_daily_menu($date2);

//To-do List
$users = $cls_user->get_all_user();
$to_do_list = get_all_to_do_list($_SESSION[USERNAME], $page, 5);
$total_to_do_list = get_total_to_do_list($_SESSION[USERNAME]);

if (isset($_GET['cmd']) && $_GET['cmd'] == 'update') {
  $to_do_read = $_GET['status'];
  $to_do_id = $_GET['id'];

  $update = update_to_do_text($to_do_read, $to_do_id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Record updated successfully.';
    header('Location: index.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Sorry! try again.';
    header('Location: index.php');
    exit();
  }
}

if (isset($_GET['cmd']) && $_GET['cmd'] == 'delete') {
  $to_do_id = $_GET['id'];

  $update = delete_to_do_list($to_do_id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Record Deleted successfully.';
    header('Location: index.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Sorry! try again.';
    header('Location: index.php');
    exit();
  }
}

if (isset($_POST['submit'])) {
  $data = $database->clean_data($_POST);
  $to_do_creator = $data['to_do_creator'];
  $to_do_receiver = $data['to_do_receiver'];
  $to_do_text = $data['to_do_text'];
  $result = sent_to_do_text($to_do_creator, $to_do_receiver, $to_do_text);

  if ($result)
    $_SESSION[SUCCESS_MESSAGE] = 'Your To do Text Sent Successfully.';
  else
    $_SESSION[ERROR_MESSAGE] = 'Error In Submit Data.';
}

include 'includes/inc_left.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- Small boxes (Stat box) -->
      <section class="col-sm-12 connectedSortable ui-sortable">
        <!--<div class="row">
          <div class="col-sm-12 col-xs-12">
            <div class="box box-success">
              <div class="box-header">
                <i class="fa fa-archive"></i>
                <h3 class="box-title">Today : <?php echo $get_today_menu[0]['custom_menu'] ?></h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="item">
                  <a href="estimate_inventory_report.php?date=<?php echo date('Y-m-d'); ?>" class="btn btn-primary">Estimate Inventory Report</a>
                  <a href="inventory_issue_report.php?date=<?php echo date('Y-m-d'); ?>&cmd=daily_menu" class="btn btn-primary">Inventory Issue Report</a>
                </div>
              </div>
              <div class="box-header">
                <i class="fa fa-archive"></i>
                <h3 class="box-title">Tomorrow : <?php echo $get_tomorrow_menu[0]['custom_menu'] ?></h3>
              </div>
              <div class="box-body">
                <div class="item">
                  <a href="estimate_inventory_report.php?date=<?php echo date('Y-m-d'); ?>" class="btn btn-primary">Estimate Inventory Report</a>
                  <a href="inventory_issue_report.php?date=<?php echo date('Y-m-d'); ?>&cmd=daily_menu" class="btn btn-primary">Inventory Issue Report</a>
                </div>
              </div>
            </div>
          </div>
        </div>-->

        <div class="row">
          <div class="col-sm-12 col-xs-12">
            <!-- TO DO List -->
            <div class="box box-primary">
              <div class="box-header">
                <i class="ion ion-clipboard"></i>
                <h3 class="box-title">To Do List</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
                <?php
                require_once("pagination2.php");
                echo pagination(5, $page, 'index.php?page=', $total_to_do_list);
                ?>
              </div><!-- /.box-header -->
              <div class="box-body">
                <ul class="todo-list">
                  <?php
                  if ($to_do_list) {
                    $c = 1;
                    foreach ($to_do_list as $to_do) {

                      $tempDate = $to_do['to_do_ts'];
                      $day = date('l', strtotime($tempDate));
                      $month = date('jS F Y', strtotime($tempDate));

                      $user_full_name = $cls_user->get_single_user($to_do['to_do_creator']);
                      ?>
                      <li>
                        <span class="handle">
                          <i class="fa fa-ellipsis-v"></i>
                          <i class="fa fa-ellipsis-v"></i>
                        </span>
                        <input type="checkbox" name="active[]" id="action<?php echo $to_do['id']; ?>" onclick="check(<?php echo $to_do['id']; ?>)" />
                        <!-- todo text -->
                        <span class="text"><?php echo substr($to_do['to_do_text'], 0, 40); ?></span>
                        <!-- Emphasis label -->
                        <small class="label label-<?php echo ($c++ % 2 == 1) ? 'primary' : 'success' ?>"><i class="fa fa-clock-o"></i> <?php echo $month; ?></small>
                        <!-- General tools such as edit or delete-->
                        <div class="tools">
                          <a href="#" title="View More" data-toggle="modal" data-target="#myModal<?php echo $to_do['id']; ?>"><i class="ion ion-clipboard"></i></a>&nbsp;&nbsp;
                          <a href="index.php?id=<?php echo $to_do['id']; ?>&cmd=delete" title="Delete"><i class="fa fa-trash-o"></i></a>
                        </div>
                      </li>

                      <div class="modal fade" id="myModal<?php echo $to_do['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">View To Do From : <?php echo $user_full_name['full_name']; ?></h4>
                            </div>
                            <div class="modal-body">
                              <p><?php echo $to_do['to_do_text']; ?></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php
                    }
                  } else {
                    echo '<li>No Records..</li>';
                  }
                  ?>                                                     
                </ul>
              </div><!-- /.box-body -->
              <div class="box-footer clearfix no-border">
                <button class="btn btn-default pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add New To Do</button>
              </div>

              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <form method="post" action="">  
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Enter To Do</h4>
                      </div>
                      <div class="modal-body">
                        <div class="col-sm-6">
                          <select name="to_do_receiver" class="form-control">
                            <option value="">Select User</option>
                            <?php
                            if ($users) {
                              foreach ($users as $user) {
                                ?>
                                <option value="<?php echo $user['username']; ?>"><?php echo $user['full_name']; ?></option>
                                <?php
                              }
                            }
                            ?>
                          </select>
                        </div>

                        <div class="col-sm-12"><br>
                          <textarea name="to_do_text" class="form-control" rows="4"></textarea>
                        </div>
                        <input type="hidden" name="to_do_creator" value="<?php echo $_SESSION[USERNAME]; ?>">
                      </div><br>

                      <div class="modal-footer"><br>
                        <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div><!-- /.box -->
          </div>
        </div>
      </section>

      <!--<section class="col-sm-7 connectedSortable ui-sortable">
        <div class="row">
          <div class="col-sm-6 col-xs-12">
            <div class="box box-primary">
              <div class="box-header">
                <i class="fa fa-folder"></i>
                <h3 class="box-title">Maedat</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="item">
                  <a href="drag_cat.php" class="btn btn-primary btn-block">Create Menu</a>
                </div>
                <div class="item">
                  <a href="daily_menu.php" class="btn btn-primary btn-block">Daily Menu</a>
                </div>
                <div class="item">
                  <a href="base_menus.php" class="btn btn-primary btn-block">Base Menu</a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-xs-12">
            <div class="box box-primary">
              <div class="box-header">
                <i class="fa fa-edit"></i>
                <h3 class="box-title">Hub</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="item">
                  <a href="hub_pending.php?cmd=hub_pending" class="btn btn-success btn-block">Hub Pending</a>
                </div>
                <div class="item">
                  <a href="total_daily_hub_rcpt.php" class="btn btn-success btn-block">Total hub receipt</a>
                </div>
                <div class="item">
                  <a href="takhmeen_amount.php" class="btn btn-success btn-block">Takhmeen Amount</a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="box box-success">
              <div class="box-header">
                <i class="fa fa-edit"></i>
                <h3 class="box-title">Estimated And Actual Costs for last 30 Niyaaz</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
              </div>
            </div>
          </div>
        </div>
      </section>-->
    </div>

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
$get_records = $cls_menu->get_daily_menu_entries(30);
?>

<script>

  function check(val) {
    var doc = document.getElementById('action' + val).checked;

    if (doc == true)
      doc = 1;
    else
      doc = 0;
    window.location.href = 'index.php?id=' + val + '&status=' + doc + '&cmd=update';
  }

  var area = new Morris.Line({
    element: 'revenue-chart',
    resize: true,
    data: [
<?php
$i = 0;
foreach ($get_records as $rec) {
  $i++;
  $y = date('Y-m-d', $rec['menu_date']);
  $estCost = number_format($rec['est_costing'], 2, '.', '');
  $actCost = $rec['act_cost'];
  if ($i == 1) {
    $data = "{y: '$y', estCost: '$estCost', actCost: '$actCost'}";
  } else {
    $data .=",\n";
    $data .= "{y: '$y', estCost: '$estCost', actCost: '$actCost'}";
  }
}
echo $data;
?>
    ],
    xkey: 'y',
    ykeys: ['estCost', 'actCost'],
    labels: ['Est. Cost', 'Act. Cost'],
    lineColors: ['#a0d0e0', '#3c8dbc'],
    hideHover: 'auto'
  });

</script>
<?php
include 'includes/footer.php';
?>