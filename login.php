<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.user.php');
$cls_user = new Mtx_User();
$_SESSION[USERNAME] = '';
$_SESSION[FULLNAME] = '';

if (isset($_POST['login'])) {
  $fmb_id = $database->clean_data($_POST['fmb_id']);
  $result = $cls_user->log_in($fmb_id, $_POST['password']);
  if ($result['success']) {
    $_SESSION['logged_in'] = TRUE;
    $id = $cls_user->get_id();
    $user_rights = $cls_user->getUserRights();
    $user_name = $cls_user->get_username();
    $type = $cls_user->get_level();
    $fullname = $cls_user->getFullName();
    $_SESSION[FULLNAME] = ucwords($fullname);
    $_SESSION[USERNAME] = $user_name;
    $_SESSION[USER_ID] = $id;
    $_SESSION[USER_TYPE] = $type;
    $bin = strrev(decbin($user_rights));
    $ary_user_rights = str_split($bin);
    $_SESSION[USER_RIGHTS] = $ary_user_rights;
    header('Location: index.php');
    exit;
  } else {
    $error_message = $result['errors'][0];
  }
}

$title = 'Log In';
$active_page = 'family';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="asset/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="asset/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
  </head>
  <body class="login-page">
    <div class="row">
        <div class="col-md-12"><?php include 'includes/messages.php'; ?></div>
      </div>
    <div class="login-box">
      <div class="login-logo">
        <b>Admin</b> Panel
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <form action="" method="post">
          <div class="form-group has-feedback">
            <input type="text" name="fmb_id" class="form-control" placeholder="User ID" required />
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" required />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
            </div><!-- /.col -->
            <div class="col-xs-4">
              <input type="submit" name="login" class="btn btn-primary btn-block btn-flat" value="Sign In">
            </div><!-- /.col -->
          </div>
        </form>        

        <a href="#">Request New Password</a><br>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="asset/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="asset/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="asset/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
