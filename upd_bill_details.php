<?php
include 'session.php';
$page_number = 30;
$pg_link = 'add_bill';
require_once('classes/class.database.php');
require_once('classes/class.billbook.php');
require_once('classes/class.product.php');
require_once('classes/class.receipt.php');
$user_id = $_SESSION[USER_ID];
$cls_billbook = new Mtx_BillBook();
$cls_receipt = new Mtx_Receipt();
$cls_product = new Mtx_Product();

if (isset($_POST['update'])) {
  if (!isset($_POST['type']))
    $_POST['type'] = '';
  if (!isset($_POST['make_payment'])) {
    $_POST['make_payment'] = 0;
    $_POST['type'] = '';
    $_POST['bankname'] = '';
    $_POST['cheque'] = '';
    $_POST['dob'] = '';
  }
  $vat = $database->clean_currency($_POST['vat']);
  $discount = $database->clean_currency($_POST['discount']);
  $charge = $database->clean_currency($_POST['charge']);

  if ($_GET['cmd'] == 'show_bills') {
    $update = $cls_billbook->update_bill($_POST['heads'], floatval($vat), floatval($discount), floatval($charge), floatval($_POST['amount']), (int)$_POST['make_payment'], $_POST['type'], $_POST['bankname'], $_POST['cheque'], $_POST['dob'], $_POST['description'], $_POST['bill_id'], $_POST['contact'], $_POST['ShopName']);
  } else {
    $update = $cls_billbook->update_directPurchase($_POST['heads'], floatval($vat), floatval($discount), floatval($charge), floatval($_POST['amount']), (int)$_POST['make_payment'], $_POST['type'], $_POST['bankname'], $_POST['cheque'], $_POST['dob'], $_POST['description'], $_POST['bill_id'], $_POST['bill_date'], $_POST['menu_date'], $_POST['contact'], $_POST['ShopName']);
  }
  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Bill updated successfully';
  } else {
    $_SESSION[ERROR_MESSAGE] = '';
  }
}

if (isset($_POST['cancel'])) {
  $billID = $_GET['bid'];
  $time = time();
  if ($_GET['cmd'] == 'show_bills') {
    $query = "UPDATE `account_bill` SET `cancel` = '1', `cancel_user_id` = '$user_id', `cancel_ts` = '$time' WHERE `id` = '$billID'";
    $redirect = 'grocery_bill_book.php';
  }
  if ($_GET['cmd'] == 'show_direct_bills') {
    $query = "UPDATE `direct_purchase` SET `cancel` = '1', `cancel_user_id` = '$user_id', `cancel_ts` = '$time' WHERE `id` = '$billID'";
    $redirect = 'direct_plus_payment.php';
  }

  $result = $database->query($query);
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'Bill canceled successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while canceling the bill';
  }
  header("Location: $redirect");
  exit();
}

if ($_GET['cmd'] == 'show_bills') {
  $bills = $cls_billbook->get_all_account_bill($_GET['bid']);
  $details = $cls_billbook->get_bills_from_bill_details($bills['id']);
} else {
  $bills = $cls_billbook->get_direct_bills($_GET['bid']);
  $details = $cls_billbook->get_direct_purchase_bills_from_bill_details($_GET['bid']);
}
$shops = $cls_billbook->get_shops();
$heads = $cls_receipt->get_account_heads();
$count = count($details) + 1;
$title = 'Bill Details';
$active_page = 'account';

require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Bill Books</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php'); ?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8 ">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <?php
            if ($bills['BillNo'] != 0)
              $billNo = $bills['BillNo'];
            else
              $billNo = $bills['id'];
            ?>
            <div class="form-group">
              <label class="control-label col-md-2">Bill No.</label>
              <div class="col-md-4">
                <p class="form-control-static"><?php echo $billNo; ?></p>
                <input type="hidden" name="bill_id" id="bill_id" value="<?php echo $bills['id']; ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Account Heads</label>
              <div class="col-md-4">
                <select class="form-control" name="heads" id="heads">
                  <option value="0">--Select One--</option>
                  <?php
                  if ($heads) {
                    foreach ($heads as $head) {
                      if ($bills['acct_heads'] == $head['head'])
                        $selected = 'selected';
                      else
                        $selected = '';
                      ?>
                      <option value="<?php echo $head['head']; ?>" <?php echo $selected; ?>><?php echo $head['head']; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
            <?php if ($_GET['cmd'] == 'show_direct_bills') { ?>
              <div class="form-group">
                <label class="control-label col-md-2">Bill Date</label>
                <div class="col-md-4">
                  <input type="date" name="bill_date" class="form-control" id="bill_date" placeholder="" value="<?php echo date('Y-m-d', $bills['bill_date']); ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-2">Menu Date</label>
                <div class="col-md-4">
                  <input type="date" name="menu_date" class="form-control" id="menu_date" placeholder="" value="<?php echo $bills['menu_date']; ?>">
                </div>
              </div>
            <?php } ?>
            <div class="form-group">
              <label class="control-label col-md-2">Shop Name</label>
              <div class="col-md-4">
                <select class="form-control" name="ShopName">
                  <option value="">--Select One--</option>
                  <?php
                  foreach ($shops as $shop) {
                    $sel = ($bills['name'] == $shop['id']) ? 'selected' : '';
                    ?>
                    <option value="<?php echo $shop['id']; ?>" <?php echo $sel; ?>><?php echo $shop['ShopName']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-2">
              </div>
              <div class="col-md-4">
                <strong>Item Name</strong>
              </div>
              <div class="col-md-2">
                <strong>Qty</strong>
              </div>
              <div class="col-md-2">
                <strong>Price</strong>
              </div>
              <div class="col-md-2">
                <strong>Total</strong>
              </div>
            </div>
            <?php
            if($details){
              $i = 0;
              foreach ($details as $detail) {
              ?>
              <div class="form-group">
                <label class="control-label col-md-2"><?php
                  if ($i == 0)
                    echo "Items";
                  else
                    echo '&nbsp;';
                  ?></label>
                <div class="col-md-4">
                  <p class="form-control-static"><?php echo ucwords($detail['item_name']); ?></p>
                </div>
                <div class="col-md-2">
                  <p class="form-control-static" id="quantity<?php echo $i; ?>"><?php echo $detail['quantity']; ?></p>
                </div>
                <div class="col-md-2">
                  <p class="form-control-static" id="unit<?php echo $i; ?>"><?php echo $detail['unit']; ?></p>
                </div>
                <div class="col-md-2">
                  <p class="form-control-static"><?php echo number_format($detail['price'], 2); ?></p>
                </div>
              </div>
              <?php
              $i++;
              }
            }
            ?>

            <div class="form-group">
              <label class="control-label col-md-2">VAT Charges</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="vat" id="vat" placeholder="%" onblur="AddVAT(this.value);" value="<?php echo $bills['VAT']; ?>">
                <!--p class="form-control-static"><?php echo $bills['VAT'] . '%'; ?></p-->
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Discount</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="discount" id="discount" placeholder="Rupees" onblur="Discount(this.value);" value="<?php echo number_format($bills['Discount'], 2); ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Other Charges</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="charge" id="charge" placeholder="Rupees" onblur="OtherCharges(this.value);" value="<?php echo number_format($bills['other_charges'], 2); ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Net Amount</label>
              <div class="col-md-4">
                <p class="form-control-static" id="grand_amount"><?php echo number_format($bills['amount'], 2); ?></p>
                <input type="hidden" class="form-control" name="amount" id="amount" value="<?php echo $bills['amount']; ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Description</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="description" id="description" value="<?php echo $bills['description']; ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Contact Person</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="contact" id="contact" value="<?php echo $bills['contact_person']; ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Make Payment</label>
              <div class="col-md-4">
                <input type="checkbox" class="checkbox-inline" name="make_payment" id="make_payment" <?php echo ($bills['paid'] != 0) ? 'checked' : ''; ?> onclick="payment_options();">
              </div>
            </div>

            <?php
            $cash = '';
            $cheque = '';
            if ($bills['paid'] != 0) {
              if ($bills['payment_type'] == 'cash')
                $cash = 'checked';
              if ($bills['payment_type'] == 'cheque')
                $cheque = 'checked';
            }
            ?>
            <div class="form-group" id="payment_options" style="display: <?php echo ($bills['paid'] != 0) ? 'block' : 'none'; ?>">
              <label class="control-label col-md-2">Type</label>
              <div class="col-md-4">
                <input type="radio" id="radio" name="type" value="cash" <?php echo $cash; ?>>&nbsp;Cash&emsp;
                <input type="radio" id="radio" name="type" value="cheque" <?php echo $cheque; ?>>&nbsp;Cheque&emsp;
                <input type="radio" id="radio" name="type" value="cheque-neft">&nbsp;NEFT
              </div>
            </div>
            <div id="cheque_detail" style="display: <?php
            if (!empty($cheque))
              echo 'block';
            else
              echo 'none';
            ?>">
              <div></div>

              <div class="form-group" >
                <label class="control-label col-md-2">Bank Name</label>
                <div class="col-md-4">
                  <input type="text" name="bankname" value="<?php echo $bills['bankname']; ?>" id="bankname" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-2" id="num">Cheque No.</label>
                <div class="col-md-4">
                  <input type="text" name="cheque" value="<?php echo $bills['cheque_no']; ?>" id="cheque" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-2">Date</label>
                <div class="col-md-4">
                  <input type="date" name="dob" value="<?php echo $bills['cheque_date']; ?>" class="form-control" id="dob" placeholder="">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>
              <?php if($bills['paid'] == 0){ ?>
              <button type="submit" name="update" id="update" class="btn btn-success">Update</button>
              <?php } if($_SESSION[USER_TYPE] == 'A'){ ?>
              <button type="submit" name="cancel" id="cancel" class="btn btn-danger">Cancel</button>
              <?php } ?>
              <a href="print_bill_details.php?cmd=<?php echo $_GET['cmd']; ?>&bid=<?php echo $_GET['bid']; ?>"  class="btn btn-info" target="_blank">Print</a>
            </div>

          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          var count = 0;
          var net_amt = 0;
          var gblmul = <?php echo $bills['amount']; ?>;
          var gblVat = 0;
          var gblCharges = 0;
          var gblDiscountRates = 0;
          var limit = <?php echo $count; ?>;
          
          $("input:radio[name=type]").click(function() {
            var radio = $(this).val();
            if (radio === 'cheque') {
              $('#cheque_detail').show();
              $('#num').text('Cheque No.');
            } else if (radio === 'cheque-neft') {
              $('#cheque_detail').show();
              $('#num').text('NEFT No.');
            } else {
              $('#cheque_detail').hide();
            }
          });
          function payment_options() {
            var chkbox = document.getElementById('make_payment').checked;
            if (chkbox === true) {
              $('#payment_options').show();
              $('#make_payment').val('1');
            } else {
              $('#payment_options').hide();
              $('#make_payment').val('0');
              $('#cheque_detail').hide();
            }
          }
          
          function getSum() {
            net_amt = (Number(gblmul) + Number(gblCharges)) - Number(gblDiscountRates);
            if (gblVat > 0) {
              net_amt += (Number(gblVat) * net_amt) / 100;
            }
            $('#grand_amount').text(net_amt);
            $('#amount').val(net_amt);
          }
          function getMul(id)
          {
            for (var i = 0; i < id; i++) {
              var quantity = document.getElementById("quantity" + i).value;
              var unit = document.getElementById("unit" + i).value;
              gblmul += (quantity * unit);
              $('#price1' + i).text(quantity * unit);
              $('#price' + i).val(quantity * unit);
            }
          }
          
          function AddVAT(vat) {
            gblVat = vat;
            getSum();
          }

          function OtherCharges(charge) {
            gblCharges = charge;
            getSum();
          }

          function Discount(discount) {
            gblDiscountRates = discount;
            getSum();
          }
        </script>
      </div>
      <!-- /Content -->
    </section>
  </div>

<?php
include('includes/footer.php');
?>
