<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.receipt.php");
$cls_receipt = new Mtx_Receipt();

$data = $cls_receipt->get_all_daily_cash();
$title = 'Add Hub';
$active_page = 'account';

require_once 'includes/header.php';

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <table class="table table-bordered table-hover table-condensed">
            <thead>
              <tr>
                <th>No.</th>
                <th>Date</th>
                <th class="text-right">Opening Cash</th>
                <th class="text-right">Opening Bank</th>
                <th class="text-right">Closing Cash</th>
                <th class="text-right">Closing Bank</th>
                <th class="text-right">Bank Deposit</th>
                <th class="text-right">Bank Withdwaw</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1;foreach($data as $record) {?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $record['date']; ?></td>
                <td class="text-right"><?php echo number_format($record['opening_cash'], 2); ?></td>
                <td class="text-right"><?php echo number_format($record['opening_bank'], 2); ?></td>
                <td class="text-right"><?php echo number_format($record['closing_cash'], 2); ?></td>
                <td class="text-right"><?php echo number_format($record['closing_bank'], 2); ?></td>
                <td class="text-right"><?php echo number_format($record['to_bank'], 2); ?></td>
                <td class="text-right"><?php echo number_format($record['from_bank'], 2); ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>