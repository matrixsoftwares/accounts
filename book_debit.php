<?php
include 'session.php';
$pg_link = 'book_debit';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();
$title = 'Debit voucher book';
$active_page = 'account';
$from_date = $to_date = FALSE;
$btn_print_link = TRUE;
$page = 1;
if (isset($_GET['page']) && $_GET['page'] != '') {
  $page = $_GET['page'];
}

if (isset($_GET['search'])) {
  $from_date = $_GET['from_date'];
  $to_date = $_GET['to_date'];
  $btn_print_link = "print_vouchers.php?from_date=$from_date&to_date=$to_date&cmd=debit";
  $post = TRUE;
  $search = $cls_receipt->get_receipts($page, 20, 'debit', $from_date, $to_date);

  $voucher = $cls_receipt->get_number_voucher('debit', $from_date, $to_date);
  $amount = $cls_receipt->get_debit_total_between_dates($from_date, $to_date);
} else {
  $post = FALSE;
  $search = $cls_receipt->get_receipts($page, 20, 'debit');
  $voucher = $cls_receipt->get_number_voucher('debit');
  $amount = $cls_receipt->get_debit_total_between_dates();
}
include('includes/header.php');

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<script src="asset/dist/js/bootstrap-tooltip.js"></script> 
<script src="asset/dist/js/bootstrap-popover.js"></script>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Bill Books</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <?php include 'includes/inc.dates.php'; ?>
          <?php if ($_GET) { ?>
            <div class="col-md-12">&nbsp;</div>
            <div class="alert-success">
              <strong>Total Amount: </strong><?php echo number_format($amount[0]['Amount'], 2); ?>
            </div>
          <?php } ?>
          <div class="col-md-12">&nbsp;</div>
          <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Ref. No.</th>
                <th>Name</th>
                <th>Type of Payment</th>
                <th class="text-right">Amount</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if ($search) {
                foreach ($search as $debit) {
                  ?>
              <tr class="<?php echo ($debit['cancel'] == 1) ? 'alert-danger' : ''; ?>">
                    <td><a href="update_voucher.php?cmd=debit&did=<?php echo $debit['id']; ?>"><?php echo $debit['id']; ?></a></td>
                    <td><?php echo $debit['ref_no']; ?></td>
                    <td class="example" rel="popover" data-content="<?php echo $debit['description']; ?>" data-placement="right" data-original-title="Description"><?php echo ($debit['name'] > 0) ? $debit['shopName'] : $debit['name']; ?></td>
                    <td><?php echo ucfirst($debit['payment_type']); ?></td>
                    <td class="text-right"><?php echo number_format($debit['amount'], 2); ?></td>
                    <td><?php echo date('d F, Y', $debit['timestamp']); ?></td>
                  </tr>
                <?php }
              } else {
                ?>
                <tr>
                  <td colspan="6" class="alert-danger">No vouchers found.</td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          <?php
          require_once("pagination.php");
          $getData = ($from_date && $to_date) ? 'from_date=' . $from_date . '&to_date=' . $to_date . '&search=Search&' : '';
          echo pagination(20, $page, '?' . $getData . 'page=', $voucher);
          ?>
        </div>
        <!-- /Center Bar -->

        <script>
          $(function()
          {
            $(".example").popover();
          });
        </script>  
      </div>
      <!-- /Content -->
    </section>
  </div>
<?php
include('includes/footer.php');
?>