<?php

class Mtx_Product {

  private $id;
  private $ingredientname;
  private $quantity;
  private $menu_id;

  public function Mtx_Product($id = '') {
    if ($id) {
      if ($this->set_id($id))
        return TRUE;
      else
        return FALSE;
    }
  }

  public function __destruct() {
    // TODO: destructor code
  }

  public function menu_items_deduct($ary_ingredient, $ary_quantity, $ary_unit, $total_thali) {
    global $database;
    $max = count($ary_ingredient);
    for ($i = 0; $i < $max; $i++) {
      if ($ary_ingredient[$i] != 0) {
        $qty = $database->clean_currency($ary_quantity[$i]);
        //to fetch the perticular record from inventory tbl
        $query = "SELECT `quantity` FROM `inventory` WHERE `id` LIKE '$ary_ingredient[$i]'";
        $qty = $database->query_fetch_full_result($query);
        // now will deduct from inventory - requested quantity
        $update = $qty[0]['quantity'] - $qty;
        //now update into inventory tbl
        $query = "UPDATE `inventory` SET `quantity` = '$update' WHERE `id`='$ary_ingredient[$i]'";
        $database->query($query);

        //entry in inventory tbl
        $qty = $database->clean_currency($ary_quantity[$i]);
        $query = "INSERT INTO `inventory_issue`(`ingredient_id`,`menu_id`,`quantity`) VALUES('" . $ary_ingredient[$i] . "','" . $this->menu_id . "','" . $qty . "')";
        $result = $database->query($query);
      }
    }
    return $result;
  }

  public function get_all_direct_items() {
    global $database;
    $query = "SELECT * FROM `dp_lookup` ORDER BY `name` ASC";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_ingredients($menu_id = FALSE) {
    global $database;

    $query = "SELECT *, (i.quantity - (i.used_qty + i.adjustment) + COALESCE((SELECT (SUM(ib.quantity) - SUM(ib.used_qty)) FROM `inventory_bills` ib WHERE ib.inventory_id LIKE i.id), 0) ) cqty FROM `inventory` i ORDER BY i.name ASC";
    if ($menu_id) {
      $query = "SELECT *, (i.quantity - (i.used_qty + i.adjustment) + COALESCE((SELECT (SUM(ib.quantity) - SUM(ib.used_qty)) FROM `inventory_bills` ib WHERE ib.inventory_id LIKE i.id), 0) ) cqty FROM `inventory` i WHERE id='$menu_id' ORDER BY i.name ASC";
      $result = $database->query_fetch_full_result($query);
      $rtn_array = array('cqty' => $result[0]['cqty'], 'UNIT' => $result[0]['unit']);
      return $rtn_array;
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_inventory_latest_price_list() {
    global $database;

    $query = "SELECT * FROM get_inventory_price_from_bills";

    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_item() {
    global $database;
    $query = "SELECT `id`,`name` FROM `inventory`";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function finalize_inventory_issue($issue_date, $user_id, $cmd = FALSE, $ary_qty = FALSE) {
    global $database;
    $current_ts = time();

    // step 1 get all inventory issue records
    if ($cmd) {
      $records = $this->get_estimated_issue_item_in_current_day($issue_date);
    } else {
      $records = $this->get_issued_item_in_current_day($issue_date);
    }
    // step 2 foreach issue update inventory used record (update_inventory_used)
    $date = explode('-', $issue_date);
    $issue_date = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    $issue_end = mktime(23, 59, 59, $date[1], $date[2], $date[0]);
    $i = 0;
    $cost = array();
    foreach ($records as $item) {
      $cmd = ($cmd == FALSE) ? FALSE : 'estimate';
      if ($ary_qty)
        $qty = $database->clean_currency($ary_qty[$i]);
      else
        $qty = $database->clean_currency($item['quantity']);
      
      if ($cmd)
        $act_cost = $this->update_inventory_used($item['inventory_id'], $qty, $cmd);

      if ($cmd) {
        $query = "INSERT INTO `inventory_issue`(`inventory_id`, `quantity`, `act_cost`, `issue_date`, `timestamp`, `user_id`) VALUES('$item[inventory_id]', '$qty', '$act_cost', '$issue_date', '$current_ts', '$user_id')";
        $result = $database->query($query);
      } else {
        //we will not update the act_cost because we already cut the quantity from estimate inventory report and also we inserted the act_cost.
//        $query = "UPDATE `inventory_issue` SET `act_cost` = '$act_cost' WHERE `id` = '$item[id]'";
//        $result = $database->query($query);
      }
      $i++;
    }

    //step 3 update actual cost into inventory_issue tbl

    $query = "SELECT IFNULL(SUM(`other_charges`), 0) TOTAL_CHARGES FROM `direct_purchase` WHERE `bill_date` BETWEEN '$issue_date' AND '$issue_end'";

    $result = $database->query_fetch_full_result($query);
    $otherCharges = $result[0]['TOTAL_CHARGES'];
    $invItems = $this->get_issued_item_in_current_day(date('Y-m-d', $issue_date));
    foreach ($invItems as $item) {
      $cost[] = $item['act_cost'];
    }
    $InvItemCost = array_sum($cost);
    $directItems = $this->get_issued_direct_purchase(date('Y-m-d', $issue_date));
    $grandDirect = 0;
    if (isset($directItems) && is_array($directItems)) {
      foreach ($directItems as $item) {
        $totalPerItem = $item['quantity'] * $item['unit'];
        $grandDirect += $totalPerItem;
      }
    }

    $totalActualCost = $otherCharges + $InvItemCost + $grandDirect;
    if (!$cmd) {

      $query = "INSERT into inventory_finalize(`timestamp`, `status`, `user_id`, `current_ts`, `act_cost`) values('$issue_date', '1', '$user_id', '$current_ts', '$totalActualCost')";
      $result = $database->query($query);
    }
    return $result;
  }

  private function update_inventory_bills_used($inventory_id, $diff_quantity, $cmd = FALSE) {
    global $database;
    $price = 0;
    $cmd = ($cmd == FALSE) ? FALSE : 'estimate';
    if($inventory_id == '') return 0;
    $diff_quantity = floatval($diff_quantity);
    while ($diff_quantity > 0) {
      $query = "SELECT id, unit_price, (`quantity` - `used_qty`) rem_qty FROM `inventory_bills` WHERE `inventory_id` = '$inventory_id' and (`quantity` - `used_qty`) > 0 order by `id` asc limit 0, 1";
      $result = $database->query_fetch_full_result($query);
      $id = $result[0]['id'];
      if ($diff_quantity >= $result[0]['rem_qty']) {
        $diff_quantity = $diff_quantity - $result[0]['rem_qty'];

        $rem_qty = floatval($result[0]['rem_qty']);
        $query = "UPDATE `inventory_bills` SET `used_qty` = (used_qty + $rem_qty) WHERE id = '$id'";
        $price += ($result[0]['rem_qty'] * $result[0]['unit_price']);
        $result = $database->query($query);
      } else {
        $query = "UPDATE `inventory_bills` SET `used_qty` = (used_qty + $diff_quantity) WHERE id = '$id'";
        $price += ($diff_quantity * $result[0]['unit_price']);
        $diff_quantity = 0;
        $result = $database->query($query);
      }
    }
    return $price;
  }

  private function update_inventory_used($item_id, $quantity, $cmd = FALSE) {
    global $database;
    
    // Clean the $quantity
    $quantity = $database->clean_currency($quantity);
    
    //step 1 get remaining qty from initial qty - used qty SELECT ( `quantity` -  `used_qty` )remaining_qty FROM  `inventory`  WHERE id =1
    $query = "SELECT id, used_qty, unit_price, ( `quantity` -  (`used_qty` + `adjustment`) ) remaining_qty FROM  `inventory`  WHERE id = '$item_id' LIMIT 0, 1";

    $remaining_qty = $database->query_fetch_full_result($query);
    $price = 0;
    // step 2 if remaining_qty > 0 then issue partial or full amount from it
    $cmd = ($cmd == FALSE) ? FALSE : 'estimate';
    if ($remaining_qty[0]['remaining_qty'] > 0) {
      $diff_quantity = $quantity - $remaining_qty[0]['remaining_qty'];
      if ($diff_quantity > 0) {
        // issue complete remaining qty
        $rem_qty = (int)$remaining_qty[0]['remaining_qty'];
        $query = "UPDATE `inventory` SET `used_qty` = (used_qty + $rem_qty) WHERE id = '$item_id'";
        $result = $database->query($query);
        $price += ($remaining_qty[0]['remaining_qty'] * $remaining_qty[0]['unit_price']);

        $price += $this->update_inventory_bills_used($item_id, $diff_quantity, $cmd);
      } else {
        $query = "UPDATE `inventory` SET `used_qty` = (used_qty + $quantity) WHERE id = '$item_id'";
        $result = $database->query($query);
        $price += $remaining_qty[0]['unit_price'] * $quantity;
      }
    } else {
      $price += $this->update_inventory_bills_used($item_id, $quantity, $cmd);
    }
    return $price;
    //step 3 SELECT (`quantity` - `used_qty`) rem_qty FROM `inventory_bills` WHERE `inventory_id` = 1 and (`quantity` - `used_qty`) > 0 order by `id` asc limit 0, 1
    // step 4 calculate actual costing 
    //tep 5 return actual costing 
  }

  public function is_date_finalized($issue_date) {
    global $database;
    $dateval = explode("-", $issue_date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    $query = "SELECT * FROM `inventory_finalize` WHERE `timestamp` BETWEEN '$start_time' AND '$end_time' LIMIT 0, 1";
    $result = $database->query_fetch_full_result($query);
    if ($result[0]['status'] == 1)
      return TRUE;
    else
      return FALSE;
  }

  public function get_other_charges($date) {
    global $database;
    $dateval = explode("-", $date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    $query = "SELECT * FROM `direct_purchase` WHERE `menu_date` LIKE '$date'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function add_inventory_issue($in_id, $quantity, $issue_date, $user_id) {
    global $database;
    $timestamp = time();
    $dateval = explode("-", $issue_date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $issue_time = mktime(0, 0, 0, $month, $day, $year);
    $quantity = $database->clean_currency($quantity);
    $query = "INSERT INTO `inventory_issue`(`inventory_id`, `quantity`, `issue_date`, `timestamp`, `user_id`) VALUES('$in_id','$quantity','$issue_time','$timestamp','$user_id')";
    $result = $database->query($query);
    return $result;
  }

  public function check_product_available_in_inventory($inventory_id, $given_quantity) {
    global $database;
    $query = "SELECT (`quantity` - (`used_qty` + `adjustment`)) rem_qty, `used_qty` FROM `inventory` WHERE `id` = '$inventory_id' LIMIT 0, 1";
    $result = $database->query_fetch_full_result($query);
    if ($result && $result[0]['rem_qty'] > 0)
      $initial_stock = $result[0]['rem_qty'];
    else
      $initial_stock = 0;

    if ($result && $result[0]['rem_qty'] >= $given_quantity) {
      return 'Y';
    } else {
      // subtract initial stock if available
      $given_quantity -= $initial_stock;
      $query = "SELECT (`quantity` - `used_qty`) rem_qty, `used_qty` FROM `inventory_bills` WHERE `inventory_id` = '$inventory_id' AND (`quantity` - `used_qty`) >= $given_quantity";

      $result = $database->query_fetch_full_result($query);
      if ($result)
        return 'Y';
      else
        return 'N';
    }
  }

  public function get_qty_available_in_inventory($inventory_id) {
    global $database;

    $query = "SELECT (i.quantity - (i.used_qty + i.adjustment) + COALESCE((SELECT (SUM(ib.quantity) - SUM(ib.used_qty)) FROM `inventory_bills` ib WHERE ib.inventory_id LIKE i.id), 0) ) cqty FROM `inventory` i WHERE i.id LIKE '$inventory_id'";
    $result = $database->query_fetch_full_result($query);

    if ($result) {
      return $result[0]['cqty'];
    } else {
      return '0';
    }
  }

  public function add_estimate_inventory_issue($menuName, $itemName, $quantity, $issue_date) {
    global $database;

    $date = explode('-', $issue_date);
    $issue_date = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    $query = "INSERT INTO `est_inventory`(`item`, `qty`, `date`) VALUES('$itemName','$quantity','$issue_date')";
    $result = $database->query($query);

    return $result;
  }

  function get_estimate_cost($inv_id, $cmd = FALSE) {
    global $database;

    if ($cmd) {
      $query = "SELECT * FROM `lp_inventory_costing` WHERE `inventory_id` = '$inv_id'";
      $result = $database->query_fetch_full_result($query);
      return $result[0]['cost'];
    } else {
      $query = "SELECT unit_price FROM `inventory_bills` WHERE `inventory_id` = '$inv_id' ORDER BY `timestamp` DESC LIMIT 0, 1";
      $result = $database->query_fetch_full_result($query);
      if (!$result) {
        $query = "SELECT unit_price FROM `inventory` WHERE `id` = '$inv_id'";
        $result = $database->query_fetch_full_result($query);
      }
      return $result[0]['unit_price'];
    }
  }

  function get_estimate_cost_direct($item_name, $cmd = FALSE) {
    global $database;
    if ($cmd) {
      $query = "SELECT cost FROM `lp_direct_costing` WHERE `inventory_id` = '$item_name'";
      $result = $database->query_fetch_full_result($query);
      return $result[0]['cost'];
    } else {
      $query = "SELECT price FROM `direct_bill_details` WHERE `item_name` = '$item_name' ORDER BY `id` DESC LIMIT 0, 1";
      $result = $database->query_fetch_full_result($query);
      return $result[0]['price'];
    }
  }

  public function get_all_estimate_inventory_between_dates($from, $to) {
    global $database;
    $dateval = explode("-", $from);
    $date = explode("-", $to);
    $fday = $dateval[2];
    $fmonth = $dateval[1];
    $fyear = $dateval[0];
    $from = mktime(0, 0, 0, $fmonth, $fday, $fyear);
    $tday = $date[2];
    $tmonth = $date[1];
    $tyear = $date[0];
    $to = mktime(23, 59, 59, $tmonth, $tday, $tyear);

    $query = "SELECT item as inventory_id, (SELECT name FROM `inventory` i WHERE i.id = est.item) item_name, SUM(`qty`) AS qty, `date`, (SELECT ic.name FROM lp_item_category ic WHERE ic.id = (SELECT category FROM inventory i WHERE i.id = item)) as category, (SELECT ((i.quantity - (i.used_qty + i.adjustment)) + IFNULL((SUM(ib.quantity) - SUM(ib.used_qty)),0)) FROM `inventory` i LEFT JOIN inventory_bills ib ON i.id = ib.inventory_id WHERE i.id LIKE est.item) current_quantity, (SELECT unit FROM `inventory` i WHERE i.id = est.item) unit FROM `est_inventory` est WHERE `date` BETWEEN '$from' AND '$to' GROUP BY item ORDER BY category";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_estimate_direct_bills_between_dates($from, $to) {
    global $database;
    $dateval = explode("-", $from);
    $date = explode("-", $to);
    $fday = $dateval[2];
    $fmonth = $dateval[1];
    $fyear = $dateval[0];
    $from = mktime(0, 0, 0, $fmonth, $fday, $fyear);
    $tday = $date[2];
    $tmonth = $date[1];
    $tyear = $date[0];
    $to = mktime(23, 59, 59, $tmonth, $tday, $tyear);

    $query = "SELECT item, SUM(`qty`) AS qty, (SELECT name FROM lp_item_category ic WHERE ic.id = (SELECT category FROM dp_lookup dl WHERE dl.name LIKE edp.item)) as category FROM `est_direct_pur` edp WHERE `date` BETWEEN '$from' AND '$to' GROUP BY item ORDER BY category";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function delete_inventory_issue($id) {
    global $database;
    $query = "DELETE FROM `inventory_issue` WHERE `id` = '$id'";
    $result = $database->query($query);
    return $result;
  }

  public function delete_estimated_direct_pur($id, $date = FALSE) {
    global $database;
    if ($date) {
      $date = explode('-', $date);
      $start = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
      $end = mktime(23, 59, 59, $date[1], $date[2], $date[0]);
    }
    if (is_array($id)) {
      $operator = 'IN';
      $id = "('" . implode("', '", $id) . "')";
    } else {
      $operator = '=';
      $id = "$id";
    }
    if ($date)
      $query = "DELETE FROM `est_direct_pur` WHERE `item` $operator $id AND `date` BETWEEN '$start' AND '$end'";
    else
      $query = "DELETE FROM `est_direct_pur` WHERE `id` = '$id'";
    $result = $database->query($query);
    return $result;
  }

  public function delete_estimated_inventory_issue($id, $date) {
    global $database;

    $date = explode('-', $date);
    $start = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    $end = mktime(23, 59, 59, $date[1], $date[2], $date[0]);
    if (is_array($id)) {
      $operator = 'IN';
      $id = "('" . implode("', '", $id) . "')";
    } else {
      $operator = '=';
      $id = "$id";
    }
    $query = "DELETE FROM `est_inventory` WHERE `item` $operator $id AND `date` BETWEEN '$start' AND '$end'";

    $result = $database->query($query) or die(mysql_error());
    return $result;
  }
  
  public function delete_estimated_inventory_issue_der($id, $date) {
    global $database;

    $date = explode('-', $date);
    $start = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    $end = mktime(23, 59, 59, $date[1], $date[2], $date[0]);
    if (is_array($id)) {
      $operator = 'IN';
      $id = "('" . implode("', '", $id) . "')";
    } else {
      $operator = '=';
      $id = "$id";
    }
    $query = "DELETE FROM `est_inventory_der` WHERE `item` $operator $id AND `date` BETWEEN '$start' AND '$end'";

    $result = $database->query($query) or die(mysql_error());
    return $result;
  }

  function return_inventory($item_id, $qty, $date) {
    global $database;

    $date = explode('-', $issue_date);
    $date = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    $query = "INSERT INTO `return_inventory`(`item_id`, `qty`, `timestamp`) VALUES('$item_id', '$qty', '$date')";
    $result = $database->query($query);
    return $result;
  }

  public function get_issued_item_in_current_day($date) {
    global $database;

    $dateval = explode("-", $date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    $query = "SELECT i.id as inventory_id, inis.id, inis.issue_date, inis.act_cost, inis.quantity, i.name, i.unit FROM `inventory_issue` inis INNER JOIN `inventory` i ON inis.inventory_id = i.id WHERE `issue_date` between '$start_time' AND '$end_time'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function update_cook_est_inv_qty($ary_est_qty, $issue_date) {
    global $database;
    $items = $this->get_estimated_issue_item_in_current_day($issue_date);
    $dateval = explode("-", $issue_date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    if ($items) {
      $i = 0;
      foreach ($items as $item) {
        $query = "UPDATE `est_inventory` SET `qty` = '$ary_est_qty[$i]' WHERE `id` = '$item[id]' AND `date` between '$start_time' AND '$end_time'";
        $result = $database->query($query);
        $i++;
      }
    }
    return $result;
  }

  function update_cook_est_direct_qty($ary_est_qty, $issue_date) {
    global $database;
    $items = $this->get_estimated_issue_direct_purchase($issue_date);
    $dateval = explode("-", $issue_date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    if ($items) {
      $i = 0;
      foreach ($items as $item) {
        $query = "UPDATE `est_direct_pur` SET `qty` = '$ary_est_qty[$i]' WHERE `id` = '$item[id]' AND `date` between '$start_time' AND '$end_time'";
        $result = $database->query($query);
        $i++;
      }
    }
    return $result;
  }

  function insert_cook_est_inv_qty($ary_est_qty, $issue_date, $person_count) {
    global $database;

    $dateval = explode("-", $issue_date);
    $start_time = mktime(0, 0, 0, $dateval[1], $dateval[2], $dateval[0]);

    foreach ($ary_est_qty as $id => $qty) {
      $query = "SELECT id, qty FROM `est_inventory` WHERE `date` LIKE '$start_time' AND `item` LIKE '$id'";
      $result = $database->query_fetch_full_result($query);

      if ($result) {
//        $qty = $database->clean_currency($qty);
//        $qty /= $person_count;
        if ($result[0]['qty'] != $qty) {
          $id = $result[0]['id'];
          $query = "UPDATE `est_inventory` SET `qty` = '$qty' where id like '$id'";
          $result = $database->query($query);
        }
      } else {
        $query = "INSERT INTO `est_inventory` (`item`, `qty`, `date`) VALUES ('$id', '$qty', '$start_time')";
        $result = $database->query($query);
      }
    }

    return $result;
  }
  
  function insert_cook_est_inv_qty_der($ary_est_qty, $issue_date, $person_count) {
    global $database;

    $dateval = explode("-", $issue_date);
    $start_time = mktime(0, 0, 0, $dateval[1], $dateval[2], $dateval[0]);

    foreach ($ary_est_qty as $id => $qty) {
      $query = "SELECT id, qty FROM `est_inventory_der` WHERE `date` LIKE '$start_time' AND `item` LIKE '$id'";
      $result = $database->query_fetch_full_result($query);

      if ($result) {
//        $qty = $database->clean_currency($qty);
//        $qty /= $person_count;
        if ($result[0]['qty'] != $qty) {
          $id = $result[0]['id'];
          $query = "UPDATE `est_inventory_der` SET `qty` = '$qty' where id like '$id'";
          $result = $database->query($query);
        }
      } else {
        $query = "INSERT INTO `est_inventory_der` (`item`, `qty`, `date`) VALUES ('$id', '$qty', '$start_time')";
        $result = $database->query($query);
      }
    }

    return $result;
  }

  function insert_cook_est_direct_qty($ary_est_qty, $issue_date) {
    global $database;

    $dateval = explode("-", $issue_date);
    $start_time = mktime(0, 0, 0, $dateval[1], $dateval[2], $dateval[0]);

    foreach ($ary_est_qty as $id => $qty) {
      $query = "SELECT id, qty FROM `est_direct_pur` WHERE `date` LIKE '$start_time' AND `item` LIKE '$id'";
      $result = $database->query_fetch_full_result($query);

      if ($result) {
        if ($result[0]['qty'] != $qty) {
          $id = $result[0]['id'];
          $query = "UPDATE `est_direct_pur` SET `qty` = '$qty' where id like '$id'";
          $result = $database->query($query);
        }
      } else {
        $query = "INSERT INTO `est_direct_pur` (`item`, `qty`, `date`) VALUES ('$id', '$qty', '$start_time')";
        $result = $database->query($query);
      }
    }

    return $result;
  }

  public function get_estimated_issue_item_in_current_day($date) {
    global $database;

    $dateval = explode("-", $date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    $query = "SELECT id, item as inventory_id, (SELECT `name` FROM `inventory` i WHERE i.id = est.item) item, qty as quantity, (SELECT `unit` FROM `inventory` i WHERE i.id = est.item) unit, date FROM `est_inventory` est WHERE `date` between '$start_time' AND '$end_time'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function check_est_item_exist_in_issue($date) {
    global $database;

    $dateval = explode("-", $date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    $query = "SELECT COUNT(*) cnt FROM `inventory_issue` WHERE `issue_date` between '$start_time' AND '$end_time'";
    $result = $database->query_fetch_full_result($query);
    $result = $result[0]['cnt'];
    if ($result > 0)
      return TRUE;
    else
      return FALSE;
  }

  public function get_issued_direct_purchase($date) {
    global $database;

    $query = "SELECT dp.id, dbd.item_name, dbd.quantity, dbd.unit FROM `direct_purchase` dp INNER JOIN `direct_bill_details` dbd ON dp.id = dbd.bill_id WHERE dp.menu_date LIKE '$date'";
    //echo $query;
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_estimated_issue_direct_purchase($date) {
    global $database;
    $dateval = explode("-", $date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    $query = "SELECT * FROM `est_direct_pur` WHERE `date` between '$start_time' AND '$end_time'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_person_according_size($size) {
    global $database;
    $query = "SELECT `person` FROM `tiffin_details` WHERE `size` = '$size'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['person'];
  }

  public function get_particular_ingredient_name($id) {
    global $database;
    $query = "SELECT `id`,`name`,`unit` FROM `inventory` WHERE `id` = '$id'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }
  
  public function get_all_base_menu_der() {
    global $database;
    $query = "SELECT *, (SELECT menu_name FROM `menu` m WHERE m.`menu_id`=bmd.`base_menu_id`) Menu FROM `base_menu_der` bmd GROUP BY bmd.`base_menu_id`";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }
  
  public function get_all_items_by_base_menu($base_menu_id) {
    global $database;
    $query = "SELECT *, (SELECT name FROM `inventory` i WHERE i.`id` = bmd.`item_id`) Item FROM `base_menu_der` bmd WHERE `base_menu_id` = '$base_menu_id'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  //getter and setter
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function get_ingredient_name() {
    return $this->ingredientname;
  }

  public function set_ingredient_name($ingredientname) {
    $this->ingredientname = $ingredientname;
  }

  public function getQuantity() {
    return $this->quantity;
  }

  public function setQuantity($quantity) {
    $this->quantity = $quantity;
  }

  public function get_menu_id() {
    return $this->menu_id;
  }

  public function set_menu_id($menu_id) {
    $this->menu_id = $menu_id;
  }

}

?>
