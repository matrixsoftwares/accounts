<?php

  class Mtx_User {

    private $id;
    private $level;
    private $name;
    private $fullname;
    private $rights;

    public function Mtx_User($id = '') {
      if ($id) {
        if ($this->set_id($id))
          return TRUE;
        else
          return FALSE;
      }
    }

    public function __destruct() {
      // TODO: destructor code
    }
    
   public function register_user($user_name, $password, $type,$ref_id) {
      global $database;
      $query = "INSERT INTO `users`(`username`, `password`,`userlevel`,`ref_id`) VALUES ('$user_name','$password','$type','$ref_id')";
      $ary_return = $database->query($query);
      if ($ary_return) {
        return TRUE;
      }
      return FALSE;
    }

    private function load_user() {
      global $database;

      $query = "SELECT * FROM `users` WHERE `id` LIKE '" . $this->id . "'";
      $result = $database->query_fetch_full_result($query);
      $result = $result[0];
      if (!$result)
        return FALSE;
      $this->id = $result['id'];
      $this->name = $result['username'];
      $this->password = $result['password'];
      $this->level = $result['type'];
      $this->fullname = $result['full_name'];

      return TRUE;
    }

    /*
    * ==========================================
    *  Validate user function
    * ==========================================
    */

    /*
    * =================================================
    *               login user functions
    * =================================================
    */

    public function add_user($name, $email, $mobile, $userid, $password, $mohallah, $user_rights)
    {
      global $database;
      $pwd = md5($password);
      $type = 'S';
      $query = "INSERT INTO users(`username`,`password`,`type`,`full_name`, `Mohallah`, `email`, `mobile`,`user_rights`) VALUES('$userid','$pwd','$type','$name','$mohallah','$email','$mobile','$user_rights')";
      $result = $database->query($query);
      return $result;
    }
    
    public function Update_user($name, $email, $mobile, $password, $confirm_password, $new_password, $userid, $mohallah, $user_rights)
    {
      global $database;
      $type = 'S';
      $query = "UPDATE `users` SET `full_name` = '$name',`Mohallah` = '$mohallah',`email` = '$email',`mobile` = '$mobile', `user_rights` = '$user_rights' WHERE `username` = '$userid'";
      $result = $database->query($query);
      if($password != ''){
        $confirm = md5($confirm_password);
        $query = "UPDATE `users` SET `password` = '$confirm' WHERE `username` LIKE '$userid'";
        $result = $database->query($query);
      }
      return $result;
    }
    
    public function check_user_exist($userid)
    {
      global $database;
      $query = "SELECT * FROM `users` WHERE `username` LIKE '$userid' LIMIT 1";
      $result = mysql_num_rows(mysql_query($query));
      return $result;
    }
    
    public function check_old_password($old_pwd, $id)
    {
      global $database;
      $query = "SELECT `password` FROM `users` WHERE `id` LIKE '$id'";
      $result = $database->query_fetch_full_result($query);
      if($result[0]['password'] == md5($old_pwd)){
        return 1;
      } else {
        return 0;
      }
    }
    
    public function log_in($user_name, $password) {
      global $database;

      // verify user name
      $query = "SELECT * FROM `users` WHERE `username` = '$user_name'";
      
      $result = $database->query_fetch_full_result($query);
      if ($result) {
        // confirm password
        if (md5($password) == $result[0]['password']) {
          $this->set_level($result[0]['type']);
          $this->set_username($result[0]['username']);
          $this->set_id($result[0]['id']);
          $this->setFullName($result[0]['full_name']);
          $this->setUserRights($result[0]['user_rights']);
          $return['success'] = TRUE;
          return $return;
        } else {
          $return['success'] = FALSE;
          $return['errors'][] = 'Incorrect Password.';
          return $return;
        }
      } else {
        $return['success'] = FALSE;
        $return['errors'][] = 'Username not found';
        return $return;
      }
    }
    
    public function get_all_user($user_id = FALSE)
    {
      global $database;
      $query = "SELECT * FROM `users`";
      if($user_id){
        $query = "SELECT * FROM `users` WHERE `id` = '$user_id'";
        $result = $database->query_fetch_full_result($query);
        return $result[0];
      }
      $result = $database->query_fetch_full_result($query);
      return $result;
    }
    
    public function get_single_user($userid)
    {
      global $database;
      $query = "SELECT * FROM `users` WHERE `username` LIKE '$userid'";
      $result = $database->query_fetch_full_result($query);
      return $result[0];
    }
    
    public function add_message($message, $user)
    {
      global $database;
      $time = time();
      $query = "INSERT INTO messages(`message`,`user_id`,`timestamp`) VALUES('$message','$user','$time')";
      $result = $database->query($query);
      return $result;
    }

    public function update_message($message, $id)
    {
      global $database;
      $time = time();
      $query = "UPDATE messages SET `message` = '$message', `timestamp` = '$time' WHERE `id` = '$id'";
      $result = $database->query($query);
      return $result;
    }

    public function get_messages()
    {
      global $database;
      $query = "SELECT * FROM `messages`";
      $result = $database->query_fetch_full_result($query);
      return $result;
    }
    
    public function update_general_settings($id, $email, $tanzeem, $hub, $kisht, $start_year, $no_years, $default_year, $start_date, $end_date, $zabihat_inayat, $mumineen_contri, $open_cash, $open_bank, $lia_cash, $lia_bank, $hub_pending_tpl, $senderId, $msg_user_name, $msg_password, $masaar_sender_id, $API_key, $route, $country_code, $default_sms_vendor) {
      global $database;
      $query = "UPDATE `general_settings` SET `back_up_email` = '$email', `tanzeem_name` = '$tanzeem', `avg_niyaz_hub` = '$hub', `default_kisht` = '$kisht', `start_year` = '$start_year', `no_of_years` = '$no_years', `default_year` = '$default_year', `start_date` = '$start_date',`end_date` = '$end_date', `zabihat_inayat` = '$zabihat_inayat', `mumineen_contri` = '$mumineen_contri', `hub_pending_tpl` = '$hub_pending_tpl', `sender_id` = '$senderId', `user_name` = '$msg_user_name', `password` = '$msg_password', `masaar_sender_id` = '$masaar_sender_id', `API_key` = '$API_key', `route` = '$route', `country_code` = '$country_code', `default_sms_vendor` = '$default_sms_vendor' WHERE `id` = '$id'";
      $result = $database->query($query);
      $query = "UPDATE `balancesheet` SET `amount` = '$open_cash' WHERE `name` LIKE 'opening_cash'";
      $result = $database->query($query);
      $query = "UPDATE `balancesheet` SET `amount` = '$open_bank' WHERE `name` LIKE 'opening_bank'";
      $result = $database->query($query);
      $query = "UPDATE `balancesheet` SET `amount` = '$lia_cash' WHERE `name` LIKE 'lia_cash'";
      $result = $database->query($query);
      $query = "UPDATE `balancesheet` SET `amount` = '$lia_bank' WHERE `name` LIKE 'lia_bank'";
      $result = $database->query($query);
      return $result;
    }


    public function add_general_settings($email, $tanzeem, $hub, $kisht, $start_year, $no_years, $default_year, $start_date, $end_date, $zabihat_inayat, $mumineen_contri, $open, $lia, $hub_pending_tpl, $senderId, $msg_user_name, $msg_password, $masaar_sender_id, $API_key, $route, $country_code, $default_sms_vendor) {
      global $database;
      $query = "INSERT INTO `general_settings`(`back_up_email`, `tanzeem_name`, `avg_niyaz_hub`, `default_kisht`, `start_year`, `no_of_years`, `default_year`, `start_date`, `end_date`, `zabihat_inayat`, `hub_pending_tpl`, `sender_id`, `user_name`, `password`, `masaar_sender_id`, `API_key`, `route`, `country_code`, `default_sms_vendor`) VALUES ('$email', '$tanzeem', '$hub', '$kisht', '$start_year', '$no_years', '$default_year', '$start_date', '$end_date', '$zabihat_inayat', '$mumineen_contri', '$hub_pending_tpl', '$senderId', '$msg_user_name', '$msg_password', '$masaar_sender_id', '$API_key', '$route', '$country_code', '$default_sms_vendor')";
      $result = $database->query($query);
      $id = $database->get_last_insert_id();
      $query = "UPDATE `balancesheet` SET `amount` = '$open' WHERE `name` LIKE 'opening balance'";
      $result = $database->query($query);
      $query = "UPDATE `balancesheet` SET `amount` = '$lia' WHERE `name` LIKE 'liabilities'";
      $result = $database->query($query);
      return $id;
    }
    
    public function get_general_settings($id = FALSE) {
      global $database;
      $query = "SELECT * FROM `general_settings`";
      if($id) $query .= " WHERE `id` = '$id'";
      $result = $database->query_fetch_full_result($query);
      return $result;
    }
    
    public function get_page_rights($rights) {
      global $database;
      $query = "SELECT * FROM `page_rights` WHERE `rights` LIKE '$rights'";
      $result = $database->query_fetch_full_result($query);
      return $result;
    }




    /*
    * =====================================================
    *               LOGIN FUNCTION END
    * =====================================================
    */




    /*
    * ======================================================
    *               Get and Set functions
    * ======================================================
    */

    public function get_id() {
      return $this->id;
    }

    public function set_id($id) {
      $this->id = $id;
      $this->load_user();
    }

    public function get_level(){
      return $this->level;
    }

    public function set_level($level){
      $this->level = $level;
      $this->load_user();
    }
    
    public function set_username($name)
    {
        $this->name = $name;
    }
    
    public function get_username()
    {
        return $this->name;
    }

    public function setFullName($fullname)
    {
        $this->fullname = $fullname;
    }
    
    public function getFullName()
    {
        return $this->fullname;
    }

    public function setUserRights($rights)
    {
        $this->rights = $rights;
    }
    
    public function getUserRights()
    {
        return $this->rights;
    }

  }

  ;
?>