<?php

class Mtx_Receipt {

  private $id;

  public function Mtx_Receipt($id = '') {
    if ($id) {
      if ($this->set_id($id))
        return TRUE;
      else
        return FALSE;
    }
  }

  public function __destruct() {
    // TODO: destructor code
  }

  //function
  public function add_last_year_month($file, $date, $hub) {
    global $database;

    $date = explode('-', $date);
    $paid_till = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    $query = "INSERT INTO fmb_hub(`FileNo`, `hub_raqam`, `paid_till`) VALUES('$file','$hub','$paid_till')";
    $result = $database->query($query);
    return $result;
  }

  public function add_last_year_month_sabil_home($file, $date, $hub) {
    global $database;

    $date = explode('-', $date);
    $paid_till = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    $query = "INSERT INTO `sabil_home`(`FileNo`, `amount`, `paid_till`) VALUES('$file','$hub','$paid_till')";
    $result = $database->query($query);
    return $result;
  }

  public function add_last_year_month_sabil_vepaar($file, $date, $hub) {
    global $database;

    $date = explode('-', $date);
    $paid_till = mktime(0, 0, 0, $date[0], $date[1], $date[2]);
    $query = "INSERT INTO `sabil_vepaar`(`FileNo`, `amount`, `paid_till`) VALUES('$file','$hub','$paid_till')";
    $result = $database->query($query);
    return $result;
  }

  function get_last_five_hub_rcpt($file_no) {
    global $database;
    $query = "SELECT * FROM `fmb_receipt_hub` WHERE `FileNo` LIKE '$file_no' AND `cancel` = '0' ORDER BY `date` DESC LIMIT 0, 5 ";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function add_receipt_hub($fileno, $name, $amount, $paid_till, $paid_upto, $months, $type, $bank, $cheque, $cheque_date, $user_id, $form_id, $original, $settlement, $ref_no, $partial_ids = FALSE) {
    global $database;

    //process the form if not already processed
    $query = "SELECT `return_id` FROM `forms_processed` where `form_id` LIKE '$form_id'";
    $result = $database->query_fetch_full_result($query);

    if (!$result) {
      $query = "UPDATE fmb_hub SET `paid_till` = '$paid_upto' WHERE FileNo='$fileno'";
      $result = $database->query($query);

      $time = time();

      //clean the amount
      $amount = $database->clean_currency($amount);
      $receipt = "INSERT INTO fmb_receipt_hub(`FileNo`, `name`,`amount`, `from_date`, `to_date`, `payment_type`, `bank`, `cheque`, `cheque_date`, `date`, `user_id`, `original_amount`, `settlement`,`ref_no`) VALUES('$fileno','$name','$amount','$paid_till','$paid_upto','$type','$bank','$cheque','$cheque_date', '$time', '$user_id', '$original', '$settlement', '$ref_no')";

      if ($partial_ids != '')
        $receipt = "INSERT INTO fmb_receipt_hub(`FileNo`, `name`,`amount`, `from_date`, `to_date`, `payment_type`, `bank`, `cheque`, `date`, `user_id`, `original_amount`, `settlement`, `ref_no`, `partial_ids`) VALUES('$fileno','$name','$amount','$paid_till','$paid_upto','$type','$bank','$cheque','$time', '$user_id', '$original', '$settlement', '$ref_no', '$partial_ids')";

      $fh = fopen('test.txt', 'a');
      fwrite($fh, $query);
      fclose($fh);
      $result_receipt = $database->query($receipt) or die(mysql_error());
      $id = $database->get_last_insert_id();

      // insert the row into the forms processed table
      $time = time();
      $query = "INSERT INTO forms_processed VALUES ('$form_id','$id','$time')";
      $database->query($query);

      return $id;
    }else {
      return $result[0]['return_id'];
    }
  }

  public function add_sabil_home($fileno, $name, $amount, $paid_till, $paid_upto, $months, $type, $bank, $cheque, $date, $user_id, $form_id, $partial_ids = FALSE) {
    global $database;

    //process the form if not already processed
    $query = "SELECT `return_id` FROM `forms_processed` where `form_id` LIKE '$form_id'";
    $result = $database->query_fetch_full_result($query);

    if (!$result) {
      $query = "UPDATE `sabil_home` SET `paid_till` = '$paid_upto' WHERE FileNo='$fileno'";
      $result = $database->query($query);

      if (!$date)
        $time = time();
      else {
        $date = explode('-', $date);
        $time = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
      }

      //clean the amount
      $amount = $database->clean_currency($amount);
      $receipt = "INSERT INTO `sabil_home_receipt`(`FileNo`, `name`,`amount`, `from_date`, `to_date`, `payment_type`, `bank`, `cheque`, `date`, `user_id`) VALUES('$fileno','$name','$amount','$paid_till','$paid_upto','$type','$bank','$cheque','$time', '$user_id')";

      if ($partial_ids != '')
        $receipt = "INSERT INTO fmb_receipt_hub(`FileNo`, `name`,`amount`, `from_date`, `to_date`, `payment_type`, `bank`, `cheque`, `date`, `user_id`, `partial_ids`) VALUES('$fileno','$name','$amount','$paid_till','$paid_upto','$type','$bank','$cheque','$time', '$user_id', '$partial_ids')";

      $fh = fopen('test.txt', 'a');
      fwrite($fh, $query);
      fclose($fh);
      $result_receipt = $database->query($receipt) or die(mysql_error());
      $id = $database->get_last_insert_id();

      // insert the row into the forms processed table
      $time = time();
      $query = "INSERT INTO forms_processed VALUES ('$form_id','$id','$time')";
      $database->query($query);

      return $id;
    } else {
      return $result[0]['return_id'];
    }
  }

  public function add_sabil_vepaar($fileno, $name, $amount, $paid_till, $paid_upto, $months, $type, $bank, $cheque, $date, $user_id, $form_id, $partial_ids = FALSE) {
    global $database;

    //process the form if not already processed
    $query = "SELECT `return_id` FROM `forms_processed` where `form_id` LIKE '$form_id'";
    $result = $database->query_fetch_full_result($query);

    if (!$result) {
      $query = "UPDATE `sabil_home` SET `paid_till` = '$paid_upto' WHERE FileNo='$fileno'";
      $result = $database->query($query);

      if (!$date)
        $time = time();
      else {
        $date = explode('-', $date);
        $time = mktime(0, 0, 0, $date[0], $date[1], $date[2]);
      }

      //clean the amount
      $amount = $database->clean_currency($amount);
      $receipt = "INSERT INTO `sabil_home_receipt`(`FileNo`, `name`,`amount`, `from_date`, `to_date`, `payment_type`, `bank`, `cheque`, `date`, `user_id`) VALUES('$fileno','$name','$amount','$paid_till','$paid_upto','$type','$bank','$cheque','$time', '$user_id')";
      //echo $receipt;exit;
      if ($partial_ids != '')
        $receipt = "INSERT INTO fmb_receipt_hub(`FileNo`, `name`,`amount`, `from_date`, `to_date`, `payment_type`, `bank`, `cheque`, `date`, `user_id`, `partial_ids`) VALUES('$fileno','$name','$amount','$paid_till','$paid_upto','$type','$bank','$cheque','$time', '$user_id', '$partial_ids')";



      $fh = fopen('test.txt', 'a');
      fwrite($fh, $query);
      fclose($fh);
      $result_receipt = $database->query($receipt) or die(mysql_error());
      $id = $database->get_last_insert_id();

      // insert the row into the forms processed table
      $time = time();
      $query = "INSERT INTO forms_processed VALUES ('$form_id','$id','$time')";
      $database->query($query);

      return $id;
    }else {
      return $result[0]['return_id'];
    }
  }

  public function get_single_sabil_home($fileNo) {
    global $database;
    $query = "SELECT * FROM `sabil_home` WHERE `FileNo`='$fileNo'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_single_sabil_vepaar($fileNo) {
    global $database;
    $query = "SELECT * FROM `sabil_vepaar` WHERE `FileNo`='$fileNo'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_total_amount_receipt($rcpt_id) {
    global $database;
    $query = "SELECT * FROM `fmb_receipt_hub` WHERE `id` = '$rcpt_id'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_total_amount_vol_receipt($id) {
    global $database;
    $query = "SELECT * FROM `receipt` WHERE id = '$id'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function insert_reason_for_vol_cancellation($reason, $rcpt_id, $user_id) {
    global $database;
    $timestamp = time();
    $query = "UPDATE `receipt` SET `reason` = '$reason', `cancel` = '1', `cancel_ts` = '$timestamp', `cancel_user_id` = '$user_id' WHERE `id` = '$rcpt_id'";

    $result = $database->query($query) or die(mysql_error());
    return $result;
  }

  public function insert_reason_for_cancellation($reason, $rcpt_id, $user_id) {
    global $database;
    $timestamp = time();
    $query = "UPDATE `fmb_receipt_hub` SET `reason` = '$reason', `cancel` = '1', `cancel_ts` = '$timestamp', `cancel_user_id` = '$user_id' WHERE `id` = '$rcpt_id'";
    $result = $database->query($query);
    return $result;
  }

  public function get_total_amount_receipt_sabil_home($id) {
    global $database;
    $query = "SELECT * FROM `sabil_home_receipt` WHERE receipt_no = '$id'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_total_amount_receipt_sabil_vepaar($id) {
    global $database;
    $query = "SELECT * FROM `sabil_vepaar_receipt` WHERE receipt_no = '$id'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_remarks() {
    global $database;
    $query = "SELECT * FROM `voluntary_remarks`";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_account_heads() {
    global $database;
    $query = "SELECT * FROM `account_head`";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_master_account_heads($id = FALSE) {
    global $database;
    $query = "SELECT * FROM `account_heads_master`";
    if ($id) {
      $query = "SELECT *  FROM `account_heads_master` WHERE `heads` LIKE '$id'";
      $result = $database->query_fetch_full_result($query);
      return $result[0];
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_sub_heads_in_master($id) {
    global $database;
    $query = "SELECT *  FROM `account_head` WHERE `master_head_name` = '$id'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function get_cheque_amount_credit($sub_head = FALSE, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) CHEQUE_CLEARED FROM `credit_voucher` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '1' AND `cancel` = '0'";
    if($sub_head)
      $query = "SELECT IFNULL(SUM(`amount`), 0) CHEQUE_CLEARED FROM `credit_voucher` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND acct_heads IN (select head from account_head where head = '$sub_head') AND `cheque_clear` = '1' AND `cancel` = '0'";
    
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    $cleared = $result[0]['CHEQUE_CLEARED'];
    return $cleared;
  }
  
  function get_cheque_unclear_amount_credit($start = FALSE, $end = FALSE) {
    global $database;

    $query = "SELECT IFNULL(sum(`amount`), 0) CHEQUE_UNCLEARED FROM `credit_voucher` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '0' AND `cancel` = '0'";
    
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    $uncleared = $result[0]['CHEQUE_UNCLEARED'];
    return $uncleared;
  }

  function get_cash_amount_credit($sub_head = FALSE, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) CASH_AMOUNT FROM `credit_voucher` WHERE `payment_type` LIKE 'cash' AND `cancel` = '0'";
    
    if($sub_head)
      $query = "SELECT IFNULL(SUM(`amount`), 0) CASH_AMOUNT FROM `credit_voucher` WHERE acct_heads IN (select head from account_head where head = '$sub_head') AND `payment_type` LIKE 'cash' AND `cancel` = '0'";
    
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }    
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CASH_AMOUNT'];
  }

  public function get_credit_sum_from_sub_heads_id($sub_head, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT sum(`amount`) amt FROM `credit_voucher` WHERE acct_heads IN (select head from account_head where head = '$sub_head')";
    if ($start && $end)
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_credit_voucher_from_sub_heads_id($sub_head, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT *  FROM `credit_voucher` WHERE `acct_heads` LIKE '$sub_head'";
    if ($start && $end)
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_credit_voucher_from_master($master, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT head FROM `account_head` WHERE `master_head_name` = '$master'";
    $result = $database->query_fetch_full_result($query);
    $ary_credit = array();
    foreach ($result as $sub) {
      $query = "SELECT *  FROM `credit_voucher` WHERE `acct_heads` LIKE '$sub[head]'";
      if ($start && $end)
        $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
      $result = $database->query_fetch_full_result($query);
      if ($result) {
        foreach ($result as $credit) {
          $ary_credit[] = $credit;
        }
      }
    }
    return $ary_credit;
  }

  public function get_vol_rcpt_detail_from_sub_heads($sub_head, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT *  FROM `receipt` WHERE `acct_heads` IN (select id from account_head where head = '$sub_head')";
    if ($start && $end)
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }
  
  public function get_vol_rcpt_detail_from_not_in_sub_heads($sub_head, $sub_head2, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT SUM(`amount`) AS Amount  FROM `receipt` WHERE `acct_heads` NOT IN ('$sub_head', '$sub_head2')";
    if ($start && $end)
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Amount'];
  }

  public function get_vol_rcpt_detail_from_master($master, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT *  FROM `receipt` WHERE `acct_heads` IN (select id from account_head where master_head_name = '$master')";
    if ($start && $end)
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_debit_vocuhers($sub_head_id, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT *  FROM `debit_voucher` WHERE `acct_heads` LIKE '$sub_head_id'";
    if ($start && $end)
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_debit_vocuhers_from_master($master, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT head FROM `account_head` WHERE `master_head_name` = '$master'";
    $result = $database->query_fetch_full_result($query);
    $ary_debit = array();
    foreach ($result as $sub) {
      $query = "SELECT *  FROM `debit_voucher` WHERE `acct_heads` LIKE '$sub[head]'";
      if ($start && $end)
        $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
      $result = $database->query_fetch_full_result($query);
      if ($result) {
        foreach ($result as $deb) {
          $ary_debit[] = $deb;
        }
      }
    }
    return $ary_debit;
  }

  public function get_bill_payment_bills($sub_head_id, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT *  FROM `account_bill` WHERE `acct_heads` LIKE '$sub_head_id'";
    if ($start && $end)
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_bill_payment_bills_from_master($master, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT head FROM `account_head` WHERE `master_head_name` = '$master'";
    $result = $database->query_fetch_full_result($query);
    $ary_bills = array();
    foreach ($result as $bill) {
      $query = "SELECT *  FROM `account_bill` WHERE `acct_heads` LIKE '$bill[head]'";
      if ($start && $end)
        $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
      $result = $database->query_fetch_full_result($query);
      if ($result) {
        foreach ($result as $deb) {
          $ary_bills[] = $deb;
        }
      }
    }
    return $ary_bills;
  }

  function get_cash_debit_amount($sub_head = FALSE, $start = FALSE, $end = FALSE) {
    global $database;
    if($sub_head)
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CASH_AMOUNT  FROM `debit_voucher` WHERE `payment_type` LIKE 'cash' AND `acct_heads` IN (SELECT head FROM `account_head` WHERE `head` = '$sub_head') AND `cancel` = '0'";
    else
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CASH_AMOUNT  FROM `debit_voucher` WHERE `payment_type` LIKE 'cash' AND `cancel` = '0'";
    
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CASH_AMOUNT'];
  }
  
  function get_cheque_debit_amount($sub_head = FALSE, $start = FALSE, $end = FALSE) {
    global $database;
    if($sub_head)
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CLEAR_AMOUNT  FROM `debit_voucher` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '1' AND `cancel` = '0' AND `acct_heads` IN (SELECT head FROM `account_head` WHERE `head` = '$sub_head')";
    else
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CLEAR_AMOUNT  FROM `debit_voucher` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '1' AND `cancel` = '0'";
    
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CLEAR_AMOUNT'];
  }
  
  function get_cheque_unclear_debit_amount($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) AS UNCLEAR_AMOUNT  FROM `debit_voucher` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '0' AND `cancel` = '0'";
    
    if($start && $end) {
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    return $result[0]['UNCLEAR_AMOUNT'];
  }

  public function get_debit_sum_from_sub_heads_id($sub_head, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT SUM(`amount`) AS Amount  FROM `debit_voucher` WHERE `acct_heads` IN (select head from account_head where head = '$sub_head')";
    if ($start && $end)
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_cash_amount_bills($sub_head = FALSE, $start = FALSE, $end = FALSE) {
    global $database;
    if($sub_head)
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CASH_AMOUNT  FROM `account_bill` WHERE `payment_type` LIKE 'cash' AND `acct_heads` IN (SELECT head FROM `account_head` WHERE `head` = '$sub_head') AND `cancel` = '0' AND `paid` = '1'";
    else
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CASH_AMOUNT  FROM `account_bill` WHERE `payment_type` LIKE 'cash' AND `cancel` = '0' AND `paid` = '1'";
    
    if ($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CASH_AMOUNT'];
  }

  function get_cheque_amount_bills($sub_head = FALSE, $start = FALSE, $end = FALSE) {
    global $database;
    if($sub_head)
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CHEQUE_AMOUNT FROM `account_bill` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '1' AND `acct_heads` IN (SELECT head FROM `account_head` WHERE `head` = '$sub_head') AND `cancel` = '0'";
    else
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CHEQUE_AMOUNT FROM `account_bill` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '1' AND `cancel` = '0'";
    
    if ($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CHEQUE_AMOUNT'];
  }

  function get_cheque_unclear_amount_bills($sub_head = FALSE, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) AS UNCLEAR_AMOUNT FROM `account_bill` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '0' AND `cancel` = '0'";
    
    if($start && $end) {
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    return $result[0]['UNCLEAR_AMOUNT'];
  }

  function get_cash_amount_direct_purchase($sub_head = FALSE, $start = FALSE, $end = FALSE) {
    global $database;
    if($sub_head)
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CASH_AMOUNT  FROM `direct_purchase` WHERE `payment_type` LIKE 'cash' AND `acct_heads` IN (SELECT head FROM `account_head` WHERE `head` = '$sub_head') AND `cancel` = '0' AND `paid` = '1'";
    else
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CASH_AMOUNT  FROM `direct_purchase` WHERE `payment_type` LIKE 'cash' AND `cancel` = '0' AND `paid` = '1'";
    
    if ($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CASH_AMOUNT'];
  }

  function get_cheque_amount_direct_purchase($sub_head = FALSE, $start = FALSE, $end = FALSE) {
    global $database;
    if($sub_head)
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CHEQUE_AMOUNT FROM `direct_purchase` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '1' AND `acct_heads` IN (SELECT head FROM `account_head` WHERE `head` = '$sub_head') AND `cancel` = '0' AND paid = 1";
    else
      $query = "SELECT IFNULL(SUM(`amount`), 0) AS CHEQUE_AMOUNT FROM `direct_purchase` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '1' AND `cancel` = '0' AND paid = 1";
    
    if ($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CHEQUE_AMOUNT'];
  }

  function get_cheque_unclear_amount_direct_purchase($sub_head = FALSE) {
    global $database;
    if($sub_head)
    $query = "SELECT IFNULL(SUM(`amount`), 0) AS UNCLEAR_AMOUNT FROM `direct_purchase` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '0' AND `cancel` = '0' AND paid = 1";
    else
    $query = "SELECT IFNULL(SUM(`amount`), 0) AS UNCLEAR_AMOUNT FROM `direct_purchase` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '0' AND `cancel` = '0' AND paid = 1 AND `acct_heads` IN (SELECT head FROM `account_head` WHERE `head` = '$sub_head')";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['UNCLEAR_AMOUNT'];
  }

  public function get_bills_from_sub_heads($sub_head, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT SUM(`amount`) AS Amount  FROM `account_bill` WHERE `acct_heads` IN (select head from account_head where head = '$sub_head')";
    if ($start && $end)
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  function get_cheque_amount_from_sub_head_voluntary($sub_head_id, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) AS CHEQUE_CLEARED FROM `receipt` WHERE (`type` LIKE 'cheque' OR `type` LIKE 'neft') AND `acct_heads` = '$sub_head_id' AND cancel = '0' AND `cheque_clear` = '1'";
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    $cleared = $result[0]['CHEQUE_CLEARED'];
    $query = "SELECT SUM(`amount`) AS CHEQUE_UNCLEARED FROM `receipt` WHERE (`type` LIKE 'cheque' OR `type` LIKE 'neft') AND `acct_heads` = '$sub_head_id' AND cancel = '0' AND `cheque_clear` = '0'";
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    $result = $database->query_fetch_full_result($query);
    $uncleared = $result[0]['CHEQUE_UNCLEARED'];
    return $cleared;
  }

  function get_cash_amount_from_sub_head_voluntary($sub_head_id, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) AS CASH_AMOUNT  FROM `receipt` WHERE `type` LIKE 'cash' AND `acct_heads` = '$sub_head_id' AND cancel = '0'";
    
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CASH_AMOUNT'];
  }

  public function get_vol_rcpt_from_sub_heads($sub_head, $start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT SUM(`amount`) AS Amount  FROM `receipt` WHERE `acct_heads` = '$sub_head' AND cancel = '0'";
    if ($start && $end)
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_debit_voucher_from_sub_heads_id($sub_head) {
    global $database;
    $query = "SELECT *  FROM `debit_voucher` WHERE `acct_heads` LIKE '$sub_head'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_bills_from_acct_bills($sub_head) {
    global $database;
    $query = "SELECT `name`,`amount`,`payment_type`,`timestamp`,`id`  FROM `account_bill` WHERE `acct_heads` LIKE 'Zabihat Hub'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function add_remarks($remark) {
    global $database;
    $query = "INSERT INTO `voluntary_remarks`(`remarks`) VALUES('$remark')";
    $result = $database->query($query);
    return $result;
  }

  public function add_master_heads($head) {
    global $database;
    $query = "INSERT INTO `account_heads_master`(`heads`) VALUES('$head')";
    $result = $database->query($query);
    return $result;
  }

  public function add_account_head($head, $master_name) {
    global $database;
    $query = "INSERT INTO `account_head`(`head`, `master_head_name`) VALUES('$head', '$master_name')";
    $result = $database->query($query);
    return $result;
  }

  public function add_receipt($FileNo, $name, $amount, $type, $bank, $cheque, $cheque_date, $user_id, $form_id, $acct_heads) {
    global $database;
    //process the form if not already processed
    $query = "SELECT `return_id` FROM `forms_processed` where `form_id` LIKE '$form_id'";
    //echo $query;exit;
    $result = $database->query_fetch_full_result($query);

    if (!$result) {
      $timestamp = time();
      if ($cheque_date != '') {
        $date = explode('-', $cheque_date);
        $cheque_date2 = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
      } else
        $cheque_date2 = '0';
      $query = "INSERT INTO `receipt`(`FileNo`, `hubber_name`, `amount`, `type`, `bankname`, `cheque_no`, `cheque_date`, `timestamp`, `user_id`, `acct_heads`) VALUES('$FileNo','$name','$amount','$type','$bank','$cheque','$cheque_date2', '$timestamp', '$user_id', '$acct_heads')";
      //echo $query.'<br>';
      $result = $database->query($query) or die(mysql_error());
      $id = $database->get_last_insert_id();
      if($type == 'neft') {
        $query = "UPDATE `receipt` SET `cheque_clear` = '1', `cheque_clear_date` = '$cheque_date' WHERE `id` = '$id'";
        $database->query($query);
      }
      $time = time();
      $query = "INSERT INTO forms_processed VALUES ('$form_id','$id','$time')";
      //echo $query.'<br>';

      $database->query($query);

      return $id;
    } else {
      return $result[0]['return_id'];
    }
  }

  public function get_single_acct_heads($id) {
    global $database;
    $query = "SELECT * FROM `account_head` WHERE `id` = '$id' LIMIT 0, 1";
    $result = $database->query_fetch_full_result($query) or die(mysql_error());
    if ($result)
      $name = $result[0]['master_head_name'] . ' / ' . $result[0]['head'];
    else
      $name = '----';
    return $name;
  }

  public function add_partial_payment($FileNo, $amount, $name, $user_id, $form_id, $close = FALSE, $remaining_amount = FALSE, $cmd = FALSE) {
    global $database;

    //process the form if not already processed
    $query = "SELECT `return_id` FROM `forms_processed` where `form_id` LIKE '$form_id'";
    //echo $query.'<br>';
    $result = $database->query_fetch_full_result($query);

    if (!$result) {
      $time = time();
      //query to insert data into partial payment table
      $query = "INSERT INTO `partial_payments`(`FileNo`, `amount`, `name`, `creat_date`,`user_id`) VALUES('$FileNo','$amount','$name','$time', '$user_id')";
      $result = $database->query($query) or die(mysql_error());
      $id = $database->get_last_insert_id();

      // insert the row into the forms processed table
      $time = time();
      $query = "INSERT INTO forms_processed VALUES ('$form_id','$id','$time')";
      $database->query($query);

      // if monthly hub is equal to partial amount then we will get the ids of other partial receipt and will update the close date 
      if ($close) {
        $query = "SELECT `id` FROM `partial_payments` WHERE `FileNo` = '$FileNo' AND `close_date` = '0'";
        $rows = mysql_num_rows(mysql_query($query));
        $result = $database->query_fetch_full_result($query);
        $rtn_array = array();
        for ($i = 0; $i < $rows; $i++) {
          $rtn_array[$i] = $result[$i]['id'];
        }
        $partial_ids = implode(',', $rtn_array);

        $query = "UPDATE `partial_payments` SET close_date = '$close' WHERE FileNo='$FileNo'";
        $result = $database->query($query);
        if ($cmd && $cmd == 'return_id')
          return $id;
        return $partial_ids;
      }

      return $id;
      //return $ary_partial_id;
    } else {
      return $result[0]['return_id'];
    }
  }

  public function add_remaining_payment($FileNo, $remaining_amount, $name, $user_id, $form_id) {
    global $database;
    $time = time();
    $query = "INSERT INTO `partial_payments`(`FileNo`, `amount`, `name`, `creat_date`,`user_id`) VALUES('$FileNo','$remaining_amount','$name','$time','$user_id')";
    $result = $database->query($query) or die(mysql_error());
    $id = $database->get_last_insert_id();
    return $id;
  }

  public function receipt_amount_total_is_hub($fId) {
    global $database;
    $query = "SELECT SUM(`amount`) AS TotalPayment FROM `partial_payments` WHERE FileNo='$fId' AND close_date='0'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_partial_receipt_details($id) {
    global $database;

    $query = "SELECT * FROM partial_payments WHERE `id` = '$id'";
    $result = $database->query_fetch_full_result($query) or die(mysql_error());
    if ($result)
      return $result[0];
  }

  public function get_all_receipt($page_num, $row_count, $from_date = FALSE, $to_date = FALSE) {
    global $database;
    $limit = ($page_num - 1) * $row_count;
    $query = "SELECT * FROM `receipt` ORDER BY `id` DESC LIMIT $limit,$row_count";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT * FROM `receipt` WHERE `timestamp` BETWEEN '$from' AND '$to' ORDER BY `id` DESC LIMIT $limit,$row_count";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_total_voluntary_receipt($from_date = FALSE, $to_date = FALSE) {
    global $database;
    $query = "SELECT COUNT(*) AS `Total` FROM `receipt`";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query .= " WHERE `timestamp` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Total'];
  }

  public function get_all_partial_receipt($page_num, $row_count, $from_date = FALSE, $to_date = FALSE) {
    global $database;
    $limit = ($page_num - 1) * $row_count;
    $query = "SELECT * FROM `partial_payments` ORDER BY `id` DESC LIMIT $limit,$row_count";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT * FROM `partial_payments` WHERE `creat_date` BETWEEN '$from' AND '$to' ORDER BY `id` DESC LIMIT $limit,$row_count";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_total_partial_receipt($from_date = FALSE, $to_date = FALSE) {
    global $database;
    $query = "SELECT COUNT(*) AS `Total` FROM `partial_payments`";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT COUNT(*) AS `Total` FROM `partial_payments` WHERE `creat_date` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Total'];
  }

  public function make_cheque_clear($rcpt_id, $table) {
    global $database;
    $date = date('Y-m-d');
    $query = "UPDATE $table SET `cheque_clear` = '1', `cheque_clear_date` = '$date' WHERE `id` = '$rcpt_id'";
    //echo $query;exit;
    $result = $database->query($query);
    return $result;
  }

  public function make_bill_cheque_clear($bill_id, $status) {
    global $database;
    $query = "UPDATE `account_bill` SET `cheque_clear` = '$status' WHERE `id` = '$bill_id'";
    $result = $database->query($query);
    return $result;
  }

  public function make_vol_cheque_clear($rcpt_id, $status) {
    global $database;
    $query = "UPDATE `receipt` SET `cheque_clear` = '$status' WHERE `id` = '$rcpt_id'";
    $result = $database->query($query);
    return $result;
  }

  public function get_total_hub_receipt_cheque($from_date = FALSE, $to_date = FALSE) {
    global $database;
    $query = "SELECT COUNT(*) AS `Total` FROM `fmb_receipt_hub` WHERE `payment_type` = 'cheque' AND `cheque_clear` = '0'";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT COUNT(*) AS `Total` FROM `fmb_receipt_hub` WHERE `payment_type` = 'cheque' AND `cheque_clear` = '0' AND `date` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Total'];
  }

  public function get_total_bills_cheque($from_date = FALSE, $to_date = FALSE) {
    global $database;
    $query = "SELECT COUNT(*) AS `Total` FROM `account_bill` WHERE `payment_type` = 'cheque' AND `cheque_clear` = '0'";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT COUNT(*) AS `Total` FROM `account_bill` WHERE `payment_type` = 'cheque' AND `cheque_clear` = '0' AND `cheque_date` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Total'];
  }

  public function get_total_vol_receipt_cheque($from_date = FALSE, $to_date = FALSE) {
    global $database;
    $query = "SELECT COUNT(*) AS `Total` FROM `receipt` WHERE `type` = 'cheque' AND `cheque_clear` = '0'";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT COUNT(*) AS `Total` FROM `receipt` WHERE `type` = 'cheque' AND `cheque_clear` = '0' AND `cheque_date` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Total'];
  }
  
  function get_all_cheque($table, $payment_type = FALSE) {
   global $database;
   $payment_type = $payment_type ? $payment_type : 'payment_type';
   $subquery = ($table == '`debit_voucher`' OR $table == '`credit_voucher`' OR $table == '`account_bill`' OR $table == 'direct_purchase') ? ", IFNULL((SELECT `ShopName` FROM `companies` c WHERE c.id LIKE t.name), name) name" : "";
   $query = "SELECT * $subquery FROM $table t WHERE (t.`$payment_type` LIKE 'cheque' OR t.`$payment_type` LIKE 'neft') AND `cheque_clear` = '0' AND `cancel` = '0' ORDER BY `id` DESC";
   //if($table == '`credit_voucher`') echo $query;
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function get_all_unpaid($table, $paid = FALSE) {
   global $database;
   $paid = $paid ? $paid : 'paid';
   $subquery = ($table == '`debit_voucher`' OR $table == '`credit_voucher`' OR $table == '`account_bill`' OR $table == 'direct_purchase') ? ", IFNULL((SELECT `ShopName` FROM `companies` c WHERE c.id LIKE t.name), name) name" : "";
   $query = "SELECT * $subquery FROM $table t WHERE t.`$paid` = 0 AND `cancel` = '0' ORDER BY `id` DESC";
   //if($table == '`credit_voucher`') echo $query;
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_hub_cheque_receipt($page_num, $row_count, $from_date = FALSE, $to_date = FALSE) {
    global $database;
    $limit = ($page_num - 1) * $row_count;

    $query = "SELECT * FROM `fmb_receipt_hub` WHERE `payment_type` = 'cheque' AND `cheque_clear` = '0'";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query .= " AND `date` BETWEEN '$from' AND '$to'";
    }
    $query .= " ORDER BY `id` DESC LIMIT $limit,$row_count";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_bills_cheque($page_num, $row_count, $from_date = FALSE, $to_date = FALSE) {
    global $database;
    $limit = ($page_num - 1) * $row_count;

    $query = "SELECT * FROM `account_bill` WHERE `payment_type` = 'cheque' AND `cheque_clear` = '0' ORDER BY id DESC LIMIT $limit,$row_count";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT * FROM `account_bill` WHERE `payment_type` = 'cheque' AND `cheque_clear` = '0' AND `cheque_date` BETWEEN '$from' AND '$to' ORDER BY id DESC LIMIT $limit,$row_count";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_vol_cheque_receipt($page_num, $row_count, $from_date = FALSE, $to_date = FALSE) {
    global $database;
    $limit = ($page_num - 1) * $row_count;

    $query = "SELECT * FROM `receipt` WHERE `type` LIKE 'cheque' AND `cheque_clear` = '0' ";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query .= "AND `cheque_date` BETWEEN '$from' AND '$to'";
    }
    $query .= " ORDER BY id DESC LIMIT $limit,$row_count";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_total_bills_between_dates($from = FALSE, $to = FALSE, $payment_type = FALSE) {
    global $database;
    $query = "SELECT SUM(`amount`) AS Amount FROM `account_bill`";
    if ($from && $to) {
      $from_split = explode('-', $from);
      $to_split = explode('-', $to);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);
      $query .= " WHERE `cheque_date` BETWEEN '$from' AND '$to'";
    }
    if ((!$from) && (!$to) && $payment_type)
      $query .= " WHERE `payment_type` = 'cheque' AND `cheque_clear` = '0'";
    else if ($payment_type)
      $query .= " AND `payment_type` = 'cheque' AND `cheque_clear` = '0'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_total_between_dates($from = FALSE, $to = FALSE) {
    global $database;
    $query = "SELECT SUM(`amount`) AS Amount FROM `fmb_receipt_hub` WHERE `cancel` = '0' ";
    if ($from && $to) {
      $query .= " AND `date` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_vol_total_between_dates($from = FALSE, $to = FALSE, $payment_type = FALSE) {
    global $database;
    $query = "SELECT SUM(`amount`) AS Amount FROM `receipt`";
    if ($from && $to) {
      $from_split = explode('-', $from);
      $to_split = explode('-', $to);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);
      $query .= " WHERE `cancel` = '0' AND `timestamp` BETWEEN '$from' AND '$to'";
    }
    if ((!$from) && (!$to) && $payment_type)
      $query .= "WHERE `type` = 'cheque' AND `cheque_clear` = '0'";
    else if ($payment_type)
      $query .= " AND `type` = 'cheque' AND `cheque_clear` = '0'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_partial_total_between_dates($from = FALSE, $to = FALSE) {
    global $database;
    $query = "SELECT SUM(`amount`) AS Amount FROM `partial_payments`";
    if ($from && $to) {
      $from_split = explode('-', $from);
      $to_split = explode('-', $to);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);
      $query .= " WHERE `creat_date` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }
  
  public function get_daily_cash($from_date, $to_date) {
    global $database;
    $query = "SELECT * FROM `daily_cash` WHERE `date` BETWEEN '$from_date' AND '$to_date'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_receipt_for_hub($page_num, $row_count, $from_date = FALSE, $to_date = FALSE) {
    global $database;
    $limit = ($page_num - 1) * $row_count;

    $query = "SELECT * FROM `fmb_receipt_hub` ORDER BY `id` DESC LIMIT $limit,$row_count";
    if ($from_date && $to_date) {
      $query = "SELECT * FROM `fmb_receipt_hub` WHERE `date` BETWEEN '$from_date' AND '$to_date' ORDER BY `id` DESC LIMIT $limit,$row_count";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_receipt_for_sabil_home($page_num, $row_count, $from_date = FALSE, $to_date = FALSE) {
    global $database;
    $limit = ($page_num - 1) * $row_count;

    $query = "SELECT * FROM `sabil_home_receipt` ORDER BY `receipt_no` DESC LIMIT $limit,$row_count";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT * FROM `sabil_home_receipt` WHERE `date` BETWEEN '$from' AND '$to' ORDER BY `receipt_no` DESC LIMIT $limit,$row_count";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_receipt_for_sabil_vepaar($page_num, $row_count, $from_date = FALSE, $to_date = FALSE) {
    global $database;
    $limit = ($page_num - 1) * $row_count;

    $query = "SELECT * FROM `sabil_vepaar_receipt` ORDER BY `receipt_no` DESC LIMIT $limit,$row_count";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT * FROM `sabil_vepaar_receipt` WHERE `date` BETWEEN '$from' AND '$to' ORDER BY `receipt_no` DESC LIMIT $limit,$row_count";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_total_hub_receipt($from_date = FALSE, $to_date = FALSE) {
    global $database;
    $query = "SELECT COUNT(*) AS `Total` FROM `fmb_receipt_hub`";
    if ($from_date && $to_date) {
      $query .= " WHERE `date` BETWEEN '$from_date' AND '$to_date'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Total'];
  }

  public function get_total_sabil_home_receipt($from_date = FALSE, $to_date = FALSE) {
    global $database;
    $query = "SELECT COUNT(*) AS `Total` FROM `sabil_home_receipt`";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT COUNT(*) AS `Total` FROM `sabil_home_receipt` WHERE `date` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Total'];
  }

  public function get_total_sabil_vepaar_receipt($from_date = FALSE, $to_date = FALSE) {
    global $database;
    $query = "SELECT COUNT(*) AS `Total` FROM `sabil_vepaar_receipt`";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT COUNT(*) AS `Total` FROM `sabil_vepaar_receipt` WHERE `date` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Total'];
  }

  public function add_entry_form($file, $amount, $id) {
    global $database;
    $time = time();
    $query = "INSERT INTO entry(`FileNo`, `amount`, `timestamp`, `user_id`) VALUES('$file', '$amount', '$time', '$id')";
    $result = $database->query($query);
    return $result;
  }

  public function get_entry_form() {
    global $database;
    $query = "SELECT DISTINCT(FileNo) FROM entry";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_receipt_by_id($fId) {
    global $database;
    $query = "select sum(amount) as total, FileNo, timestamp FROM entry where FileNo='$fId'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function receipt_exist($file, $paid_till, $paid_upto) {
    global $database;
    $query = "SELECT * FROM fmb_receipt_hub WHERE `FileNo` = '$file' AND `from_date`='$paid_till' AND `to_date` = '$paid_upto'";

    $result = mysql_num_rows(mysql_query($query));
    return $result;
  }

  public function get_all_hub_receipt() {
    global $database;
    $query = "Select * from fmb_receipt_hub";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }
  
  public function get_balance($field) {
    global $database;
    $query = "SELECT `amount` FROM `balancesheet` WHERE `name` LIKE '$field'";
    $result = $database->query_fetch_full_result($query);
    if($result) $balance = $result[0]['amount'];
    else $balance = FALSE;
    return $balance;
  }

  public function get_bank_withrawals($from_date = FALSE, $to_date = FALSE) {
    global $database;
    
    if($from_date && $to_date){
      $query = "SELECT SUM(`from_bank`) as from_bank FROM `bank_transaction` WHERE `date` BETWEEN '$from_date' AND '$to_date'";
    } else {
      $query = "SELECT SUM(`from_bank`) as from_bank FROM `bank_transaction`";
    }
    $result = $database->query_fetch_full_result($query);
    if($result) {
      $bank_withrawal = $result[0]['from_bank'];
    } else {
      $bank_withrawal = FALSE;
    }
    return $bank_withrawal;
  }

  public function get_bank_deposits($from_date = FALSE, $to_date = FALSE) {
    global $database;
    
    if($from_date && $to_date){
      $query = "SELECT SUM(`to_bank`) as to_bank FROM `bank_transaction` WHERE `date` BETWEEN '$from_date' AND '$to_date'";
    } else {
      $query = "SELECT SUM(`to_bank`) as to_bank FROM `bank_transaction`";
    }
    $result = $database->query_fetch_full_result($query);
    if($result) {
      $bank_withrawal = $result[0]['to_bank'];
    } else {
      $bank_withrawal = FALSE;
    }
    return $bank_withrawal;
  }

  public function get_opening_balance($name) {
    global $database;
    $query = "Select amount from balancesheet where name LIKE '%$name%'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_liabilities($name) {
    global $database;
    $query = "Select amount from balancesheet where name LIKE '%$name%'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_balance_by_date($date){
    global $database;
    $query = "SELECT * FROM `daily_cash` WHERE `date` >= '$date' ORDER BY `date` ASC LIMIT 0,1";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function get_cash_amount_hub($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) AS CASH_AMOUNT FROM `fmb_receipt_hub` WHERE `payment_type` LIKE 'cash' AND `cancel` = '0'";
    
    if($start && $end){
      $query .= " AND `date` BETWEEN '$start' AND '$end'";
    }
        
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CASH_AMOUNT'];
  }

  function get_cheque_amount_hub($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) AS CHEQUE_CLEARED FROM `fmb_receipt_hub` WHERE `cheque_clear` = '1' AND `cancel` = '0' AND (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft')";
    
    if($start && $end){
      $query .= " AND `date` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    $cheque_cleared = $result[0]['CHEQUE_CLEARED'];
    return $cheque_cleared;
  }
  
  function get_cheque_unclear_amount_hub($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) AS CHEQUE_UNCLEARED FROM `fmb_receipt_hub` WHERE `cheque_clear` = '0' AND `cancel` = '0' AND (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft')";
    
    if($start && $end){
      $query .= " AND `date` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    $cheque_uncleared = $result[0]['CHEQUE_UNCLEARED'];
    return $cheque_uncleared;
  }

  public function get_hub_sum($start = FALSE, $end = FALSE, $command = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(amount),0) AS total FROM fmb_receipt_hub WHERE `cancel` =  '0'";
    if ($start && $end)
      $query .= " AND `date` BETWEEN '$start' AND '$end'";
    if ($command)
      $query .= " GROUP BY `payment_type`";

    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_all_uncleared_chequesum() {
    global $database;
    $query = "SELECT SUM(amount) AS UNCLEARED_AMOUNT FROM fmb_receipt_hub WHERE `cheque_clear` =  '0' AND payment_type='cheque'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['UNCLEARED_AMOUNT'];
  }

  public function get_all_vol_uncleared_chequesum() {
    global $database;
    $query = "SELECT SUM(amount) AS UNCLEARED_AMOUNT FROM receipt WHERE `cheque_clear` =  '0' AND type='cheque'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['UNCLEARED_AMOUNT'];
  }

  function get_cash_amount_voluntary($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) CASH_AMOUNT FROM `receipt` WHERE `type` LIKE 'cash' AND `cancel` = '0' AND `acct_heads` = '0'";
    
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CASH_AMOUNT'];
  }

  function get_cheque_amount_voluntary($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) CHEQUE_CLEARED FROM `receipt` WHERE (`type` LIKE 'cheque' OR `type` LIKE 'neft') AND `cancel` = '0' AND `cheque_clear` = '1' AND `acct_heads` = '0'";
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    $cleared = $result[0]['CHEQUE_CLEARED'];
    return $cleared;
  }
  
  function get_cheque_unclear_amount_voluntary($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`amount`), 0) CHEQUE_UNCLEARED FROM `receipt` WHERE (`type` LIKE 'cheque' OR `type` LIKE 'neft') AND `cancel` = '0' AND `cheque_clear` = '0'";
    
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    $uncleared = $result[0]['CHEQUE_UNCLEARED'];
    return $uncleared;
  }

  public function get_voluntary_hub_sum($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT SUM(amount) totalvoluntaryhub, (select sum(amount) from receipt where type='cheque' AND `cheque_clear` != 1) Unclear FROM receipt WHERE `cancel` != '1' AND `acct_heads` = 0";
    if ($start && $end)
      $query = "SELECT SUM(amount) totalvoluntaryhub, (select sum(amount) from receipt where type='cheque' AND `cheque_clear` != 1) Unclear FROM receipt WHERE `cancel` != '1' AND `acct_heads` = 0 AND `timestamp` BETWEEN '$start' AND '$end'";
    $cash = $database->query_fetch_full_result($query);
    $result = $cash[0]['totalvoluntaryhub'] - $cash[0]['Unclear'];
    return $result;
  }
  
  function get_total_cash_sum_bills() {
    global $database;
    $query = "SELECT IFNULL(SUM(amount), 0) as CASH_AMOUNT FROM `account_bill` WHERE `payment_type` LIKE 'cash' AND `paid` = '1' AND `cancel` = '0'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CASH_AMOUNT'];
  }
  
  function get_total_cheque_sum_bills() {
    global $database;
    $query = "SELECT IFNULL(SUM(amount), 0) as CHEQUE_CLEAR_AMOUNT FROM `account_bill` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '1' AND `cancel` = '0' AND `paid` = '1'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CHEQUE_CLEAR_AMOUNT'];
  }
  
  function get_total_cheque_unclear_sum_bills($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(amount), 0) as CHEQUE_UNCLEAR_AMOUNT FROM `account_bill` WHERE (`payment_type` LIKE 'cheque' OR `payment_type` LIKE 'neft') AND `cheque_clear` = '0' AND `cancel` = '0'";
    
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CHEQUE_UNCLEAR_AMOUNT'];
  }
  
  public function get_total_sum_bills($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(amount),0) AS NetAmount FROM `account_bill` WHERE acct_heads = ''";
    if ($start && $end)
      $query = "SELECT IFNULL(SUM(amount),0) AS NetAmount FROM `account_bill` WHERE `timestamp` BETWEEN '$start' AND '$end' AND acct_heads = ''";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function print_normal_receipt($rcpt_id) {
    global $database;
    $query = "select *, (SELECT ah.head from account_head ah where ah.id = r.acct_heads) head from receipt r where r.id='$rcpt_id'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_pending_amount_for_hub($file) {
    global $database;
    $query = "SELECT * FROM fmb_hub WHERE FileNo = '$file'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_all_inventory_item() {
    global $database;
    $query = "SELECT * FROM inventory";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function add_item_in_inventory($item, $category, $quantity, $unit, $price, $step) {
    global $database;
    $query = "INSERT INTO `inventory`(`name`,`quantity`, `unit`, `unit_price`, `category`, `step`) VALUES('$item','$quantity','$unit', '$price', '$category', '$step')";
    $result = $database->query($query);
    return $result;
  }
  
  public function add_min_qty_of_item_in_inventory($item, $min_qty, $unit) {
    global $database;
    $query = "UPDATE `inventory` SET `min_qty` = '$min_qty', `unit` = '$unit' WHERE name = '$item'";
    $result = $database->query($query);
    return $result;
  }
  
  public function get_adjustment_of_inventory_item($item) {
    global $database;
    $query = "SELECT `adjustment` FROM `inventory` WHERE `id` = '$item'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['adjustment'];
  }
  
  public function add_adjustment_in_inventory($item, $qty) {
    global $database;
    $adjust_res = $this->get_adjustment_of_inventory_item($item);
    $adjustment = $adjust_res - $qty;
    
    $query = "UPDATE `inventory` SET `adjustment` = '$adjustment' WHERE id = '$item'";
    $result = $database->query($query);
    if($result){
      $adjust_log = $this->add_adjust_inventory_log($item, $qty, $_SESSION[USER_ID], 'plus');
    }
    return $result;
  }
  
  public function minus_adjustment_in_inventory($item, $qty) {
    global $database;
    $adjust_res = $this->get_adjustment_of_inventory_item($item);
    $adjustment = $adjust_res + $qty;
    
    $query = "UPDATE `inventory` SET `adjustment` = '$adjustment' WHERE `id` = '$item'";
    $result = $database->query($query);
    if($result){
      $adjust_log = $this->add_adjust_inventory_log($item, $qty, $_SESSION[USER_ID], 'minus');
    }
    return $result;
  }
  
  public function add_adjust_inventory_log($item, $qty, $user_id, $oprt) {
    global $database;
    $timestamp = date('Y-m-d');
    $query = "INSERT INTO `inventory_adjustments_log`(`item_id`,`amount`,`user_id`, `timestamp`, `oprt`) VALUES('$item','$qty','$user_id','$timestamp', '$oprt')";
    $result = $database->query($query);
    return $result;
  }
  
  public function add_inventory_in_inventory_bills($item, $quantity, $price, $user_id, $FileNo, $name) {
    global $database;
    
    $date = explode('-', date('Y-m-d'));
    $time = mktime(0, 0, 0, $date[0], $date[1], $date[2]);
      
    $query = "INSERT INTO `inventory_bills`(`inventory_id`,`quantity`, `unit_price`, `timestamp`, `user_id`) VALUES('$item','$quantity', '$price', '$time', '$user_id')";
    $result = $database->query($query);
    
    $qry = "INSERT INTO `inventory_hadayah`(`user_id`,`inventory_id`,`quantity`,`unit_price`,`FileNo`,`name`,`timestamp`) VALUES('$user_id','$item','$quantity','$price','$FileNo','$name','$time')";        $res = $database->query($qry);
    return $res;
  }

  public function update_item_in_inventory($id, $quantity, $date = FALSE) {
    global $database;
    $qty = "SELECT `quantity` FROM `inventory` WHERE `id` = '$id'";
    $res_qty = $database->query_fetch_full_result($qty);
    $upd_qty = $res_qty[0]['quantity'] + $quantity;
    $query = "UPDATE `inventory` SET `quantity`='$upd_qty' WHERE id='$id'";
    $result = $database->query($query) or die(mysql_error());
    if ($date) {
      $date = explode('-', $date);
      $time = mktime(0, 0, 0, $date[0], $date[1], $date[2]);
      $query = "INSERT INTO `inventory_bills`(`quantity`,`timestamp`,`inventory_id`) VALUES('$quantity','$time','$id')";
      $result = $database->query($query) or die(mysql_error());
      return $result;
    }
    return $result;
  }

  public function add_hub_receipt_record($thali_id, $thali_name, $year, $amount, $ref_no, $timestamp, $form_id, $payment_type, $bank_name, $chk_no, $chk_date) {
    global $database;

    $query = "SELECT `return_id` FROM `forms_processed` where `form_id` LIKE '$form_id'";
    $result = $database->query_fetch_full_result($query);
    if (!$result) {
      $query = "INSERT INTO `fmb_receipt_hub`(`FileNo`, `name`, `amount`, `year`, `ref_no`, `date`, `payment_type`, `bank_name`, `cheque_no`, `cheque_date`) VALUES ('$thali_id', '$thali_name', '$amount', '$year', '$ref_no', '$timestamp', '$payment_type', '$bank_name', '$chk_no', '$chk_date')";
      $result = $database->query($query) or die(mysql_error());
      $insert_id = $database->get_last_insert_id();
      if($payment_type == 'neft') {
        $query = "UPDATE `fmb_receipt_hub` SET `cheque_clear` = '1', `cheque_clear_date` = '$timestamp' WHERE `id` = '$insert_id'";
        $database->query($query);
      }
      $time = time();
      $query = "INSERT INTO forms_processed VALUES ('$form_id','$insert_id','$time')";
      $database->query($query);
      return $insert_id;
    } else {
      return $result[0]['return_id'];
    }
  }

  public function get_unit($item) {
    global $database;
    $query = "SELECT * FROM `inventory` WHERE id='$item'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_particular_partial_receipt($fileNo) {
    global $database;
    $query = "SELECT SUM(`amount`) AS TotalPayment FROM `partial_payments` WHERE `close_date` = 0 AND `FileNo` = '$fileNo'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_hub_raqam_entry_from_tiffin_size($from_date, $to_date, $fileNo) {
    global $database;
    $query = "SELECT `timestamp`, `new_tiffin_size`, `new_fmb_hub` FROM `tiffin_size` WHERE `FileNo` = '$fileNo' AND `timestamp` BETWEEN '$from_date' AND '$to_date' ORDER BY `timestamp` ASC";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_last_hub_raqam_entry_from_tiffin_size($fileNo) {
    global $database;
    $query = "SELECT `timestamp`, `new_tiffin_size`, `new_fmb_hub` FROM `tiffin_size` WHERE `FileNo` = '$fileNo' ORDER BY `timestamp` DESC LIMIT 0,1";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_hub_raqam_from_tiffin_cost($timestamp, $tiffin_size) {
    global $database;
    $query = "SELECT * FROM `tiffin_cost` INNER JOIN `tiffin_details` ON tiffin_details.id = tiffin_cost.size_id WHERE `month` = '$timestamp' AND `size` = '$tiffin_size'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_hub_raqam_entry_from_tiffin_size_DESC($fileNo) {
    global $database;
    $query = "SELECT `timestamp`, `new_tiffin_size`, `new_fmb_hub` FROM `tiffin_size` WHERE `FileNo` = '$fileNo' ORDER BY `timestamp` DESC LIMIT 0,1";
    //echo $query;
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_previous_timestamp($timestamp, $fileNo) {
    global $database;
    $query = "SELECT `timestamp`, `new_tiffin_size`, `new_fmb_hub` FROM `tiffin_size` WHERE `FileNo` = '$fileNo' AND `timestamp` < '$timestamp' ORDER BY `timestamp` DESC LIMIT 0,1";
    //echo $query;
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_tiffin_cost_for_null($month, $tiffin_size) {
    global $database;
    $query = "SELECT * FROM `tiffin_cost` AS tc INNER JOIN `tiffin_details` AS td ON td.id = tc.size_id WHERE `month` = '$month' AND `size` = '$tiffin_size'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_last_receipt_no($fileNo) {
    global $database;
    $query = "SELECT * FROM `fmb_receipt_hub` WHERE `FileNo` = '$fileNo' ORDER BY `id` DESC LIMIT 0,1";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_settlement_receipts() {
    global $database;
    $query = "SELECT * FROM `fmb_receipt_hub` WHERE `settlement` = 'Y'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_by_date_hub_receipts($from_date, $to_date, $tanzeem){
    global $database;
    
    $query = "SELECT * FROM `fmb_receipt_hub` WHERE `date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cash'";
    $result_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT IFNULL(SUM(amount),0) total FROM `fmb_receipt_hub` WHERE `date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cash'";
    $result_sum_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT * FROM `fmb_receipt_hub` WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_cheque = $database->query_fetch_full_result($query);

    $query = "SELECT IFNULL(SUM(amount),0) total FROM `fmb_receipt_hub` WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_sum_cheque = $database->query_fetch_full_result($query);
    
    return array('cash_receipts'=>$result_cash, 'cash_receipts_total'=>$result_sum_cash[0]['total'], 'cheque_receipts'=>$result_cheque, 'cheque_receipts_total'=>$result_sum_cheque[0]['total']);
  }
  
  public function get_by_date_voln_receipts($from_date, $to_date, $tanzeem){
    global $database;
    
    $query = "SELECT * FROM `receipt` WHERE FROM_UNIXTIME(`timestamp`) BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `type` LIKE 'cash'";
    $result_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT IFNULL(SUM(amount),0) total FROM `receipt` WHERE FROM_UNIXTIME(`timestamp`) BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `type` LIKE 'cash'";
    $result_sum_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT * FROM `receipt` WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_cheque = $database->query_fetch_full_result($query);

    $query = "SELECT IFNULL(SUM(amount),0) total FROM `receipt` WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_sum_cheque = $database->query_fetch_full_result($query);
    
    return array('cash_voln_receipts'=>$result_cash, 'cash_voln_receipts_total'=>$result_sum_cash[0]['total'], 'cheque_voln_receipts'=>$result_cheque, 'cheque_voln_receipts_total'=>$result_sum_cheque[0]['total']);
  }
  
  public function get_by_date_credit_vouchers($from_date, $to_date, $tanzeem){
    global $database;
    
    $query = "SELECT * FROM `credit_voucher` WHERE FROM_UNIXTIME(`timestamp`) BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cash'";
    $result_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT IFNULL(SUM(amount),0) total FROM `credit_voucher` WHERE FROM_UNIXTIME(`timestamp`) BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cash'";
    $result_sum_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT * FROM `credit_voucher` WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_cheque = $database->query_fetch_full_result($query);

    $query = "SELECT IFNULL(SUM(amount),0) total FROM `credit_voucher` WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_sum_cheque = $database->query_fetch_full_result($query);
    
    return array('cash_credit_vouchers'=>$result_cash, 'cash_credit_vouchers_total'=>$result_sum_cash[0]['total'], 'cheque_credit_vouchers'=>$result_cheque, 'cheque_credit_vouchers_totals'=>$result_sum_cheque[0]['total']);
  }
  
  public function get_by_date_acct_bills($from_date, $to_date, $tanzeem){
    global $database;
    
    $query = "SELECT ab.*, c.ShopName FROM `account_bill` ab LEFT JOIN companies c ON ab.name = c.id WHERE FROM_UNIXTIME(`timestamp`) BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cash'";
    $result_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT IFNULL(SUM(amount),0) total FROM `account_bill` WHERE FROM_UNIXTIME(`timestamp`) BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cash'";
    $result_sum_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT ab.*, c.ShopName FROM `account_bill` ab LEFT JOIN companies c ON ab.name = c.id WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_cheque = $database->query_fetch_full_result($query);

    $query = "SELECT IFNULL(SUM(amount),0) total FROM `account_bill` WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_sum_cheque = $database->query_fetch_full_result($query);
    
    return array('cash_account_bills'=>$result_cash, 'cash_account_bills_total'=>$result_sum_cash[0]['total'], 'cheque_account_bills'=>$result_cheque, 'cheque_account_bills_totals'=>$result_sum_cheque[0]['total']);
  }
  
  public function get_by_date_debit_vouchers($from_date, $to_date, $tanzeem){
    global $database;
    
    $query = "SELECT dv.*, c.ShopName FROM `debit_voucher` dv LEFT JOIN companies c ON dv.name = c.id WHERE FROM_UNIXTIME(`timestamp`) BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cash'";
    $result_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT IFNULL(SUM(amount),0) total FROM `debit_voucher` WHERE FROM_UNIXTIME(`timestamp`) BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cash'";
    $result_sum_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT dv.*, c.ShopName FROM `debit_voucher` dv LEFT JOIN companies c ON dv.name = c.id WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_cheque = $database->query_fetch_full_result($query);

    $query = "SELECT IFNULL(SUM(amount),0) total FROM `debit_voucher` WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_sum_cheque = $database->query_fetch_full_result($query);
    
    return array('cash_debit_vouchers'=>$result_cash, 'cash_debit_vouchers_total'=>$result_sum_cash[0]['total'], 'cheque_debit_vouchers'=>$result_cheque, 'cheque_debit_vouchers_totals'=>$result_sum_cheque[0]['total']);
  }
  
  public function get_by_date_direct_purchases($from_date, $to_date, $tanzeem){
    global $database;
    
    $query = "SELECT dp.*, c.ShopName FROM `direct_purchase` dp LEFT JOIN companies c ON dp.name LIKE c.id WHERE FROM_UNIXTIME(`timestamp`) BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cash'";
    $result_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT IFNULL(SUM(amount),0) total FROM `direct_purchase` WHERE FROM_UNIXTIME(`timestamp`) BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cash'";
    $result_sum_cash = $database->query_fetch_full_result($query);
    
    $query = "SELECT dp.*, c.ShopName FROM `direct_purchase` dp LEFT JOIN companies c ON dp.name LIKE c.id WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_cheque = $database->query_fetch_full_result($query);

    $query = "SELECT IFNULL(SUM(amount),0) total FROM `direct_purchase` WHERE `cheque_clear_date` BETWEEN '$from_date%' AND '$to_date%' AND `cancel` = 0 AND `payment_type` LIKE 'cheque' AND `cheque_clear` = 1";
    $result_sum_cheque = $database->query_fetch_full_result($query);
    
    return array('cash_direct_purchases'=>$result_cash, 'cash_direct_purchases_total'=>$result_sum_cash[0]['total'], 'cheque_direct_purchases'=>$result_cheque, 'cheque_direct_purchases_totals'=>$result_sum_cheque[0]['total']);
  }
  
  public function get_daily_rcpt_between_days($from = FALSE, $to = FALSE, $tanzeem = FALSE) {
    global $database;
    $query = "SELECT hr.id, hr.name, hr.FileNo, hr.payment_type, hr.amount, hr.cancel, hr.year, f.Mohallah FROM `fmb_receipt_hub` hr INNER JOIN `family` f ON f.FileNo = hr.FileNo";
    $condition = array();
    $update = '';
    if ($tanzeem && strtoupper($tanzeem) != 'ALL' && $tanzeem != '')
      $condition[] = "f.`Mohallah` = '$tanzeem'";
    if ($from && $to)
      $condition[] = "`date` BETWEEN '$from' AND '$to'";
    if (count($condition) > 0)
      $update = " WHERE " . implode(" AND ", $condition);
    $query .= $update;

    $result = $database->query_fetch_full_result($query);
    $total = 0;
    $table = '<table class="table table-bordered table-condensed table-hover"><thead><tr><th>Receipt No.</th><th>'.THALI_ID.'</th><th>Name</th><th>Year</th><th>Payment Type</th><th class="text-right">Amount</th></tr></thead><tbody>';
    if ($result) {
      $i = 1;
      foreach ($result as $receipt) {
        if ($receipt['cancel'] == 0) {
          $total += $receipt['amount'];
          $red = '';
          $opening_strike = '';
          $end_strike = '';
        } else {
          $red = ' class="alert-danger"';
          $opening_strike = '<strike>';
          $end_strike = '</strike>';
        }

        $table .= '<tr' . $red . '><td>' . $opening_strike . '' . $receipt['id'] . '' . $end_strike . '</td><td>' . $opening_strike . '' . $receipt['FileNo'] . '' . $end_strike . '</td><td>' . $opening_strike . '' . $receipt['name'] . '' . $end_strike . '</td><td>' . $opening_strike . '' . $receipt['year'] . '' . $end_strike . '</td><td>' . $opening_strike . '' . ucfirst($receipt['payment_type']) . '' . $end_strike . '</td><td><span class="pull-right">' . $opening_strike . '' . 'Rs. ' . number_format($receipt['amount'], 2, '.', ',') . '' . $end_strike . '</span></td>';
      }
      if (isset($total) && $total > 0) {
        $table .= '<tr><td colspan="5" style="text-align: right"><strong>Total :</strong></td><td class="text-right">' . 'Rs. ' . number_format($total, 2, '.', ',') . '</td></tr>';
      }
    } else {
      $table .= '<tr><td colspan="6" class="alert-danger">Sorry! no receipts found.</td></tr>';
    }
    $table .= '</tbody></table>';
    return $table;
  }
  
  function get_daily_receipt($tblName, $fromYMD = FALSE, $toYMD = FALSE, $fromTime = FALSE, $toTime = FALSE, $colName = 'timestamp') {
    global $database;
    $date = $fromYMD && $toYMD ? "`$colName` BETWEEN '$fromYMD' AND '$toYMD'" : "`$colName` BETWEEN '$fromTime' AND '$toTime'";
    $paid = ($tblName == 'direct_purchase' || $tblName == 'account_bill') ? "AND `paid` = '1'" : '';
    $type = $tblName == 'receipt' ? 'type' : 'payment_type';
    $subQuery = "(SELECT ShopName FROM `companies` c WHERE c.id LIKE t.name) name";
    $subQuery = ($tblName == 'account_bill' || $tblName == 'direct_purchase' || $tblName == 'debit_voucher') ? '*, '.$subQuery : '*';
    $query = "SELECT $subQuery FROM `$tblName` t WHERE t.`cancel` = '0' && $date AND (`$type` LIKE 'cash' OR `$type` LIKE '') $paid";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function get_daily_bank_receipt($tblName, $fromYMD, $toYMD) {
    global $database;
    $type = $tblName == 'receipt' ? 'type' : 'payment_type';
    $subQuery = "IFNULL((SELECT ShopName FROM `companies` c WHERE c.id LIKE t.name), name) name";
    $subQuery = ($tblName == 'account_bill' || $tblName == 'direct_purchase' || $tblName == 'debit_voucher') ? '*, '.$subQuery : '*';
    $query = "SELECT $subQuery FROM `$tblName` t WHERE t.`cancel` = '0' && `cheque_clear_date` BETWEEN '$fromYMD' AND '$toYMD' AND `$type` IN ('cheque', 'neft')";
    //echo $query.'<br>';
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_daily_vol_rcpt_between_days($from, $to) {
    global $database;
    $query = "SELECT * FROM `receipt` WHERE `timestamp` BETWEEN '$from' AND '$to'";
    $result = $database->query_fetch_full_result($query);
    $total = 0;
    $table = '<table class="table table-bordered table-condensed table-hover"><thead><tr><th>Id</th><th>'.THALI_ID.'</th><th>Name</th><th>Payment Type</th><th class="text-right">Amount</th></tr></thead><tbody>';
    if ($result) {
      $i = 1;
      foreach ($result as $receipt) {
        $total += $receipt['amount'];
        $table .= '<tr><td>' . $receipt['id'] . '</td><td>' . $receipt['FileNo'] . '</td><td>' . $receipt['hubber_name'] . '</td><td>' . ucfirst($receipt['type']) . '</td><td><span class="pull-right">' . 'Rs. ' . number_format($receipt['amount'], 2, '.', ',') . '</span></td>';
      }
      if (isset($total) && $total > 0) {
        $table .= '<tr><td colspan="4" style="text-align: right"><strong>Total :</strong></td><td colspan="2" class="text-right">' . 'Rs. ' . number_format($total, 2, '.', ',') . '</td></tr>';
      }
    } else {
      $table .= '<tr><td colspan="6" class="alert-danger">Sorry! no receipts found.</td></tr>';
    }
    $table .= '</tbody></table>';
    return $table;
  }

  public function tiffin_count($from = FALSE, $to = FALSE) {
    global $database;
    $query = "SELECT * FROM `daily_entry` ORDER BY `FileNo` DESC";
    if ($from && $to) {
      $query = "SELECT * FROM `tiffin_size` WHERE `timestamp` BETWEEN '$from' AND '$to' ORDER BY `FileNo` DESC";
      if ($from == '' && $to == '') {
        $query = "SELECT * FROM `tiffin_size` ORDER BY `FileNo` DESC";
      }
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function add_voucher($cmd, $name, $acct_heads, $description, $amount, $type, $bankname, $cheque_no, $cheque_date, $userid, $form_id, $contactPerson = FALSE, $ref_no = FALSE) {
    global $database;
    $timestamp = time();

    if ($cheque_date != '') {
      $date = explode('-', $cheque_date);
      $cheque_date2 = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    } else
      $cheque_date2 = '0';

    ($cmd == 'credit') ? $tbl = 'credit_voucher' : $tbl = 'debit_voucher';
    $query = "INSERT INTO $tbl(`name`, `acct_heads`, `description`, `amount`, `payment_type`, `bank_name`, `cheque_no`, `cheque_date`, `timestamp`, `user_id`) VALUES('$name', '$acct_heads', '$description', '$amount', '$type', '$bankname', '$cheque_no', '$cheque_date2', '$timestamp', '$userid')";
    $result = $database->query($query);
    if ($result)
      $id = $database->get_last_insert_id();
    else
      $id = FALSE;
    
    if($cmd == 'debit') {
      $clear_date = date('Y-m-d');
      $query = "UPDATE `$tbl` SET `contact_person` = '$contactPerson', `ref_no` = '$ref_no', `cheque_clear_date` = '$clear_date' WHERE `id` = '$id'";
      $result = $database->query($query);
    }
    
    if($type == 'neft') {
      $clear_date = date('Y-m-d');
      $query = "UPDATE $tbl SET `cheque_clear` = '1', `cheque_clear_date` = '$clear_date' WHERE `id` = '$id'";
      $database->query($query);
    }
    return $id;
  }

  public function get_credit_total_between_dates($from = FALSE, $to = FALSE) {
    global $database;
    $query = "SELECT SUM(`amount`) AS Amount FROM `credit_voucher` WHERE `cancel` = '0'";
    if ($from && $to) {
      $from = explode('-', $from);
      $from = mktime(0, 0, 0, $from[1], $from[2], $from[0]);
      $to = explode('-', $to);
      $to = mktime(23, 59, 59, $to[1], $to[2], $to[0]);
      $query .= " AND `timestamp` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_debit_total_between_dates($from = FALSE, $to = FALSE) {
    global $database;
    $query = "SELECT SUM(`amount`) AS Amount FROM `debit_voucher` WHERE ";
    if ($from && $to) {
      $from = explode('-', $from);
      $from = mktime(0, 0, 0, $from[1], $from[2], $from[0]);
      $to = explode('-', $to);
      $to = mktime(23, 59, 59, $to[1], $to[2], $to[0]);
      $query .= "`timestamp` BETWEEN '$from' AND '$to' AND ";
    }
    $query .= "`cancel` = '0'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_receipts($page_num, $row_count, $cmd, $from_date = FALSE, $to_date = FALSE) {
    global $database;
    ($cmd == 'credit') ? $tbl = 'credit_voucher' : $tbl = 'debit_voucher';
    if(!$row_count) {
      if ($from_date && $to_date) {
        $from_split = explode('-', $from_date);
        $to_split = explode('-', $to_date);

        $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
        $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

        
      }
      $query = "SELECT *, (SELECT `ShopName` FROM `companies` c WHERE c.id LIKE t.name) shopName FROM $tbl t WHERE `timestamp` BETWEEN '$from' AND '$to' AND `cancel` = 0 ORDER BY `id` DESC";
    } else {
      $limit = ($page_num - 1) * $row_count;

      $query = "SELECT *, (SELECT `ShopName` FROM `companies` c WHERE c.id LIKE t.name) shopName FROM $tbl t";
      if ($from_date && $to_date) {
        $from_split = explode('-', $from_date);
        $to_split = explode('-', $to_date);

        $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
        $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

        $query .= " WHERE `timestamp` BETWEEN '$from' AND '$to'";
      }
      $query .= " ORDER BY `id` DESC LIMIT $limit,$row_count";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_number_voucher($cmd, $from_date = FALSE, $to_date = FALSE) {
    global $database;
    ($cmd == 'credit') ? $tbl = 'credit_voucher' : $tbl = 'debit_voucher';
    $query = "SELECT COUNT(*) AS Total FROM $tbl WHERE `cancel` = 0";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT COUNT(*) AS `Total` FROM $tbl WHERE `timestamp` BETWEEN '$from' AND '$to' AND `cancel` = 0";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Total'];
  }

  public function get_all_for_FMB_DETAILS($fId, $year) {
    global $database;
    $query = "SELECT * FROM `fmb_receipt_hub` WHERE `FileNo` = '$fId' AND `year` = '$year'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_hub_receipt_cheque() {
    global $database;
    $start = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
    $end = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
    $query = "SELECT id, name, amount, bank_name as bank, cheque_date as timestamp FROM `fmb_receipt_hub` WHERE (`payment_type` = 'cheque' OR `payment_type` = 'neft') AND cheque_date BETWEEN '$start' AND '$end' AND cheque_clear = '0' AND `cancel` = '0'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_vol_receipt_cheque() {
    global $database;
    $start = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
    $end = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
    $query = "SELECT id, amount, bankname as bank, hubber_name as name, cheque_date as timestamp FROM `receipt` WHERE type = 'cheque' AND cheque_date BETWEEN '$start' AND '$end' AND cheque_clear = '0'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_voucher($vid, $tbl) {
    global $database;
    $query = "SELECT *, IFNULL((SELECT ShopName FROM `companies` c WHERE c.id LIKE t.name), name) name FROM $tbl t WHERE `id` = '$vid'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }
  
  function daily_cash_entry_exist() {
    global $database;
    $date = date('Y-m-d');
    $query = "SELECT COUNT(*) AS ROW FROM `daily_cash` WHERE `date` = '$date'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['ROW'];
  }

  function add_opening_daily_cash($open_cash, $open_bank, $addi_cash, $addi_bank, $withdraw) {
    global $database;
    $date = date('Y-m-d');
    $query = "INSERT INTO `daily_cash`(`date`, `opening_cash`, `opening_bank`, `additional_cash`, `additional_bank`) VALUES ('$date', '$open_cash', '$open_bank', '$addi_cash', '0') ON DUPLICATE KEY UPDATE `opening_cash` = '$open_cash', `opening_bank` = '$open_bank', `additional_cash` = '$addi_cash', `additional_bank` = '$addi_bank'";
    $result = $database->query($query);
    $date = date('Y-m-d');
    $query = "INSERT INTO `bank_transaction`(`date`, `from_bank`) VALUES ('$date', '$withdraw') ON DUPLICATE KEY UPDATE `from_bank` = '$withdraw'";
    $result = $database->query($query);
    return $result;
  }

  function add_closing_daily_cash($close_cash, $close_bank, $deposit) {
    global $database;
    $date = date('Y-m-d');
    $query = "UPDATE `daily_cash` SET `closing_cash` = '$close_cash', `closing_bank` = '$close_bank' WHERE `date` = '$date'";
    $result = $database->query($query);
    $date = date('Y-m-d');
    $query = "INSERT INTO `bank_transaction`(`date`, `to_bank`) VALUES ('$date', '$deposit') ON DUPLICATE KEY UPDATE `to_bank` = '$deposit'";
    $result = $database->query($query);
    return $result;
  }
  
  function get_withdraw_amount($date) {
    global $database;
    $date = date('Y-m-d');
    $query = "SELECT * FROM `bank_transaction` WHERE `date` LIKE '$date'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }
  
  function self_bank_transaction($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT IFNULL(SUM(`from_bank`), 0) WITHDRAW, IFNULL(SUM(`to_bank`), 0) DEPOSIT FROM `bank_transaction`";
    
    if($start && $end){
      $query .= " WHERE `date` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_all_daily_cash() {
    global $database;
    $query = "SELECT * FROM `daily_cash` dc INNER JOIN `bank_transaction` bt ON dc.`date` = bt.`date` ORDER BY dc.`date` DESC";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }
  
  function get_unpaid_bills_amount($start = FALSE, $end = FALSE){
    global $database;
    $query = "SELECT SUM(amount) Amount FROM account_bill WHERE paid = 0 AND cancel != 1";
    
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    $sum = $result[0]['Amount'];
    return $sum;
  }

  function get_unpaid_direct_purchase_amount($start = FALSE, $end = FALSE){
    global $database;
    $query = "SELECT SUM(amount) Amount FROM direct_purchase WHERE paid = 0 AND cancel != 1";
    
    if($start && $end){
      $query .= " AND `timestamp` BETWEEN '$start' AND '$end'";
    }
    
    $result = $database->query_fetch_full_result($query);
    $sum = $result[0]['Amount'];
    return $sum;
  }
  
  function update_unclear_cheque_data($cheque_no,$date,$table,$id){
    global $database;
    $query = "UPDATE `$table` SET `cheque_clear` = 0, `cheque_clear_date` = '$date' WHERE `id` = '$id' AND `cheque_clear` = '1' AND `cheque_no` = '$cheque_no'";
    $result = $database->query($query);
    return $result;
  }
  
  function update_cancel_receipt_data($cheque_no,$date,$table,$id){
    global $database;
    $query = "UPDATE `$table` SET `cancel` = '1', `cancel_ts` = '$date' WHERE `id` = '$id' AND `cancel` = '0' AND `cheque_no` = '$cheque_no'";
    $result = $database->query($query);
    return $result;
  }
  
  function get_unclear_cheque_and_cancel_receipt_data($cheque_no,$table){
    global $database;
    $query = "SELECT * FROM `$table` WHERE `cheque_no` LIKE '$cheque_no'"; 
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  /*
  function get_unpaid_debit_voucher_amount(){
    global $database;
    
    $query = "SELECT SUM(amount) Amount FROM debit_voucher WHERE paid = 0 AND cancel != 1";
    
    $result = $database->query_fetch_full_result($query);
    
    $sum = $result['Amount'];
    
    return $sum;
  }
   * */

  //getter and setter
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

}

?>
