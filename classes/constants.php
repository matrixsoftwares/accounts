<?php
// Database Constants
define('DB_API_SERVER','localhost');
//define('DB_API_USER','matrixso_accounts');
define('DB_API_USER','root');
//define('DB_API_PASS', 'accounts@1234');
define('DB_API_PASS', 'matrix2');
define('DB_API_NAME','matrixso_accounts');

define('PAID', 1);
define('PENDING', 2);
define('CANCEL', 3);

define('PREFIX', 'prefix');
define('FIRST_NAME', 'first_name');
define('FATHER_PREFIX', 'father_prefix');
define('FATHER_NAME', 'father_name');
define('SURNAME', 'surname');
define('USER_TYPE','user_type');
define('USER_ID','user_id');
define('LOGGED_IN','logged_in');
define('ORIGINAL','original');
define('EXPECTED','expected');
define('FILE','fId');
define('HUB','hub');
define('PAID_TILL','till');
define('FORM_ID1','form_id1');
define('FORM_ID2','form_id2');
define('PAID_UPTO','upto');
define('MONTHS','months');
define('TYPE','type');
define('BANK','bank');
define('CHEQUE','cheque');
define('DATE','date');
define('FILENO','FileNo');
define('PARTIAL_AMOUNT','partial_amount');
define('NAME','name');
define('MONTHLY_HUB','monthly_hub');
define('TOTAL_PARTIAL_AMOUNT','total_partial_amount');
define('ON_HAND', 'on_hand');
define('REFERENCE_NO','reference_no');

define('FULLNAME', 'fullname');
define('USERNAME', 'username');
define('SUCCESS_MESSAGE','success_message');
define('ERROR_MESSAGE','error_message');
define('USER_RIGHTS','user_rights');
define('ALL_RIGHTS','all_rights');

// User rights
//Accounts
define('ACCOUNTS_ENTRY', 0);
define('KEY_ACCOUNTS_ENTRY', 1);
define('ACCOUNTS_REPORTS', 1);
define('KEY_ACCOUNTS_REPORTS', 2);
define('ACCOUNTS_BALANCESHEET', 7);
define('KEY_ACCOUNTS_BALANCESHEET', 128);

//Profile
define('PROFILE_ENTRY', 2);
define('KEY_PROFILE_ENTRY', 4);
define('PROFILE_REPORTS', 3);
define('KEY_PROFILE_REPORTS', 8);

//Maedat
define('INVENTORY_ENTRY', 4);
define('KEY_INVENTORY_ENTRY', 16);
define('INVENTORY_REPORTS', 5);
define('KEY_INVENTORY_REPORTS', 32);

//Reports
define('REPORTS', 6);
define('KEY_REPORTS', 64);

//Takhmeen
define('TAKHMEEN', 8);
define('KEY_TAKHMEEN', 256);

//Idarah
define('IDARAH_REPORTS', 9);
define('KEY_IDARAH_REPORTS', 512);

//Hub Pending Report
define('HUB_PENDING_REPORTS', 10);
define('KEY_HUB_PENDING_REPORTS', 1024);

//MESSAGE CONSTANTS
define('SENDER_ID', 'HUSAMI');
define('MSG_USER_NAME', 'husami');
define('MSG_PASSWORD', 'husami');
define('MSG_URL', '');

//tanzeem name
define('TANZEEM_NAME', 'Apple Group - Surat');
define('THALI_ID', 'Thali ID');
define('YEAR', 'Year'); //TAKHMEEN YEAR
define('SHOW_REF_FMB_HUB', TRUE);
define('USE_CALENDAR', 'Hijri'); //valid value Greg, Hijri
define('DIFF_MONTH_CALC', TRUE); // define whether to use different calculation for moharram 
define('BACKUP_EMAIL', 'apple.tmjamat@gmail.com');
define('FROM_EMAIL', 'noreply@tmjamat.com');
?>
