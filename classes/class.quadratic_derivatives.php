<?php

class Mtx_Quad_derivative {

  public $a;
  public $b;
  public $c;
  public $step = 0.01;

  public function Mtx_Quad_derivative(array $ary_x, array $ary_y, $step) {
    $this->a = (((($ary_y[2] * $ary_x[0]) - ($ary_y[0] * $ary_x[2])) * ($ary_x[0] - $ary_x[1])) - ((($ary_y[1] * $ary_x[0]) - ($ary_y[0] * $ary_x[1])) * ($ary_x[0] - $ary_x[2]))) / (((($ary_x[2] * $ary_x[2] * $ary_x[0]) - ($ary_x[0] * $ary_x[0] * $ary_x[2])) * ($ary_x[0] - $ary_x[1])) - ((($ary_x[1] * $ary_x[1] * $ary_x[0]) - ($ary_x[0] * $ary_x[0] * $ary_x[1])) * ($ary_x[0] - $ary_x[2])));
    $this->c = (((($ary_y[1] * $ary_x[0]) - ($ary_y[0] * $ary_x[1])) * ($ary_x[0] - $ary_x[2])) - ($this->a * (($ary_x[1] * $ary_x[1] * $ary_x[0]) - ($ary_x[0] * $ary_x[0] * $ary_x[1])) * ($ary_x[0] - $ary_x[2]))) / (($ary_x[0] - $ary_x[1]) * ($ary_x[0] - $ary_x[2]));
    $this->b = ($ary_y[0] - ($this->a * $ary_x[0] * $ary_x[0]) - $this->c) /$ary_x[0];
    $this->step = $step;
  }
  
  public function get_a(){
    return $this->a;
  }
  
  public function get_b(){
    return $this->b;
  }
  
  public function get_c(){
    return $this->c;
  }

  public function get_qty_y($x){
    return number_format(ceil((($this->a * $x *$x) + ($this->b * $x) + $this->c) / $this->step) * $this->step,3);
  }
  
}

?>