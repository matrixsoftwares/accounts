<?php
include('session.php');
require_once('classes/class.database.php');
require_once("classes/class.receipt.php");
require_once('classes/class.menu.php');

$cls_receipt = new Mtx_Receipt();
$cls_menu = new Mtx_Menu();

if (isset($_POST['add_item'])) {
  $result = $cls_receipt->add_item_in_inventory(ucfirst($_POST['item']), $_POST['category'], floatval($_POST['quantity']), $_POST['unit'], floatval($_POST['price']), floatval($_POST['step']));
  if ($result){
    $_SESSION[SUCCESS_MESSAGE] = 'Item added successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Something went wrong';
  }
}

$title = 'Add Item';
$active_page = 'menu';
$categories = $cls_menu->get_inventory_category();

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Maedat</a></li>
        <li><a href="#">Inventory</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div></div>

            <div class="form-group">
              <label class="control-label col-md-4">Item Name</label>
              <div class="col-md-4">
                <input type="text" id="item" name="item" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Category Name</label>
              <div class="col-md-4">
                <select name="category" class="form-control">
                  <option value="">--Select Category--</option>
                  <?php foreach ($categories as $c) { ?>
                    <option value="<?php echo $c['id']; ?>"><?php echo $c['name']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Quantity</label>
              <div class="col-md-4">
                <input type="text" id="quantity" name="quantity" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Price</label>
              <div class="col-md-4">
                <input type="text" id="price" name="price" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Unit</label>
              <div class="col-md-4">
                <select id="unit" name="unit" class="form-control">
                  <option value="kgs">Kilogram</option>
                  <option value="ltrs">Litre</option>
                  <option value="pcs">Pieces</option>
                  <option value="pkts">Packet</option>
                  <option value="boxes">Box</option>
                  <option value="btls">Bottle</option>
                  <option value="carba">Carba</option>
                  <option value="tin">Tin</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Step</label>
              <div class="col-md-4">
                <input type="text" id="step" name="step" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4"></label>
              <button class="btn btn-success" type="submit" name="add_item" id="add">Add</button>
            </div>

          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          $('#add').click(function() {
            var item = $('#item').val();
            var qty = $('#quantity').val();
            var error = '';
            var validate = true;
            if (item == '')
            {
              error += 'Please enter Item Name\n';
              validate = false;
            }
            if (qty == '')
            {
              error += 'Please enter quantity\n';
              validate = false;
            }
            if (validate == false)
            {
              alert(error);
              return validate;
            }
          });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>