<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.menu.php');

$cls_menu = new Mtx_Menu();

if (isset($_GET['cid'])) {
  $cid = $_GET['cid'];
  $name = $cls_menu->get_inventory_category($cid);
} else $cid = FALSE;

if (isset($_POST['add_category'])) {
  $category_name = ucfirst(strtolower($_POST['category']));
  $add_category = $cls_menu->add_inventory_category($category_name);
  if ($add_category) {
    $_SESSION[SUCCESS_MESSAGE] = 'Category added successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while adding new category!';
  }
}

if (isset($_POST['update_category'])) {
  $category_name = ucfirst(strtolower($_POST['category']));
  $update = $cls_menu->update_inventory_category($cid, $category_name);
  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Category updated successfully.';
    header('Location: inv_category.php');
    exit;
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while Updating!';
  }
}
$categories = $cls_menu->get_inventory_category();
$title = 'Add \ Display Inventory Category';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-6">
            <form method="post" role="form" class="form-horizontal">
              <div class="form-group">
                <label class="control-label col-md-5">Category Name</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="category" id="category" value="<?php echo ($cid) ? $name : '';?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5">&nbsp;</label>
                <div class="col-md-7">
                  <?php if (!$cid) { ?>
                  <input type="submit" class="btn btn-success" name="add_category" id="add_category" value="Add Category">
                  <?php } else { ?>
                  <input type="submit" class="btn btn-success" name="update_category" id="update_category" value="Update">
      <?php } ?>
                </div>
              </div>
            </form>
          </div>
          <?php
          if ($categories) {
            ?>
            <div class="col-md-6">
              <table class="table table-hover table-condensed table-bordered">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Category Name</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="categories">
                  <?php
                  $i = 1;
                  foreach ($categories as $category) {
                    ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $category['name']; ?></td>
                      <td><a href="?cid=<?php echo $category['id'] ?>" class="btn btn-success btn-xs">Update</a> | <input type="button" name="delete" id="delete-<?php echo $category['id'] ?>" class="btn btn-danger btn-xs delete_category" value="Delete"></td>
                    </tr>
        <?php } ?>
                </tbody>
              </table>
            </div>
      <?php } ?>
        </div>
        <!-- /Center Bar -->
        <script>
          $('.delete_category').click(function(e){
            e.preventDefault();

            var answer = confirm('Are you sure you want to delete this item?');
            if (answer) {
              myApp.showPleaseWait();
              var itemId = this.id.split('-');
              jQuery.ajax({
                type: "POST", // HTTP method POST or GET
                url: "ajax.php", //Where to make Ajax calls
                data: "cmd=del_inventory_category&cid=" + itemId[1], //Form variables
                success:function(data,status){
                  //on success, show new data.
                  var category = $.parseJSON(data);
                  var tbody = '';
                  var i = 1;
                  $.each(category, function(){
                    var buttons = '<a href="inv_category.php?cid='+this.id+'" class="btn btn-success btn-xs">Update</a> | <input type="button" name="delete" id="delete-'+this.id+'" class="btn btn-danger btn-xs delete_category" value="Delete">';
                    tbody += '<tr><td>'+i+'</td><td>'+this.name+'</td><td>'+buttons+'</td></tr>';
                    i++;
                  });
                  $('#categories').html(tbody);
                },
                error:function (xhr, ajaxOptions, thrownError){
                  //On error, we alert user
                  alert(thrownError);
                }
              });
              myApp.hidePleaseWait();
            } else {
              alert('Operation Canceled!');
            }
           });

          var myApp;
          myApp = myApp || (function () {
          var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="asset/img/loader.gif"></div></div></div></div>');
          return {
              showPleaseWait: function() {
                  pleaseWaitDiv.modal();
              },
              hidePleaseWait: function () {
                  pleaseWaitDiv.modal('hide');
              },

          };
      })();
        </script>
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>