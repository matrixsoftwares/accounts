<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.product.php');
$cls_product = new Mtx_Product();
$user_id = $_SESSION[USER_ID];

if (isset($_POST['return'])) {
  $result = $cls_product->return_inventory($_POST['item'], floatval($_POST['qty']), $_POST['date']);
  if($result){
    $_SESSION[SUCCESS_MESSAGE] = "Item added successfully";
  } else{
    $_SESSION[ERROR_MESSAGE] = "Errors encountered while processing return inventory item";
  }
}
$items = $cls_product->get_all_ingredients();
$title = 'Return Inventory';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Maedat</a></li>
        <li><a href="#">Inventory</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <div class="form-group">
              <label class="control-label col-md-4">Date</label>
              <div class="col-md-3">
                <input type="date" name="date" class="form-control" id="date" placeholder="" value="<?php echo date('Y-m-d'); ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Item</label>
              <div class="col-md-3">
                <select class="form-control" name="item" id="item">
                  <option value="0">--select one--</option>
                  <?php
                  if ($items) {

                    foreach ($items as $item) {
                      ?>
                      <option value="<?php echo $item['id']; ?>"><?php echo $item['name'] . ' ' . $item['unit']; ?></option>
                      <?php
                    }
                  }
                  ?>

                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Quantity</label>
              <div class="col-md-3">
                <input type="text" name="qty" id="qty" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">&nbsp;</label>
              <div class="col-md-3">
                <input type="submit" name="return" id="return" value="Return Inventory"  class="btn btn-success">
              </div>
            </div>
          </form>
        </div>
      </div>
        <!-- /Center Bar -->
        <script>
          $('#return').click(function() {
            var menu = $('#item').val();
            var qty = $('#qty').val();
            var errors = '';
            var validate = true;
            if(menu == '0') {
              errors += 'Please select item name\n';
              validate =  false;
            }
            if(qty == '') {
              errors += 'Please enter quantity';
              validate =  false;
            }
            if(validate == false) {
              alert(errors);
              return validate;
            }
          });
        </script>
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>
