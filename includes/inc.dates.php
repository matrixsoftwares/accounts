<form method="GET" role="form" class="form-horizontal">
    <div></div>
    <div class="col-md-12">
        <label class="col-md-2 control-label">From Date</label>
        <div class="col-md-3">
            <input type="date" name="from_date" class="form-control" id="from_date" value="<?php echo $from_date; ?>" placeholder="From Date">
        </div>
        <label class="col-md-2 control-label">To Date</label>
        <div class="col-md-3">
            <input type="date" name="to_date" class="form-control" id="to_date" value="<?php echo $to_date; ?>" placeholder="To Date">
        </div>
        <input type="submit" class="btn btn-success" name="search" id="search" value="Search">
        <?php if(isset($btn_print_link)) { ?>
        <a href="<?php echo $btn_print_link; ?>" target="blank" class="btn btn-primary validate <?php echo !$post ? 'disabled' : ''; ?>" id="print">Print</a>
        <?php
            }
          if(isset($btn_csv_download_link)) {
        ?>
        <input type="submit" class="btn btn-success" name="download_csv_file" id="download_csv_file" value="Download CSV">
        <?php } ?>
    </div>
</form>

<script>
    $('#search').click(function() {
        var from = $('#from_date').val();
        var to = $('#to_date').val();
        var errors = [];
        var key = 0;
        if (from === '')
            errors[key++] = 'from';
        if (to === '')
            errors[key++] = 'to';
        if (errors.length) {
            alert('Please select ' + errors.join(' & ') + ' date to proceed...');
            return false;
        }
    });
</script>