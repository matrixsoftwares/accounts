<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 3.0.0
  </div>
  <strong>Copyright &copy; 2014-2015 <a target="_blank" href="http://www.matrixsoftwares.com/">Matrix Software Solutions</a>.</strong> All rights reserved.
</footer>

    <aside class="control-sidebar control-sidebar-dark">
<!--      <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      </ul>-->
      
      <div class="tab-content">
        <div class="tab-pane" style="display: block;" id="control-sidebar-home-tab">
          <h5 class="control-sidebar-heading">Most Visited Pages</h5>
            <ul class="control-sidebar-menu">
              ============
              Receipts
              ============
              <?php
                echo get_right_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'voluntary_rcpt.php', 'Receipt');
                echo get_right_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'credit_voucher.php', 'Credit Voucher');
              ?>
              <br>
              ===========
              Payments
              ============
              <?php
                echo get_right_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'add_bill.php', 'Bill Payment');
                echo get_right_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'direct_purchase.php', 'Direct Purchase');
                echo get_right_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'debit_voucher.php', 'Debit Voucher');
              ?>
            </ul>
        </div>
      </div>
    </aside>

      <div class="control-sidebar-bg"></div>
    </div><!-- wrapper -->
    
    <script>
      var AdminLTEOptions = {
        sidebarExpandOnHover: true,
        enableBSToppltip: true
      };
    </script>
    <script src='asset/dist/js/app.js' type='text/javascript'></script>
    
    <script>
      setTimeout(function() {
        $(".alert-message").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
      }, 5000);

      $('.carousel').carousel({
        interval: 5000
      });

    </script>
    
  </body>
</html>