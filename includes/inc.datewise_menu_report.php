        <?php
        if(!$print) $print_limit = 1;
        for($tbl = 0; $tbl < $print_limit; $tbl++) {?>
        <table class="table table-hover table-condensed table-bordered">
          <thead>
            <!--tr>
              <th colspan="4" class="text-center">Datewise Menu <?php echo ($print) ? 'report between '.date('d M, Y', strtotime($from_date)) . ' & ' . date('d M, Y', strtotime($to_date)) : '';?></th>
            </tr-->
            <tr>
              <th>Sr.</th>
              <th>Date</th>
              <th>Menu Name</th>
              <th>Person Name(s)</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if ($menus) {
              $i = 1;
              foreach ($menus as $menu) {
                $ts = $menu['timestamp'];
                $day = date('Y-m-d', $ts);
                $ary_names = array();
                $result = $cls_family->get_takhmeen_dates($day);
                if ($result) {
                  foreach ($result as $data) {
                    $name = $cls_family->get_name($data['FileNo']);
                    $ary_names[] = $name . '<br>';
                  }
                }
                ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><?php echo date('D d, F', $ts); ?></td>
                  <td><?php // $menu_name = ($menu['custom_menu'] != '') ? $menu['custom_menu'] : $cls_menu->get_base_menu_name($menu['menu_id']);
                  $menu_name = $cls_menu->get_base_menu_name($menu['menu_id']);
                  echo ucwords(strtolower($menu_name)); ?></td>
                  <td><?php echo implode('', $ary_names);?></td>
                </tr>
              <?php } ?>
                
                  <?php if(!$print) {?>
          <tr>
            <td colspan="4"><textarea id="footer_msg" rows="2" class="form-control" placeholder="Enter message to display footer at print page"></textarea></td></tr>
                  <?php } else { if($msg) {?>
          <tr><td colspan="4"><?php echo ucfirst($msg); ?></td></tr>
                  <?php }} ?>
                
            <?php } else {
              ?>
              <tr>
                <td colspan="4" class="alert-danger">Sorry! no menus found.</td>
              </tr>
  <?php } ?>
          </tbody>
        </table>
        <?php } ?>
