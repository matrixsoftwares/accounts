     <script>
      $(function() {
        $("#tag").autocomplete({
          source: "autocomplete.php",
        });
        $('.ui-autocomplete').css('z-index', '99999');
      });
    </script>
    <form class="form-horizontal" name="searching" id="searching" role="form" method="post" action="">
          <div class="form-group">
            <div class="col-md-12">
              <input type="text" name="fmb_id" class="form-control" id="fmb_id" placeholder="By <?php echo THALI_ID; ?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <input type="text" class="form-control" id="tag" name="name" placeholder="By Name">
            </div>
          </div>
      <div class="form-group">
            <div class="col-md-12">
              <button class="btn btn-success btn-block" id="search_frm" data-toggle="modal" data-target="#myModal">Search</button>
            </div>
          </div>
        </form>
    
    
<script>
  $('#search_frm').click(function(){
    var fly_id = $('#fmb_id').val();
    var fly_name = $('#tag').val();
    if((fly_id === '') && (fly_name === '')){
      alert('Please enter either FMB ID or Name');
      return false;
    } else {
      myApp.showPleaseWait();
      $.ajax({
        url     : 'ajax.php',
        type    : 'GET',
        data    : 'id='+fly_id+'&name='+fly_name+'&cmd=SEARCHER',
        success : function(data){
          $('#searched_body').empty();
          if(data != '0'){
            var fly = $.parseJSON(data);
            var text = '';
            $.each(fly, function(){
              var File_data = div_element_create('<?php echo THALI_ID; ?>:', '<a href="update_family.php?cmd=show&family_id='+this.FileNo+'">'+this.FileNo+'</a>');
              var fly_hof = div_element_create('Name:', '<p class="form-control-static pull-left">'+this.Full_Name+'</p>');
              var fly_Ejamat_ID = div_element_create('Ejamat ID:', '<p class="form-control-static pull-left">'+this.ejamatID+'</p>');
              var fly_mob_no = div_element_create('Contact No:', '<p class="form-control-static pull-left">'+this.MPh+'</p>');
              var fly_cat = div_element_create('Category:', '<p class="form-control-static pull-left">'+this.category+'</p>');
              if(this.RECEIPT === 'Not found.' || this.RECEIPT === null)
                var rcpt_id = div_element_create('Last receipt no:', '<p class="form-control-static pull-left">'+this.RECEIPT+'</p>');
              else
                var rcpt_id = div_element_create('Last receipt no:', '<p class="form-control-static pull-left"><a href="print_hub_receipt.php?id='+this.RECEIPT+'" target="_blank">'+this.RECEIPT+'</a></p>');
              var fly_open = div_element_create('Account Open:', '<p class="form-control-static pull-left">'+this.ACCT_OPEN+'</p>', ' alert-success');
              var fly_close = div_element_create('Account Close:', '<p class="form-control-static pull-left">'+this.ACCT_CLOSE+'</p>', ' alert-danger');
              text += File_data+fly_hof+fly_Ejamat_ID+fly_mob_no+fly_cat+rcpt_id+fly_open+fly_close+'<hr>';
            });
            $('#searched_hof_detail_body').hide();
            $('#result_searched_hof_detail_body').html(text).show();
            myApp.hidePleaseWait();
          } else {
            $('#searched_hof_detail_body').hide();
            $('#result_searched_hof_detail_body').text('No results found.').addClass('alert-danger').show();
            myApp.hidePleaseWait();
            return false;
          }
        }
      });
      return false;
    }
  });
  var myApp;
    myApp = myApp || (function() {
      var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="asset/img/loader.gif"></div></div></div></div>');
      return {
        showPleaseWait: function() {
          pleaseWaitDiv.modal();
        },
        hidePleaseWait: function() {
          pleaseWaitDiv.modal('hide');
        },
      };
    })();
  
  function div_element_create(label_name, txt, class_name) {
    class_name = typeof class_name !== 'undefined' ? class_name : '';
    var div = '<div class="form-group'+class_name+'"><label class="control-lable col-md-3">'+label_name+'</label><div class="col-md-9">'+txt+'</div></div><div class="clearfix"></div>';
    return div;
  }
</script>
<!--div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Search Results</h4>
      </div>
      <div class="modal-body" id="searched_body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div-->
