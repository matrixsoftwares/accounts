      <table class="table table-hover table-condensed table-bordered">
        <thead>
          <?php if($print) {?>
          <tr>
            <th colspan="8" class="text-right"><?php echo $date;?></th>
          </tr>
          <?php } ?>
          <tr>
            <th>No.</th>
            <th><?php echo THALI_ID; ?></th>
            <th>HOF Name</th>
            <th class="text-right">Takhmeen</th>
            <th class="text-right">Clear Amount</th>
            <th class="text-right">Pending Amount</th>
            <th>Count</th>
            <th <?php echo ($print) ? 'style="width:18%"' : '';?>>Month Name(s)</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($records) {
            $i = 1;
            $tot_takhmeen = $tot_clear = $tot_pending = 0;
            foreach($records as $record) {
              $tak = $cls_family->get_takhmeen_dates(FALSE, $record['FileNo']);
              $clear_amount = $cls_family->get_clear_amount($record['FileNo'], $takhmeen_year_report);
              $pending = $record['amount'] - $clear_amount;
              $tot_takhmeen += $record['amount'];
              $tot_clear += $clear_amount;
              $tot_pending += $pending;
              $ary_dates = array();
              $qty = 0;
              if($tak) {
              foreach($tak as $data) {
                $qty += $data['niyaz_qty'];
                $dt = explode('-', $data['date']);
                $time = mktime(0, 0, 0, $dt[1], $dt[2], $dt[0]);
                $h = HijriCalendar::GregorianToHijri($time);
                
                $month_name = HijriCalendar::monthName($h[0]);
                $ary_dates[] = $h[1] . ' ' . $month_name . ', ' . $h[2] . '<br>';
              }
              }
          ?>
          <tr>
            <td><?php echo $i++;?></td>
            <td><?php echo $record['FileNo'];?></td>
            <td><?php echo $cls_family->get_name($record['FileNo']);?></td>
            <td class="text-right"><?php echo number_format($record['amount'], 2);?></td>
            <td class="text-right"><?php echo number_format($clear_amount, 2);?></td>
            <td class="text-right"><?php echo number_format($pending, 2);?></td>
            <td><?php echo $qty;?></td>
            <td><?php echo implode('', $ary_dates);?></td>
          </tr>
          <?php
              }
              if($tot_clear || $tot_pending || $tot_takhmeen) {
              ?>
          <tr>
            <td colspan="3" class="text-right"><strong>Total:</strong></td>
            <td><?php echo number_format($tot_takhmeen, 2);?></td>
            <td><?php echo number_format($tot_clear, 2);?></td>
            <td><?php echo number_format($tot_pending, 2);?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
              <?php }} else { ?>
          <tr>
            <td colspan="6" class="alert-danger">Sorry! no records found.</td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
