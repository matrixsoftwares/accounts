
        <?php $i = 1; $hub = $cost = $muasaat = 0; ?>
            <table class="table table-bordered table-condensed table-hover">
                <thead>
                    <?php if($print) {?>
                    <tr>
                        <th colspan="7">Calculated Muasaat<span class="pull-right"><?php echo date('d F, Y');?></span></th>
                    </tr>
                    <?php } ?>
                    <tr>
                        <th>No.</th>
                        <th><?php echo THALI_ID; ?></th>
                        <th>HOF Name</th>
                        <th>Tiffin Size</th>
                        <th class="text-right">Tiffin Cost</th>
                        <th class="text-right">Takhmeen</th>
                        <th class="text-right">Muasaat</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($ary_File) > 0) {
                    foreach ($ary_File as $value) {
                      $tfn_cost = $value['cost'];
                      $f_id = $value['FileNo'];
                      $takh_amount = $value['takhmeen'];
                      $size = $value['size'];
                      if($tfn_cost > $takh_amount) {
                        $hub += $takh_amount;
                        $cost += $tfn_cost;
                        $saat = $tfn_cost - $takh_amount;
                        $muasaat += $saat;
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $f_id; ?></td>
                            <td><?php echo $value['HOF']; ?></td>
                            <td><?php echo $size; ?></td>
                            <td class="text-right"><?php echo number_format($tfn_cost, 2); ?></td>
                            <td class="text-right"><?php echo number_format($takh_amount, 2); ?></td>
                            <td class="text-right"><?php echo number_format($saat, 2); ?></td>
                        </tr>
                      <?php }} ?>
    <?php if (isset($hub) && isset($cost) && isset($muasaat)) { ?>
                        <tr>
                            <td colspan="4" style="text-align: right"><strong>Total</strong></td>
                            <td class="text-right"><?php echo number_format($hub, 2); ?></td>
                            <td class="text-right"><?php echo number_format($cost, 2); ?></td>
                            <td class="text-right"><?php echo number_format($muasaat, 2); ?></td>
                        </tr>
                    <?php } } else { ?>
                        <tr>
                          <td colspan="7" class="alert-danger">No results found.</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
