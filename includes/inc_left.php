<?php
  $rights = $_SESSION[USER_RIGHTS];
?>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="treeview<?php if ($active_page == 'dashboard') echo ' active'; ?>">
        <a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
      </li>
      <?php
        if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY, INVENTORY_ENTRY, ACCOUNTS_REPORTS, INVENTORY_REPORTS), $rights)) {
      ?>
      <li class="treeview<?php if ($active_page == 'menu') echo ' active'; ?>">
        <a href="#">
          <i class="fa fa-pie-chart"></i> 
          <span>Maedat</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php
            if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY, INVENTORY_ENTRY, ACCOUNTS_REPORTS), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-share"></i> Set Up <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'inv_category.php', 'Manage Inventory Category');
              ?>
            </ul>
          </li>
          <?php
            }
            if(show_menu_folder($_SESSION[USER_TYPE], array(INVENTORY_ENTRY, INVENTORY_REPORTS), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-share"></i> Inventory <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'add_inventory.php', 'Add Inventory');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'adjust_inventory.php', 'Adjust Inventory');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_REPORTS, $rights, 'inventory.php', 'List of Inventory');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'item_add.php', 'Add Item');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'return_inventory.php', 'Return Inventory');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_REPORTS, $rights, 'inventory_latest_price.php', 'Latest Price');
              ?>
            </ul>
          </li>
          <?php
            }
          ?>
        </ul>
      </li>
      <?php 
        }
        if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY, INVENTORY_ENTRY, ACCOUNTS_REPORTS), $rights)) {
      ?>
      <li class="treeview<?php if ($active_page == 'account') echo ' active'; ?>">
        <a href="#">
          <i class="fa fa-laptop"></i>
          <span>Accounts</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php
            if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-folder"></i> Credit <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'voluntary_rcpt.php', 'Receipt');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'credit_voucher.php', 'Credit Voucher');
              ?>
            </ul>
          </li>
          <?php
            }
          echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'inventory_asset.php', 'Asset Inventory');
            if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_REPORTS), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-folder"></i> Receipt Books <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'voluntary_rcpt_book.php', 'Receipt Book');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'book_credit.php', 'Credit Voucher Book');
              ?>
            </ul>
          </li>
          <?php
            }
            if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-folder"></i> Debit <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'add_bill.php', 'Bill Payment');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'direct_purchase.php', 'Direct Purchase');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'debit_voucher.php', 'Debit Voucher');
              ?>
            </ul>
          </li>
          <?php
            }
            if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY, ACCOUNTS_REPORTS), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-folder"></i> Bill Books <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'grocery_bill_book.php', 'Bill Book');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'direct_plus_payment.php', 'Direct Purchase Book');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'bill_pending.php', 'Bill Pending');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'book_debit.php', 'Debit Voucher Books');
              ?>
            </ul>
          </li>
          <?php
            }
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'daily_cash.php', 'Daily Cash');
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'daily_cash_list.php', 'List of Daily Cash');
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'list_cheque.php', 'Manage all cheques');
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'unclear_cheque_and_receipt.php', 'Manage Unclear Cheque and Receipts');
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'daily_income_expense.php', 'Daily Income and Expense');
          ?>
        </ul>
      </li>
      <?php
        }
        if ($_SESSION[USER_TYPE] == 'A') {
      ?>
      <li class="treeview<?php if ($active_page == 'settings') echo ' active'; ?>">
        <a href="#">
          <i class="fa fa-edit"></i> <span>Settings</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="general_settings.php"><span class="glyphicon glyphicon-plus-sign"></span> General Settings</a></li>
            <li><a href="shop_entry.php"><span class="glyphicon glyphicon-plus-sign"></span> Manage Company</a></li>
            <li><a href="user_add.php"><span class="glyphicon glyphicon-plus-sign"></span> Add User</a></li>
            <li><a href="user_manage.php"><span class="glyphicon glyphicon-refresh"></span> Manage Users</a></li>
        </ul>
      </li>
      <?php } ?>
      <li class="treeview<?php if ($active_page == 'search') echo ' active'; ?>">
        <a href="#">
          <i class="fa fa-circle-o text-red"></i>
          <span>Search</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li data-toggle="modal" data-target="#SEARCH_HOF_DETAILS"><a>Search</a></li>
          <?php if (SHOW_REF_FMB_HUB) { ?>
            <li><a data-toggle="modal" data-target="#SEARCH_FC_CODE_FRM">Fc Code</a></li>
          <?php } ?>
          <li><a data-toggle="modal" data-target="#SEARCH_TAKHMEEN_CALCULATION_FORM">Takhmeen Calculation</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

  <div class="modal fade" id="SEARCH_TAKHMEEN_CALCULATION_FORM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Takhmeen Calculation</h4>
        </div>
        <div class="modal-body" id="takh_body"><?php include 'inc.takhmeen_calc.php'; ?></div>
        <div class="modal-body" id="result_takh_body" style="display: none;"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- TAKHMEEN CALCULATION -->
  <!-- FC CODE -->
  <div class="modal fade" id="SEARCH_FC_CODE_FRM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Fc Code</h4>
        </div>
        <div class="modal-body" id="searched_fc_code_body"><?php include 'fc_codes.php'; ?></div>
        <div class="modal-body" id="result_searched_fc_code_body" style="display: none;"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- FC CODE -->
  <!-- SEARCH HOF DETAILS -->
  <div class="modal fade" id="SEARCH_HOF_DETAILS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Search Details</h4>
        </div>
        <div class="modal-body" id="searched_hof_detail_body"><?php include 'search_bar.php'; ?></div>
        <div class="modal-body" id="result_searched_hof_detail_body" style="display: none;"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- SEARCH HOF DETAILS -->
  <!-- TRIGGER WHEN CLOSE BUTTON IS CLICKED -->
  <script>
    $('#SEARCH_HOF_DETAILS').on('hidden.bs.modal', function(e) {
      $('#searched_hof_detail_body').show();
      $('#result_searched_hof_detail_body').empty().hide();
    });
    $('#SEARCH_FC_CODE_FRM').on('hidden.bs.modal', function(e) {
      $('#searched_fc_code_body').show();
      $('#result_searched_fc_code_body').empty().hide();
    });
    $('#SEARCH_TAKHMEEN_CALCULATION_FORM').on('hidden.bs.modal', function(e) {
      $('#takh_body').show();
      $('#result_takh_body').empty().hide();
    });
  </script>


  <!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script-->
  <!-- Slidebars -->
  <script src="asset/slidebars/slidebars.min.js"></script>
  <script src="asset/slidebars/slidebars-theme.js"></script>   
  <script src="asset/slidebars/slidebars.js"></script>
  <script>
    (function($) {
      $(document).ready(function() {
        $.slidebars();
      });
    })(jQuery);
  </script>
  
  <div class="row">
    <div class="col-md-12"><?php include 'includes/messages.php'; ?></div>
  </div>
