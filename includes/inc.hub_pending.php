          <?php if($show_tbl) {?>
<table class="table table-hover table-condensed resizable">
  <thead>
  <th>Sr No</th>
  <th><?php echo THALI_ID; ?></th>
  <th>HOF Name</th>
  <th>Mobile No.</th>
  <th>Thaali Size</th>
  <th class="text-center">Open date</th>
  <th class="text-center">Close date</th>
  <th class="text-right">Takhmeen</th>
  <th class="text-right">Paid Amount</th>
  <th class="text-right">Pending Amount</th>
</thead>
<tbody>

  <?php
  if (is_array($search) && !empty($search)) {
    $num = 1;
    $total_takhmeen = 0;
    $total_paid = 0;
    $total_pending = 0;
    foreach ($search as $family) {
      $thali_id = $family['FileNo'];
      $current_hijri = HijriCalendar::GregorianToHijri();
      $cur_year = $current_hijri[2];
      $year = $takh_year ? $takh_year : $cur_year . '-' . substr(($cur_year+1), -2);

      $takh_amount = $cls_family->get_takhmeen_record($thali_id, FALSE, "AND `year` = '$year'");
      //print_r($takh_amount);
      
      /*
       * Check for skip or limit on kisht
       */
      $limit_kisht = $takh_amount[0]['total_kisht'];
      if($limit_kisht > 0) {
        $cal_pending_kisht = $limit_kisht;
      } else {
        $cal_pending_kisht = $pending_kisht;
      }
      
      $cal_pending_kisht = $cal_pending_kisht - $takh_amount[0]['skip_kisht'];
      $cal_default_kisht = $default_kisht - $takh_amount[0]['skip_kisht'];
      
      $takhmeen_amount = ($takh_amount[0]['amount'] / $cal_default_kisht) * $cal_pending_kisht;
      //if($thali_id == 'F002') echo $thali_id, $takhmeen_amount;exit;
      
      $paid_amount = $cls_family->get_paid_amount($thali_id, $year);
      
      $pending_amount = $takhmeen_amount - $paid_amount;
      
      ($pending_amount < 0.01) ? $pending_amount = 0 : '';
      
      if(!$show_all_records){
        if(!$takhmeen_amount > 0) continue;
        if($paid_amount >= $takhmeen_amount) continue;
        if($pending_amount == 0) continue;
      }
      //if($paid_amount < $takhmeen_amount) {
      
      if($pending_amount > 0) $total_pending += $pending_amount;
      $pending_amount = ($pending_amount > 0) ? 'Rs. ' . number_format($pending_amount, 2) : 'CLEAR';
      
      //print_r($family);exit;
      if ($family['close_date'] > 0) {
        switch (USE_CALENDAR) {
          case 'Hijri':
            $hijri_close_date = HijriCalendar::GregorianToHijri($family['close_date']);
            $month_name = HijriCalendar::monthName($hijri_close_date[0]);
            $close_year = $hijri_close_date[2];
            break;
          case 'Greg':
            $month_name = date('F', $family['close_date']);
            $close_year = date('Y', $family['close_date']);
            break;
        }
      }
      $h = (USE_CALENDAR == 'Hijri') ? 'H' : '';
      
      $acct_open = $cls_family->get_family_open_date($thali_id);
      if ($acct_open) {
        switch (USE_CALENDAR) {
          case 'Hijri':
            $open = HijriCalendar::GregorianToHijri($acct_open['timestamp']);
            $open_date = $open[1] . ' ' . HijriCalendar::monthName($open[0]) . ', ' . $open[2] . ' H';
            break;
          case 'Greg':
            $open_date = date('d', $acct_open['timestamp']) . ' ' . date('F', mktime(0, 0, 0, date('m', $acct_open['timestamp']))) . ', ' . date('Y', $acct_open['timestamp']);
            break;
        }
      }
      
      //$pending_amount = 0;
      
      
        ?>
        <tr>
          <td><?php echo $num++; ?></td>
          <td><?php echo $thali_id; ?></td>
          <td><?php echo ucwords(strtolower($cls_family->get_name($thali_id))); ?></td>
          <td><?php echo $family['MPh']; ?></td>
      
          
          <td><?php echo $family['tiffin_size']; ?></td>
          <td class="text-center"><?php echo $open_date; ?></td>
          <?php if ($family['close_date'] != 0) { ?>
          <td class="text-center" style="background-color: #FF9999;"><?php echo $month_name . ', ' . $close_year . " $h"; ?></td>
          <?php } else { ?>
          <td class="text-center">--</td>
          <?php } ?>
          <td class="text-right">
            <?php
            echo number_format($takhmeen_amount, 2);
            $total_takhmeen += $takhmeen_amount;
            ?>
          </td>
          <td class="text-right"><?php
        echo number_format($paid_amount, 2);
        $total_paid += $paid_amount;
            ?></td>
          <td class="text-right"><?php echo $pending_amount; ?></td>
        </tr>
        <?php
    
    }
  } else {
    ?>
    <tr>
      <td colspan="10" class="alert-danger">Sorry! No Record Found</td>
    </tr>
    <?php
  }

  if (isset($total_takhmeen) && isset($total_paid) && isset($total_paid) && $total_takhmeen > 0 && $total_paid > 0 && $total_pending > 0) {
    ?>
    <tr>
      <td style="text-align: right" colspan="6">Total:</td>
      <td colspan="1" class="text-right"><strong><?php echo number_format($total_takhmeen, 2); ?></strong></td>
      <td colspan="1" class="text-right"><strong><?php echo number_format($total_paid, 2); ?></strong></td>
      <td class="text-right"><strong><?php echo number_format($total_pending, 2); ?></strong></td>
      <td>&nbsp;</td>
    </tr>
    <?php
  }
  ?>
</tbody>
</table>
          <?php } ?>