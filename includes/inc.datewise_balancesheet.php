<style>
  p {
    text-align: right;
  }
</style>
<br>
<div class="col-md-12">
  <div class="col-md-6 alert-success">
    <label class="form-control-static"><?php echo $from_date; ?> : </label>
    <label>Opening Cash = <?php if($opening_data[0]['opening_cash'] != '') echo $opening_data[0]['opening_cash']; else echo 0; ?> &</label>
    <label>Opening Bank = <?php if($opening_data[0]['opening_bank'] != '') echo $opening_data[0]['opening_bank']; else echo 0; ?></label>
  </div>
  <div class="col-md-6 alert-success">
    <label class="form-control-static"><?php echo $to_date; ?> :</label>
    <label>Closing Cash = <?php if($closing_data[0]['closing_cash'] != '') echo $closing_data[0]['closing_cash']; else echo 0; ?> &</label>
    <label>Closing Bank = <?php if($closing_data[0]['closing_bank'] != '') echo $closing_data[0]['closing_bank']; else echo 0; ?></label>
  </div>
</div>
<div class="col-md-6">
  <h2>Assets</h2>
  <div class="col-md-12">
    <label class="col-md-4">Heads</label>
    <label class="col-md-4 text-right">Cash</label>
    <label class="col-md-4 text-right">Cheque</label>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-12">
    <label class="col-md-4 form-control-static">Voluntary Amount</label>
    <p class="form-control-static col-md-4"><?php echo number_format($voluntary_cash_amount, 2); ?></p>
    <p class="form-control-static col-md-4"><?php echo number_format($voluntary_cheque_amount, 2); ?></p>
  </div>
  <div class="clearfix"></div>
  <?php
  if (isset($ary_cash_credit) && (!empty($ary_cash_credit))) {
    foreach ($ary_cash_credit as $key => $value) {
      if (!empty($value)) {
        //
        ?>
        <div class="col-md-12">
          <label class="col-md-4 form-control-static"><a href="bill_details.php?hd=<?php echo $key ?>&cmd=master_credit&sid=0" target="blank"><?php echo $key ?></a></label>
          <p class="form-control-static col-md-4"><?php echo number_format($ary_credit_stotal[$key], 2); ?></p>
          <p class="form-control-static col-md-4"><?php echo number_format($ary_cheque_credit[$key], 2); ?></p>
        </div>
        <div class="clearfix"></div>
        <?php
        foreach ($value as $sub_head_name => $sub_details) {
          ?>
          <div class="col-md-12">
            <label class="col-md-4 form-control-static"><a href="bill_details.php?sid=<?php echo $sub_head_name ?>&cmd=credit&hd=0" target="blank"><small><?php echo $sub_head_name; ?></small></a></label>
            <p class="form-control-static col-md-4"><?php
              $total_cash_voucher += $sub_details['SUB_HEAD_CASH_AMOUNT'];
              if ($sub_details['SUB_HEAD_CASH_AMOUNT'] != '')
                echo '<small>' . number_format($sub_details['SUB_HEAD_CASH_AMOUNT'], 2) . '</small>';
              else
                echo '<small>0</small>';
              ?></p>
            <p class="form-control-static col-md-4"><?php
              $total_cheque_voucher += $sub_details['SUB_HEAD_CHEQUE_AMOUNT'];
              if ($sub_details['SUB_HEAD_CHEQUE_AMOUNT'] != '')
                echo '<small>' . number_format($sub_details['SUB_HEAD_CHEQUE_AMOUNT'], 2) . '</small>';
              else
                echo '<small>0</small>';
              ?></p>
          </div>
          <div class="clearfix"></div>
          <?php
          if ($sub_head_name == 'Salawat Hub') {
            ?>
            <div class="col-md-12">
              <label class="col-md-4 form-control-static"><a href="bill_details.php?sid=<?php echo $sub_head_name ?>&cmd=credit"><small>Hub Amount</small></a></label>
              <p class="form-control-static col-md-4"><?php echo number_format($hub_cash_amount, 2); ?></p>
              <p class="form-control-static col-md-4"><?php echo number_format($hub_chk_amount, 2); ?></p>
            </div>
          <?php } ?>
          <?php
        }
        ?>
        <?php
      }
    }
  }
  ?>
  <div class="clearfix"></div>
  <div class="col-md-12">
    <label class="col-md-4 form-control-static">Self Bank Transaction</label>
    <p class="form-control-static col-md-4"><?php echo number_format($self_cash_withrawal, 2); ?></p>
    <p class="form-control-static col-md-4"><?php echo number_format($self_cash_deposit, 2); ?></p>
  </div>
  <div class="clearfix"></div>
  <?php
  $asset_unclear = $hub_unclear_amount + $voluntary_unclear_amount + $unclear_credit_amount;
  $lia_unclear = $unclear_debit_voucher + $total_unclear_bills;
  ?>
  <div class="col-md-12 font_20">
    <label class="col-md-4 form-control-static">Total Assets</label>
    <p class="form-control-static col-md-4"><?php
      //echo $opening_cash . '<br>' . $hub_cash_amount . '<br>' . $voluntary_cash_amount . '<br>' . $credit_cash . '<br>';
      $cash = $hub_cash_amount + $voluntary_cash_amount + $credit_cash + $self_cash_withrawal;
//        echo "Opening: $opening_cash, Hub: $hub_cash_amount, Voln.: $voluntary_cash_amount, Credit: $credit_cash";
      $cheque = $hub_chk_amount + $voluntary_cheque_amount + $total_cheque_voucher + $self_cash_deposit;
      $total_cash = $cash + $cheque;
      $income_cash = $cash;
      $income_chk = $cheque;
      echo number_format($cash, 2);
      ?></p>
    <p class="form-control-static col-md-4"><?php echo number_format($cheque, 2); ?></p>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-12">
    <label class="col-md-4 form-control-static"><a href="list_cheque.php?cmd="><a href="list_cheque.php">Unclear Amount</a></label>
    <p class="form-control-static col-md-4"><?php
      echo number_format($asset_unclear, 2);
      ?></p>
    <p class="form-control-static col-md-4">&nbsp;</p>
  </div>
</div>

<div class="col-md-6">
  <h3>Liabilities & Equity</h3>
  <div class="col-md-12">
    <label class="col-md-4">Heads</label>
    <label class="col-md-4 text-right">Cash</label>
    <label class="col-md-4 text-right">Cheque</label>
  </div>
  <div class="clearfix"></div>
  <?php
  if (isset($ary_debit) && (!empty($ary_debit))) {
    foreach ($ary_debit as $key => $value) {
      if (!empty($value)) {
        ?>
        <div class="col-md-12">
          <label class="col-md-4 form-control-static"><a href="bill_details.php?hd=<?php echo $key ?>&cmd=master_debit" target="blank"><?php echo $key; ?></a></label>
          <p class="form-control-static col-md-4"><?php echo number_format($ary_debit_stotal[$key], 2); ?></p>
          <p class="form-control-static col-md-4"><?php echo number_format($ary_debit_cheque_stotal[$key], 2); ?></p>
        </div>
        <div class="clearfix"></div>
        <?php
        foreach ($value as $sub_head => $sub_det) {
          ?>
          <div class="col-md-12">
            <label class="col-md-4 form-control-static"><a href="bill_details.php?sid=<?php echo $sub_head ?>&cmd=debit" target="blank"><small><?php echo $sub_head; ?></small></a></label>
            <p class="form-control-static col-md-4"><?php
              $total_debit_vouchers += $sub_det['SUB_HEAD_DEBIT_CASH_AMOUNT'];
              echo '<small>' . number_format($sub_det['SUB_HEAD_DEBIT_CASH_AMOUNT'], 2) . '</small>';
              ?></p>
            <p class="form-control-static col-md-4"><?php
              $total_cheque_debit_vouchers += $sub_det['SUB_HEAD_DEBIT_CHEQUE_AMOUNT'];
              echo '<small>' . number_format($sub_det['SUB_HEAD_DEBIT_CHEQUE_AMOUNT'], 2) . '</small>';
              ?></p>
          </div>
          <div class="clearfix"></div>
          <?php
        }
        ?>
        <?php
      }
    }
  }
  ?>
  <div class="clearfix"></div>
  <div class="col-md-12">
    <label class="col-md-4 form-control-static">Self Bank Transaction</label>
    <p class="form-control-static col-md-4"><?php echo number_format($self_cash_deposit, 2); ?></p>
    <p class="form-control-static col-md-4"><?php echo number_format($self_cash_withrawal, 2); ?></p>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-12 font_20">
    <label class="col-md-4 form-control-static">Total Liabilities</label>
    <p class="form-control-static col-md-4"><?php
      $cash = $total_debit_vouchers + $self_cash_deposit;
      $cheque = $total_cheque_debit_vouchers + $self_cash_withrawal;
      $tot_lia = $cash + $cheque;
      $income_lia_cash = $cash;
      $income_lia_chk = $cheque;
      echo number_format($cash, 2);
      ?></p>
    <p class="form-control-static col-md-4"><?php echo number_format($cheque, 2); ?></p>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-12">
    <label class="col-md-4 form-control-static"><a href="list_cheque.php?cmd="><a href="list_cheque.php">Unclear Amount</a></label>
    <p class="form-control-static col-md-8"><?php
      echo number_format($lia_unclear, 2);
      ?></p>
    <p class="form-control-static col-md-4">&nbsp;</p>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-12">
   <label class="col-md-4 form-control-static"><a href="list_unpaid.php">UnPaid Amount</a></label>
    <p class="form-control-static col-md-8"><?php
      echo number_format($total_unpaid, 2);
      ?></p>
    <p class="form-control-static col-md-4">&nbsp;</p>
  </div>
  <div class="clearfix"></div>
</div>
<div class="clearfix"></div>


<?php
$total_income = $income_cash - $income_lia_cash;
$total_income_chk = $income_chk - $income_lia_chk;
$balance = $total_income + $total_income_chk;
if ($total_income > 0) {
  ?>
  <div class="col-md-12">&nbsp;</div>
  <div class="col-md-12">
    <?php
    $cls = ($total_income > 0) ? 'alert-success' : 'alert-danger';
    ?>
    <div class="form-group <?php echo $cls; ?>">
      <label class="col-md-7 form-control-static">Total Income Cash</label>
      <p class="form-control-static"><?php echo number_format($total_income, 2); ?></p>
    </div>
    <?php
    $cls = ($total_income_chk > 0) ? 'alert-success' : 'alert-danger';
    ?>
    <div class="form-group <?php echo $cls; ?>">
      <label class="col-md-7 form-control-static">Total Income Cheque</label>
      <p class="form-control-static"><?php echo number_format($total_income_chk, 2); ?></p>
    </div>
  </div>
  <?php
  $cls = ($balance > 0) ? 'alert-success' : 'alert-danger';
  ?>
  <div class="col-md-6 <?php echo $cls; ?> pull-right">
    <label class="col-md-7 form-control-static">Total Income</label>
    <p class="form-control-static"><?php echo number_format($balance, 2); ?></p>
  </div>

  <?php
} else {
  ?>
  <?php
  $cls = ($total_income > 0) ? 'alert-success' : 'alert-danger';
  ?>
  <div class="col-md-6">
    <div class="form-group <?php echo $cls; ?>">
      <label class="col-md-7 form-control-static">Total Income Cash</label>
      <p class="form-control-static"><?php echo number_format($total_income, 2); ?></p>
    </div>
    <?php
    $cls = ($total_income_chk > 0) ? 'alert-success' : 'alert-danger';
    ?>
    <div class="form-group <?php echo $cls; ?>">
      <label class="col-md-7 form-control-static">Total Income Cheque</label>
      <p class="form-control-static"><?php echo number_format($total_income_chk, 2); ?></p>
    </div>
  </div>
  <?php
  $cls = ($balance > 0) ? 'alert-success' : 'alert-danger';
  ?>
  <div class="col-md-6 <?php echo $cls; ?> pull-right">
    <label class="col-md-7 form-control-static">Total Income</label>
    <p class="form-control-static"><?php echo number_format($balance, 2); ?></p>
  </div>
  <div class="col-md-6">&nbsp;</div>
  <?php
}
?>