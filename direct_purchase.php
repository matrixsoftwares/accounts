<?php
include 'session.php';
$pg_link = 'add_bill';
require_once('classes/class.database.php');
require_once('classes/class.billbook.php');
require_once('classes/class.product.php');
require_once('classes/class.receipt.php');
$user_id = $_SESSION[USER_ID];
$cls_billbook = new Mtx_BillBook();
$cls_receipt = new Mtx_Receipt();
$cls_product = new Mtx_Product();

if (isset($_POST['add_bill'])) {
  
  if(!isset($_POST['type'])) $_POST['type'] = '';
  if(!isset($_POST['make_payment'])) {
    $_POST['make_payment'] = 0;
    $_POST['type'] = '';
    $_POST['bankname'] = '';
    $_POST['cheque'] = '';
    $_POST['dob'] = '';
  }
  $result = $cls_billbook->add_directPurchase($_POST['billID'], $_POST['heads'], ucfirst($_POST['shop']), $_POST['description'], $_POST['product_name'], $_POST['quantity'], $_POST['unit'], $_POST['price'], floatval($_POST['vat']), floatval($_POST['discount']), floatval($_POST['charge']), floatval($_POST['amount']), $_POST['type'], ucfirst($_POST['bankname']), $_POST['cheque'], $_POST['dob'], $_POST['bill_date'], $user_id, (int) $_POST['make_payment'], $_POST['menu_date'], $_POST['contact']);
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'Bill added successfully';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while processing Direct Purchase Bill';
  }
}
$shops = $cls_billbook->get_shops();
$direct_items = $cls_product->get_all_direct_items();
$heads = $cls_receipt->get_account_heads();
$title = 'Add Direct Purchase Bill';
$active_page = 'account';

require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Debit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php'); ?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8 ">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="form-group">
              <label class="control-label col-md-2">Bill No.</label>
              <div class="col-md-4">
                <input type="text" name="billID" value="" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Account Heads</label>
              <div class="col-md-4">
                <select class="form-control" name="heads" id="heads">
                  <option value="0">--Select One--</option>
                  <?php if ($heads) {
                    foreach ($heads as $head) { ?>
                      <option value="<?php echo $head['head']; ?>" <?php echo ($head['head'] == 'Raw Material Purchase') ? 'selected' : ''; ?>><?php echo $head['head']; ?></option>
        <?php }
      } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-2">Bill Date</label>
              <div class="col-md-4">
                <input type="date" name="bill_date" class="form-control" id="bill_date" placeholder="" value="<?php echo date('Y-m-d'); ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-2">Menu Date</label>
              <div class="col-md-4">
                <input type="date" name="menu_date" class="form-control" id="menu_date" placeholder="" value="<?php echo date('Y-m-d'); ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-2">Shop Name</label>
              <div class="col-md-4">
                  <select name="shop" id="shop" class="form-control">
                      <option value="">--Select Shop--</option>
                      <?php foreach($shops as $shop) {?>
                      <option value="<?php echo $shop['id'];?>"><?php echo $shop['ShopName'];?></option>
                      <?php } ?>
                  </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Items</label>
              <div class="col-md-4">
                <select class="form-control" name="product_name[]" id="product_name0">
                  <option value="0">--Select One--</option>
                  <?php foreach ($direct_items as $name) { ?>
                    <option value="<?php echo $name['name']; ?>"><?php echo $name['name']; ?></option>
      <?php } ?>
                </select>
              </div>
              <div class="col-md-2">
                <input type="text" class="form-control" name="quantity[]" id="quantity0" placeholder="Quantity">
              </div>
              <div class="col-md-2">
                <input type="text" class="form-control rows" name="unit[]" id="unit0" placeholder="Unit Price" onblur="checkInput(0);">
              </div>
              <div class="col-md-2">
                <p class="form-control-static" id="price10"></p>
                <input type="hidden" class="form-control" name="price[]" id="price0" placeholder="Price">
              </div>
            </div>

      <?php for ($i = 1; $i <= 15; $i++) { ?>
              <div class="form-group" id="add_rows<?php echo $i; ?>" style="display: none;">
                <label class="control-label col-md-2">&nbsp;</label>
                <div class="col-md-4">
                  <select class="form-control" name="product_name[]" id="product_name<?php echo $i; ?>">
                    <option value="0">--Select One--</option>
                    <?php foreach ($direct_items as $name) { ?>
                      <option value="<?php echo $name['name']; ?>"><?php echo $name['name']; ?></option>
        <?php } ?>
                  </select>
                </div>
                <div class="col-md-2">
                  <input type="text" class="form-control" name="quantity[]" id="quantity<?php echo $i; ?>" placeholder="Quantity">
                </div>
                <div class="col-md-2">
                  <input type="text" class="form-control rows" name="unit[]" id="unit<?php echo $i; ?>" placeholder="Unit Price" onblur="checkInput(<?php echo $i; ?>);">
                </div>
                <div class="col-md-2">
                  <p class="form-control-static" id="price1<?php echo $i; ?>"></p>
                  <input type="hidden" class="form-control" name="price[]" id="price<?php echo $i; ?>" placeholder="Price">
                </div>
              </div>
      <?php } ?>

            <div class="form-group">
              <label class="control-label col-md-2">VAT Charges</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="vat" id="vat" placeholder="%" onblur="AddVAT(this.value);">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Discount</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="discount" id="discount" placeholder="Rupees" onblur="Discount(this.value);">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Other Charges</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="charge" id="charge" placeholder="Rupees" onblur="OtherCharges(this.value);">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Net Amount</label>
              <div class="col-md-4">
                <p class="form-control-static" id="grand_amount"></p>
                <input type="hidden" class="form-control" name="amount" id="amount">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Description</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="description" id="description">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Contact Person</label>
              <div class="col-md-4">
                <input type="text" name="contact" id="contact" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Make Payment</label>
              <div class="col-md-4">
                <input type="checkbox" id="make_payment" name="make_payment" onclick="payment_options();" value="0">
              </div>
            </div>

            <div class="form-group" id="payment_options" style="display: none;">
              <label class="control-label col-md-2">Type</label>
              <div class="col-md-4">
                <input type="radio" id="radio" name="type" value="cash">&nbsp;Cash&emsp;
                <input type="radio" id="radio" name="type" value="cheque">&nbsp;Cheque&emsp;
                <input type="radio" id="radio" name="type" value="cheque-neft">&nbsp;NEFT
              </div>
            </div>

            <div id="cheque_detail" style="display: none">
              <div></div>

              <div class="form-group" >
                <label class="control-label col-md-2">Bank Name</label>
                <div class="col-md-4">
                  <input type="text" name="bankname" id="bankname" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-2" id="num">Cheque No.</label>
                <div class="col-md-4">
                  <input type="text" name="cheque" id="cheque" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-2">Date</label>
                <div class="col-md-4">
                  <input type="date" name="dob" class="form-control" id="dob" placeholder="">
                </div>
              </div>
            </div>

            <div class="form-group">&nbsp;</div>
            <button type="submit" name="add_bill" id="add_bill" class="btn btn-success col-md-offset-2">Add Bill</button>
            <input type="reset" value="Reset" class="btn btn-default">


          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          var count = 0;
          var net_amt = 0;
          var gblmul = 0;
          var gblVat = 0;
          var gblCharges = 0;
          var gblDiscountRates = 0;
          $('#add_bill').click(function() {
            var bill = $('#BillNo').val();
            var shop = $('#shop').val();
            var amount = $('#amount').val();
            var error = 'Whoops! following error(s) are occured\n';
            var validate = true;
            if (bill == '')
            {
              error += "Please enter bill no\n";
              validate = false;
            }
            if (shop == '')
            {
              error += 'Please select shop name\n';
              validate = false;
            }
            if (amount == '')
            {
              error += 'Please enter net amount\n';
              validate = false;
            }
            if (validate == false)
            {
              alert(error);
              return validate;
            }
          });
          $("input:radio[name=type]").click(function() {
            var radio = $(this).val();
            if (radio === 'cheque')  {
              $('#cheque_detail').show();
              $('#num').text('Cheque No.');
            } else if(radio === 'cheque-neft') {
              $('#cheque_detail').show();
              $('#num').text('NEFT No.');
            } else {
              $('#cheque_detail').hide();
            }
          });
          function payment_options() {
            var chkbox = document.getElementById('make_payment').checked;
            if (chkbox === true) {
              $('#payment_options').show();
              $('#make_payment').val('1');
            } else {
              $('#payment_options').hide();
              $('#make_payment').val('0');
              $('#cheque_detail').hide();
            }
          }
      //to add rows
          function checkInput(row) {
            var product = $('#product_name' + row).val();
            var quantity = $('#quantity' + row).val();
            var unit = $('#unit' + row).val();
            if (product === '' || quantity === '' || unit === '') {
              alert('Please enter product name, quantity and unit to proceed');
              return false;
            } else {
              if (row !== count) {
                getMul(count);
                getSum();
              } else {
                add_rows();
                getMul(count);
                getSum();
                $('#product_name' + count).focus();
              }
              return true;
            }
          }
          function getSum() {
            net_amt = (Number(gblmul) + Number(gblCharges)) - Number(gblDiscountRates);
            if (gblVat > 0) {
              net_amt += (Number(gblVat) * gblmul) / 100;
            }
            $('#grand_amount').text(net_amt);
            $('#amount').val(net_amt);
          }
          function getMul(id)
          {
            gblmul = 0;
            for (var i = 0; i < id; i++) {
              var quantity = document.getElementById("quantity" + i).value;
              var unit = document.getElementById("unit" + i).value;
              gblmul += (quantity * unit);
              $('#price1' + i).text(quantity * unit);
              $('#price' + i).val(quantity * unit);
            }
          }

          function add_rows()
          {
            count = count + 1;
            var ldiv = document.getElementById("add_rows" + count);
            ldiv.style.display = "block";
          }
          function AddVAT(vat) {
            gblVat = vat;
            getSum();
          }

          function OtherCharges(charge) {
            gblCharges = charge;
            getSum();
          }

          function Discount(discount) {
            gblDiscountRates = discount;
            getSum();
          }
        </script>
      </div>
      <!-- /Content -->
    </section>
  </div>

<?php
include('includes/footer.php');
?>