<?php
date_default_timezone_set('Asia/Kolkata');

require_once('classes/class.database.php');
require_once('classes/class.user.php');
require_once('classes/class.family.php');
require_once('classes/class.receipt.php');
require_once('classes/class.barcode.php');
require_once('classes/class.product.php');
require_once('classes/class.menu.php');
require_once('classes/hijri_cal.php');
$cls_user = new Mtx_User();
$cls_family = new Mtx_family();
$cls_barcode = new Mtx_Barcode();
$cls_receipt = new Mtx_Receipt();
$cls_menu = new Mtx_Menu();
$hijari = new HijriCalendar();
$cls_product = new Mtx_Product();
$cmd = $_REQUEST['cmd'];

switch ($cmd) {
  case 'get_name':
    $check = $cls_family->check_thali_ID_exist($_REQUEST['fId']);
    if ($check) {
      $result = $cls_family->get_name($_REQUEST['fId']);
      if ($result) {
        echo ucwords($result);
      }
    } else {
      echo 'invalid'; //Invalid thali
    }
    break;
    
  case 'get_name_and_mobile':
    $check = $cls_family->check_thali_ID_exist($_REQUEST['fId']);
    if ($check) {
      $result = $cls_family->get_name_and_mobile($_REQUEST['fId']);
      if ($result) {
        echo json_encode($result);
      }
    } else {
      echo 'invalid'; //Invalid thali
    }
    break;

  case 'get_year_month':
    $check_close = $cls_family->check_close_family($_REQUEST['fId']);
    $partial = $cls_family->get_partial_amount($_REQUEST['fId']);
    if ($partial)
      $on_hand_amt = number_format($partial['PartialAmount']);
    else
      $on_hand_amt = NULL;

    $result = $cls_family->get_last_year_month($_REQUEST['fId']);
    if ($result) {
      if ($result['paid_till'] != 0) {
        $latest = $cls_family->latest_tiffin_size_year($_REQUEST['fId']);
        $monthly_hub = $latest['new_fmb_hub'];
        if ($monthly_hub != NULL)
          $monthly_hub = number_format($monthly_hub) . '/-';
        else
          $monthly_hub = 'Default';
        switch (USE_CALENDAR) {
          case 'Hijri':
            $hijri_date = $hijari->GregorianToHijri($result['paid_till']); //in hijri date
            $last_month = $hijri_date[0];
            $month_name = $hijari->monthName($hijri_date[0]);
            $last_year = $hijri_date[2];
            break;
          case 'Greg':
            $last_month = date('m', $result['paid_till']);
            $month_name = date('F', $result['paid_till']);
            $last_year = date('Y', $result['paid_till']);
            break;
        }
        //check if user is paying monthly or yearly

        $chk_pay_mode = $cls_family->check_payment_mode($_REQUEST['fId']);

        // if check close is not4  1`456 0 than we will take month = check_close[month] else current month
        $ary_collection = $cls_family->get_array_of_months($_REQUEST['fId'], $last_month, $last_year);

        //print_r($ary_collection);exit;
        $pending_amount = 0;
        foreach ($ary_collection as $key => $value) {
          $sep_key = explode('-', $key);
          switch (USE_CALENDAR) {
            case 'Hijri':
              $user_ts = HijriCalendar::HijriToUnix($sep_key[0], '01', $sep_key[1]);
              break;
            case 'Greg':
              $user_ts = mktime(0, 0, 0, $sep_key[0], '01', $sep_key[1]);
              break;
          }
          $data = $cls_family->get_hub_by_month($_REQUEST['fId'], $user_ts, $chk_pay_mode);
          if ($data) {
            $ary_collection[$key]['hub'] = $value['MULTI'] * $data['Hub_raqam'];
            $ary_collection[$key]['tiffin_size'] = $data['Tiffin_size'];
            $pending_amount += ($value['MULTI'] * $data['Hub_raqam']);
          }
        }
        //print_r($ary_collection);exit;
        $five_rcpt = $cls_receipt->get_last_five_hub_rcpt($_REQUEST['fId']);
        foreach ($five_rcpt as $key => $rcpt) {
          $rcpt_from = HijriCalendar::GregorianToHijri($rcpt['from_date']);
          $five_rcpt[$key]['from_date'] = $hijari->monthName($rcpt_from[0]) . ' - ' . $rcpt_from[2] . ' H';
          $rcpt_to = HijriCalendar::GregorianToHijri($rcpt['to_date']);
          $five_rcpt[$key]['to_date'] = $hijari->monthName($rcpt_to[0]) . ' - ' . $rcpt_to[2] . ' H';
        }

        ($pending_amount > 0) ? $pending = number_format($pending_amount) . '/-' : $pending = 'CLEAR';
        $h = (USE_CALENDAR == 'Hijri') ? 'H' : '';
        $ary_data = array("MONTHLY_HUB" => $monthly_hub, "PENDING_AMOUNT" => $pending, "YEAR_MONTH" => $month_name . ' ' . $last_year . $h, "EN_M_Y" => $last_month . '-' . $last_year, "ON_HAND_AMOUNT" => $on_hand_amt, 'RECEIPTS' => $five_rcpt);
        $rtn_ary = json_encode($ary_data);
        echo $rtn_ary;
      } else {
        echo '0';
      }
    } else
      echo '0';
    break;

  case 'get_hub_amount':
    $for_months = $_REQUEST['months'];
    if ($for_months > 0) {
      $result = $cls_family->get_last_year_month($_REQUEST['fId']);
      if ($result) {
        // to get last date
        // to get last date
        switch (USE_CALENDAR) {
          case 'Hijri';
            $hijri_date = $hijari->GregorianToHijri($result['paid_till']); //in hijri date
            $last_month = $hijri_date[0];
            $last_year = $hijri_date[2];
            break;
          case 'Greg':
            $last_month = date('m', $result['paid_till']);
            $month_name = date('F', $result['paid_till']);
            $last_year = date('Y', $result['paid_till']);
            break;
        }
        // FIX: to solve round months error in from date
        if ($last_month == 12) {
          $from_month = 1;
          $from_year = $last_year + 1;
        } else {
          $from_month = $last_month + 1;
          $from_year = $last_year;
        }

        $to_month = ($last_month + $for_months) % 12;
        $to_year = floor(($last_month + $for_months) / 12) + $last_year;

        // FIX: to solve round months error in upto date
        If ($to_month == 0) {
          $to_month = 12;
          $to_year = $to_year - 1;
        }
        $month_name = (USE_CALENDAR == 'Hijri') ? HijriCalendar::monthName($to_month) : date('F', mktime(0, 0, 0, $to_month));

        //3 steps create hijri months array according to user input
        $command = '';
        $chk_pay_mode = $cls_family->check_payment_mode($_REQUEST['fId']);
        if ($chk_pay_mode > 0) {
          switch ($chk_pay_mode) {
            case 12:
              $command = 'Monthly';
              break;
            case 3:
              $command = 'Quarterly';
              break;
            case 2: //Semi Annual
              $command = 'Semi';
              break;
            case 1: //Annual
              $command = 'Annual';
              break;
          }
        }
        $ary_collection = $cls_family->get_array_of_months($_REQUEST['fId'], $last_month, $last_year, $for_months, FALSE, $command);
        $expected_amount = 0;
        foreach ($ary_collection as $key => $value) {
          $sep_key = explode('-', $key);
          switch (USE_CALENDAR) {
            case 'Hijri':
              $user_ts = HijriCalendar::HijriToUnix($sep_key[0], '01', $sep_key[1]);
              break;
            case 'Greg':
              $user_ts = mktime(0, 0, 0, $sep_key[0], '01', $sep_key[1]);
              break;
          }
          $data = $cls_family->get_hub_by_month($_REQUEST['fId'], $user_ts);
          if ($data) {
            $ary_collection[$key]['hub'] = $value['MULTI'] * $data['Hub_raqam'];
            $ary_collection[$key]['tiffin_size'] = $data['Tiffin_size'];
            $expected_amount += ($value['MULTI'] * $data['Hub_raqam']);
          }
        }
//        $pay_term = $cls_family->get_payment_method_value($_REQUEST['fId']);
//        $expected_amount /= $pay_term;
        $h = (USE_CALENDAR == 'Hijri') ? 'H' : '';
        $arr_data = array("NEW_MONTH" => $month_name, "NEW_YEAR" => $to_year . " $h", "TOTAL" => number_format($expected_amount) . '/-', "EN_M_Y" => $to_month . '-' . $to_year);

        echo json_encode($arr_data);
      }
    }
    break;

  case 'get_not_issued_tiffin':
    $result = $cls_barcode->get_all_barcode();
    if ($result) {
      $exceptions = $cls_barcode->get_all_hub_exceptions();
      foreach ($result as $data) {
        if (!in_array($data['FileNo'], $exceptions)) {
          $date = date('d-m-Y', $data['date']);
          // if $date == current date or null than it will be array of today's not issued tiffin
          if ($date == date('d-m-Y') || $data['date'] == NULL) {
            if (!$data['family_id'])
              $data['bg'] = '#fff';
            else
              $data['bg'] = '#CDFF9B';
            $ary_not_issued_tiffin[$data['FileNo']] = $data;
          }
        }
      }
    }
    echo json_encode($ary_not_issued_tiffin);
    break;

  case 'get_not_issued_tiffin_by_Mohallah':
    $result = $cls_barcode->get_all_barcode_by_mohallah($_REQUEST['tanzeem'], $_REQUEST['thaaliDate']);
    $ary_tfn = array();
    if ($result) {
      $exceptions = $cls_barcode->get_all_hub_exceptions();
      foreach ($result as $data) {
        if (!in_array($data['FileNo'], $exceptions)) {
          $date = date('d-m-Y', $data['date']);
          // if $date == current date or null than it will be array of today's not issued tiffin
          if ($date == date('d-m-Y') || $data['date'] == NULL) {
            if (!$data['family_id'])
              $data['bg'] = '#fff';
            else
              $data['bg'] = '#CDFF9B';
            $ary_not_issued_tiffin[$data['FileNo']] = $data;
            $ary_size_tiffin[$data['FileNo']] = $data;
          }
        }
      }
      $issue = array();
      $tiffins = $cls_barcode->get_today_tiffin($_REQUEST['tanzeem'], $_REQUEST['thaaliDate']);
      foreach ($tiffins as $data) {
        if (array_key_exists($data['tiffin_size'], $issue))
          $issue[$data['tiffin_size']] += 1;
        else
          $issue[$data['tiffin_size']] = 1;

        if (array_key_exists($data['family_id'], $ary_size_tiffin))
          unset($ary_size_tiffin[$data['family_id']]);
      }
      foreach ($ary_size_tiffin as $key => $val) {
        //tiffin not issue count
        if (array_key_exists($val['tiffin_size'], $ary_tfn))
          $ary_tfn[$val['tiffin_size']] += 1;
        else
          $ary_tfn[$val['tiffin_size']] = 1;
      }
      //unset($ary_size_tiffin);
    }
    $rtn_array = array('TIFFIN' => json_encode($ary_tfn), 'DATA' => $ary_not_issued_tiffin, 'ISSUE_TIFFIN' => json_encode($issue));
    //echo $ary_not_issued_tiffin;exit;
    echo json_encode($rtn_array);
    break;

  case 'insert_issued_tiffin':
    $family_id = $_REQUEST['barcode'];
      $tdate = $_REQUEST['thalidate'];
    $result = $cls_barcode->get_all_barcode_by_mohallah($_REQUEST['tanzeem'], $date);
    if ($result) {
      $exceptions = $cls_barcode->get_all_hub_exceptions();
      foreach ($result as $data) {
        if (!in_array($data['FileNo'], $exceptions)) {
          $date = date('d-m-Y', $data['date']);
          // if $date == current date or null than it will be array of today's not issued tiffin
          if ($date == date('d-m-Y') || $data['date'] == NULL) {
            if (!$data['family_id'])
              $data['bg'] = '#fff';
            else
              $data['bg'] = '#CDFF9B';
            $ary_size_tiffin[$data['FileNo']] = $data;
          }
        }
      }
    }
    $ary_tfn = array();
    $insert = $cls_barcode->insert_issued_tiffin($family_id,$tdate);
    $tiffins = $cls_barcode->get_today_tiffin('',$tdate);
    foreach ($tiffins as $data) {
      if (array_key_exists($data['tiffin_size'], $ary_tfn))
        $ary_tfn[$data['tiffin_size']] += 1;
      else
        $ary_tfn[$data['tiffin_size']] = 1;
      if (array_key_exists($data['family_id'], $ary_size_tiffin))
        unset($ary_size_tiffin[$data['family_id']]);
    }
    $not_issue = array();
    foreach ($ary_size_tiffin as $key => $val) {
      //tiffin not issue count
      if (array_key_exists($val['tiffin_size'], $not_issue))
        $not_issue[$val['tiffin_size']] += 1;
      else
        $not_issue[$val['tiffin_size']] = 1;
    }

    if ($insert == 1) {
      $return['FileNo'] = $_REQUEST['barcode'];
      $return['result'] = 1;
    } else {
      $return['FileNo'] = $_REQUEST['barcode'];
      $return['result'] = 0;
    }
    $rtn_array = array('TIFFIN' => json_encode($ary_tfn), 'DATA' => $return, 'NOT_ISSUE' => json_encode($not_issue));
    echo json_encode($rtn_array);
    break;

  case 'get_partial_amount':
    $file = $_REQUEST['fId'];
    $result = $cls_family->get_last_year_month($_REQUEST['fId']);
    $latest = $cls_family->latest_tiffin_size_year($_REQUEST['fId']);
    if ($latest) {
      if ($latest['new_fmb_hub'] != NULL)
        $monthly_hub = number_format($latest['new_fmb_hub']) . '/-';
      else
        $monthly_hub = 'Default';
    }
    $partial = $cls_family->get_partial_amount($file);
    if ($partial && $partial['PartialAmount'] != NULL)
      $PartialAmount = number_format($partial['PartialAmount'], 2);
    else
      $PartialAmount = NULL;
    echo json_encode(array("MONTHLY_HUB" => $monthly_hub, "PARTIAL_AMOUNT" => $PartialAmount, "PAID_TILL" => $result['paid_till']));
    break;

  case 'get_unit':
    $item = $_REQUEST['item'];
    $result = $cls_receipt->get_unit($item);
    echo $result['unit'];
    break;

  case 'tiffin_size_data':
    $check = $cls_family->check_thali_ID_exist($_REQUEST['fId']);
    if ($check == 1) {
      $past = $cls_family->check_past_record_hub_exception($_REQUEST['fId']);
      $table = '';
      if ($past) {
        $table = '<table class="table table-hover table-condensed table-bordered"><thead><tr><th>FileNo</th><th>From</th><th>To</th><th>User ID</th></tr></thead><tbody>';
        foreach ($past as $record) {

          $frm_date = explode('-', $record['from_date']);
          $frm_mon_name = HijriCalendar::monthName($frm_date[0]);
          $to_date = explode('-', $record['to_date']);
          $to_mon_name = HijriCalendar::monthName($to_date[0]);
          $user_name = $cls_user->get_all_user($record['user_id']);
          $table .= '<tr><td>' . $record['FileNo'] . '</td><td>' . $frm_mon_name . '-' . $frm_date[1] . '</td><td>' . $to_mon_name . '-' . $to_date[1] . '</td><td>' . $user_name['full_name'] . '</td></tr>';
        }
        $table .= '</tbody></table>';
      }
      $result = $cls_family->get_single_family($_REQUEST['fId']);
      $hub = $cls_family->latest_tiffin_size_year($_REQUEST['fId']);
      if ($hub['new_fmb_hub'] != NULL)
        $monthly = number_format($hub['new_fmb_hub'], 2);
      else
        $monthly = '';
      $tiffin = $cls_menu->get_all_tiffin_size();
      $option = '<option value="">--Select one--</option>';
      foreach ($tiffin as $tfn) {
        ($result['tiffin_size'] == $tfn['size']) ? $selected = 'selected' : $selected = '';
        $option .= '<option value="' . $tfn['size'] . '"' . $selected . '>' . $tfn['size'] . '</option>';
      }
      $name = ($result['HOF_NAME']) ? $result['HOF_NAME'] : $result['HOF'];
      $rtn_data = array("NAME" => ucwords(strtolower(trim($name))), "TIFFIN" => $result['tiffin_size'], "HUB" => $monthly, "OPTION" => $option, "PAST_RECORD" => $table);
      echo json_encode($rtn_data);
    } else {
      echo 0;
    }
    break;

  case 'muasaat':
    $result = $cls_family->get_data_for_muasaat($_REQUEST['fId']);
    echo json_encode($result);
    break;

  case 'check_user_exist':
    $result = $cls_user->check_user_exist($_GET['userid']);
    if ($result) {
      echo $result;
    }
    break;

  case 'check_thali_ID_exist':
    $result = $cls_family->check_thali_ID_exist($_GET['thali']);
    if ($result) {
      echo $result;
    }
    break;

  case 'check_old_password':
    $result = $cls_user->check_old_password($_GET['old_pwd'], $_GET['id']);
    echo $result;
    break;

  case 'get_count_tiffin_issued':
    $check = $cls_family->check_thali_ID_exist($_GET['thali']);
    if ($check == 1) {
      $FromDate = HijriCalendar::HijriToUnix($_GET['month'], '01', $_GET['year']);
      $to_date = HijriCalendar::HijriToUnix($_GET['month'] + 1, '01', $_GET['year']);
      $ToDate = strtotime('-1 day', $to_date);
      $result = $cls_family->get_count_of_selected_month($FromDate, $ToDate, $_GET['thali']);
      if ($result) {
        echo '<div class="well well-sm">Total issued thali: ' . $result . ' times</div>';
      } else {
        echo '<div class="well well-sm">Total issued thali: ' . $result . ' times</div>';
      }
    } else {
      echo '0'; //invalid thali id
    }
    break;

  case 'get_count_all_tiffin_issued':
    $FromDate = HijriCalendar::HijriToUnix($_GET['month'], '01', $_GET['year']);
    $ToDate = HijriCalendar::HijriToUnix($_GET['month'] + 1, '01', $_GET['year']);
    $result = $cls_family->get_count_of_selected_month($FromDate, $ToDate);
    if ($result) {
      echo json_encode($result);
    }
    break;

  case 'get_year_month_for_sabil_home':
    $check_close = $cls_family->check_close_family($_REQUEST['fId']);
    $partial = $cls_family->get_partial_amount($_REQUEST['fId']);
    if ($partial)
      $on_hand_amt = number_format($partial['PartialAmount']);
    else
      $on_hand_amt = NULL;
    $result = $cls_family->get_last_year_month_home_sabil($_REQUEST['fId']);
    if ($result) {
      if ($result['paid_till'] != 0) {
        $monthly_hub = $result['amount'];
        $gre_month = date('m', $result['paid_till']);
        $last_month = date("F", mktime(0, 0, 0, date('m', $result['paid_till']), 10));
        $last_year = date('Y', $result['paid_till']);

        // if check close is not 0 than we will take month = check_close[month] else current month
        $cur_ts = date('Y');
        $cur_month = date('m');
        if ($check_close) {
          if ($check_close['close_date'] != 0) {
            $cur_ts = date('Y', $check_close['close_date']);
            $cur_month = date('m', $check_close['close_date']);
          }
        }

        if ($last_year == $cur_ts) {
          $diff_months = abs($gre_month - $cur_month);
        } else if ($last_year < $cur_ts) {
          $diff_year = $cur_ts - $last_year;
          if ($diff_year > 1) {
            $remaining_months_from_last = 12 - $gre_month;
            $diff_months = abs((12 * $diff_year) + $remaining_months_from_last);
          } else {
            $remaining_months_from_last = 12 - $gre_month;
            $diff_months = abs($remaining_months_from_last + $cur_month);
          }
        } else if ($last_year > $cur_ts) {
          $diff_months = 0;
        }
        $pending_amount = $monthly_hub * $diff_months;
        ($pending_amount > 0) ? $pending = number_format($pending_amount) . '/-' : $pending = 'CLEAR';

        $ary_data = array("MONTHLY_HUB" => number_format($result['amount']) . '/-', "PENDING_AMOUNT" => $pending, "YEAR_MONTH" => $last_month . ' ' . $last_year, "EN_M_Y" => $gre_month . '-' . $last_year, "ON_HAND_AMOUNT" => $on_hand_amt);
        $rtn_ary = json_encode($ary_data);
        echo $rtn_ary;
      } else {
        echo '0';
      }
    } else
      echo '0';
    break;

  case 'get_hub_amount_sabil_home':
    $result = $cls_family->get_last_year_month_home_sabil($_REQUEST['fId']);
    if ($result) {
      $last_date = explode('-', $_REQUEST['last_months']);
      $last_month = $last_date[0];
      $last_year = $last_date[1];
      $for_months = $_REQUEST['months'];

      // to get last date
      $hijari_month = date("F", mktime(0, 0, 0, date('m', $result['paid_till']), 10));
      ;
      $hijari_year = date('Y', $result['paid_till']);

      // FIX: to solve round months error in from date
      If ($last_month == 12) {
        $from_month = 1;
        $from_year = $last_year + 1;
      } else {
        $from_month = $last_month + 1;
        $from_year = $last_year;
      }

      $to_month = ($last_month + $for_months) % 12;
      $to_year = floor(($last_month + $for_months) / 12) + $last_year;

      // FIX: to solve round months error in upto date
      If ($to_month == 0) {
        $to_month = 12;
        $to_year = $to_year - 1;
      }
      $month_name = date("F", mktime(0, 0, 0, $to_month, 0));
      $expected_amount = $result['amount'] * $for_months;

      $arr_data = array("NEW_MONTH" => $month_name, "NEW_YEAR" => $to_year . ' H', "TOTAL" => number_format($expected_amount) . '/-', "EN_M_Y" => $to_month . '-' . $to_year);

      echo json_encode($arr_data);
    }
    break;
  //sabil home end
  //sabil vepaar start
  case 'get_year_month_for_sabil_vepaar':
    $check_close = $cls_family->check_close_family($_REQUEST['fId']);
    $partial = $cls_family->get_partial_amount($_REQUEST['fId']);
    if ($partial)
      $on_hand_amt = number_format($partial['PartialAmount']);
    else
      $on_hand_amt = NULL;
    $result = $cls_family->get_last_year_month_vepaar_sabil($_REQUEST['fId']);
    if ($result) {
      if ($result['paid_till'] != 0) {
        $monthly_hub = $result['amount'];
        $gre_month = date('m', $result['paid_till']);
        $last_month = date('F', $result['paid_till']);
        $last_year = date('Y', $result['paid_till']);

        // if check close is not 0 than we will take month = check_close[month] else current month
        $cur_ts = date('Y');
        $cur_month = date('m');
        if ($check_close) {
          if ($check_close['close_date'] != 0) {
            $cur_month = date('m', $check_close['close_date']);
          }
        }

        if ($last_year == $cur_ts) {
          $diff_months = abs($gre_month - $cur_month);
        } else if ($last_year < $cur_ts) {
          $diff_year = $cur_ts - $last_year;
          if ($diff_year > 1) {
            $remaining_months_from_last = 12 - $gre_month;
            $diff_months = abs((12 * $diff_year) + $remaining_months_from_last);
          } else {
            $remaining_months_from_last = 12 - $gre_month;
            $diff_months = abs($remaining_months_from_last + $cur_month);
          }
        } else if ($last_year > $cur_ts) {
          $diff_months = 0;
        }
        $pending_amount = $monthly_hub * $diff_months;
        ($pending_amount > 0) ? $pending = number_format($pending_amount) . '/-' : $pending = 'CLEAR';

        $ary_data = array("MONTHLY_HUB" => number_format($result['amount']) . '/-', "PENDING_AMOUNT" => $pending, "YEAR_MONTH" => $last_month . ' ' . $last_year, "EN_M_Y" => $gre_month . '-' . $last_year, "ON_HAND_AMOUNT" => $on_hand_amt);
        $rtn_ary = json_encode($ary_data);
        echo $rtn_ary;
      } else {
        echo '0';
      }
    } else
      echo '0';
    break;

  case 'get_hub_amount_sabil_vepaar':
    $result = $cls_family->get_last_year_month_vepaar_sabil($_REQUEST['fId']);
    if ($result) {
      $last_date = explode('-', $_REQUEST['last_months']);
      $last_month = $last_date[0];
      $last_year = $last_date[1];
      $for_months = $_REQUEST['months'];

      // to get last date

      $hijari_month = date('F', $result['paid_till']);
      $hijari_year = date('Y');

      // FIX: to solve round months error in from date
      If ($last_month == 12) {
        $from_month = 1;
        $from_year = $last_year + 1;
      } else {
        $from_month = $last_month + 1;
        $from_year = $last_year;
      }

      $to_month = ($last_month + $for_months) % 12;
      $to_year = floor(($last_month + $for_months) / 12) + $last_year;

      // FIX: to solve round months error in upto date
      If ($to_month == 0) {
        $to_month = 12;
        $to_year = $to_year - 1;
      }
      $month_name = date("F", mktime(0, 0, 0, $to_month, 10));
      $expected_amount = $result['amount'] * $for_months;
      $arr_data = array("NEW_MONTH" => $month_name, "NEW_YEAR" => $to_year . ' H', "TOTAL" => number_format($expected_amount) . '/-', "EN_M_Y" => $to_month . '-' . $to_year);

      echo json_encode($arr_data);
    }
    break;
  //sabil vepaar end
  case 'get_type_list':
    $type = $_GET['type'];

    break;

  case 'SEARCHER':
    $clean = $database->clean_data($_GET);
    if (isset($clean['id']) && ($clean['id'] != '')) {
      $data = $cls_family->get_searched_data_by_id($clean['id']);
    }
    if (isset($clean['name']) && ($clean['name'] != '')) {
      $data = $cls_family->get_searched_data_by_name($clean['name']);
    }

    $ary_fly = array();
    if ($data) {
      foreach ($data as $family) {
        $receipt = $cls_receipt->get_last_receipt_no($clean['id']);
        $acct_open = $cls_family->get_family_open_date($family['FileNo']);
        if ($acct_open) {
          $open = HijriCalendar::GregorianToHijri($acct_open['timestamp']);
          $family['ACCT_OPEN'] = $open[1] . ' ' . HijriCalendar::monthName($open[0]) . ', ' . $open[2] . ' H';
        }

        $acct_close = $cls_family->get_family_close_date($family['FileNo']);
        if ($acct_close && $acct_close['close_date'] > 0) {
          $close = HijriCalendar::GregorianToHijri($acct_close['close_date']);
          $family['ACCT_CLOSE'] = $close[1] . ' ' . HijriCalendar::monthName($close[0]) . ', ' . $close[2] . ' H';
        } else {
          $family['ACCT_CLOSE'] = '---';
        }
        if ($receipt)
          $family['RECEIPT'] = $receipt['id'];
        else
          $family['RECEIPT'] = 'Not found.';

        $family['Full_Name'] = $cls_family->get_name($family['FileNo']);
        $ary_fly[$family['FileNo']] = $family;
      }
      echo json_encode($ary_fly);
    } else {
      echo '0'; //invalid
    }
    break;

  case 'get_selected_item_details':
    $result = $cls_product->get_all_ingredients($_GET['item_id']);
    echo json_encode($result);
    break;

  case 'FC_CODE':
    $code = $_POST['code'];
    $result = $cls_family->search_by_fc_code($code);
    if ($result)
      echo json_encode($result);
    else
      echo 0;
    break;

  case 'get_namelist_file':
    if ($_GET['sex'] == 'F')
      return include('includes/inc.female.php');
    else
      return include('includes/inc.male.php');
    break;

  case 'get_menu_name' :
    $dt = explode('-', $_GET['dt']);
    $start = mktime(0, 0, 0, $dt[1], $dt[2], $dt[0]);
    $end = mktime(23, 59, 59, $dt[1], $dt[2], $dt[0]);
    $dmenu = $cls_menu->get_all_daily_menu($start, $end);
    $ary_menu = array('ID' => $dmenu[0]['menu_id'], 'MENU' => $dmenu[0]['menu']);
    echo json_encode($ary_menu);
    break;

  case 'update_person_count':
    $tfn = $_GET['tfn_cnt'];
    $upd = $cls_family->update_person_count($tfn, $_GET['timestamp']);
    break;

  case 'del_inv_issue_item':
    $choice = $_POST['choice'];
    $date = trim($_POST['dt']);
    $person_count = (int) $_POST['person_cnt'];
    $menu_id = (int) $_POST['menu_id'];
    $dt = explode('-', $date);
    $start_time = mktime(0, 0, 0, $dt[1], $dt[2], $dt[0]);
    //echo $id . ' - ' . $date;
    if ($choice == 'INVENTORY') {
      $id = (int) $_POST['id'];
      if ($id && $date) {
        $ret = $cls_product->delete_estimated_inventory_issue($id, $date);
        if ($ret) {
          // get all the inv_issue items for this date
          $data = $cls_menu->get_cook_est_inv_issue_items($menu_id, $person_count, $start_time);
          foreach ($data as $key => $item) {
            if ($item['inv_issue']) {
              $bg_clr = ' class="alert alert-success"';
            } else {
              $bg_clr = '';
            }
            if (!$item['base_inv_issue'])
              $bg_clr = ' class="alert alert-warning"';
            $data[$key]['BG_COLOR'] = $bg_clr;
          }
        }
      }
    } else {
      $id = $_POST['id'];
      $ret = $cls_product->delete_estimated_direct_pur($id, $start_time);
      if ($ret) {
        $data = $cls_menu->get_cook_est_direct_issue_items($menu_id, $person_count, $start_time);
        foreach ($data as $key => $bill) {
          if ($bill['direct_issue']) {
            $bg_clr = ' class="alert alert-success"';
          } else {
            $bg_clr = '';
          }
          if (!$bill['base_inv_issue'])
            $bg_clr = ' class="alert alert-warning"';

          $data[$key]['BG_COLOR'] = $bg_clr;
        }
      }
    }
    echo json_encode($data);
    break;

  case 'del_base_issue_item':
    $choice = $_POST['choice'];
    $item_id = $_POST['id'];
    $menu_id = $_POST['menu_id'];
    $person_count = $_POST['person_count'];
    if ($choice == 'base_inventory') {
      $result = $cls_menu->delete_base_menu_item($item_id, $menu_id);
      $response = $cls_menu->get_all_base_inv_issue($menu_id, $person_count);
    } else {
      $result = $cls_menu->delete_base_menu_direct_item($item_id, $menu_id);
      $response = $cls_menu->get_all_base_direct_items($menu_id, $person_count);
    }
    echo $response;
    break;

  case 'del_menu':
    $menu_id = $_POST['mid'];
    $timestamp = $_POST['timestamp'];
    $month = $_POST['month'];
    $year = $_POST['year'];
    $delete = $cls_menu->delete_menu($menu_id, $timestamp);

    if ($delete) {
      $response = $cls_menu->get_daily_menu($month, $year);
      foreach ($response as $key => $menu) {
        $response[$key]['menu_name'] = $cls_menu->get_base_menu_name($menu['menu_id']);
        $response[$key]['greg_date'] = date('d M, Y D', $menu['timestamp']);
        $hdate = HijriCalendar::GregorianToHijri($menu['timestamp']);
        $response[$key]['Hijri_date'] = "$hdate[1] " . HijriCalendar::monthName($hdate[0]) . ", $hdate[2]";
        $response[$key]['Delete'] = $menu['timestamp'] > time() ? TRUE : FALSE;
        $response[$key]['Date'] = date('Y-m-d', $menu['timestamp']);
      }
      echo json_encode($response);
    } else {
      $response = 'Errors encountered while deleting requested menu';
      echo $response;
    }
    break;

  case 'del_base_category':
    $cid = $_POST['cid'];
    $delete = $cls_menu->delete_category($cid);
    if($delete) echo TRUE;
    else echo FALSE;
//    if ($delete) {
//      $response = $cls_menu->get_all_category();
//    } else {
//      $response = 'Errors encountered while deleting category!';
//    }
//    echo json_encode($response);
    break;

  case 'del_inventory_category':
    $cid = $_POST['cid'];
    $delete = $cls_menu->delete_inventory_category($cid);
    if ($delete) {
      $response = $cls_menu->get_inventory_category();
    } else {
      $response = 'Errors encountered while deleting category!';
    }
    echo json_encode($response);
    break;

  case 'get_base_menu_from_cat':
    $cid = $_POST['cid'];
    $response = $cls_menu->get_base_menu_from_cid($cid);
    if ($response)
      echo json_encode($response);
    else
      '0'; //no menus
    break;

  case 'add_base_menu_category':
    $cat = $_POST['category'];
    $add_category = $cls_menu->add_base_menu_category(ucfirst($cat));
    if ($add_category) {
      $categories = $cls_menu->get_all_category();
      $response = json_encode($categories);
    } else
      $response = 0; //error

    echo $response;
    break;

  case 'get_yearly_takhmeen_record':
    $thali_id = $_POST['fId'];
    $year = $_POST['year'];
    $records = $cls_family->get_takhmeen_record($thali_id, FALSE, "AND `year` = '$year'");
    $dates = $cls_family->get_takhmeen_dates(FALSE, $thali_id);
    $div_ele = '';
    if ($dates) {
      foreach ($dates as $key => $data) {
        $date_spl = explode('-', $data['date']);
        $time = mktime(0, 0, 0, $date_spl[1], $date_spl[2], $date_spl[0]);
        $hijri_time = HijriCalendar::GregorianToHijri($time);
        //$dates[$key]['date'] = implode('-', HijriCalendar::GregorianToHijri($time));

        $days = '';
        for ($i = 1; $i < 32; $i++) {
          if ($i == $hijri_time[1])
            $sel_day = 'selected';
          else
            $sel_day = '';
          $days .= '<option value="' . $i . '" ' . $sel_day . '>' . $i . '</option>';
        }
        $months = '';
        for ($i = 1; $i < 13; $i++) {
          if ($i == $hijri_time[0])
            $sel_month = 'selected';
          else
            $sel_month = '';
          $months .= '<option value="' . $i . '"' . $sel_month . '>' . HijriCalendar::monthName($i) . '</option>';
        }

        $div_ele .= <<<HTML
            <div class="form-group">
          <div class="col-md-3">
            <select name="pay_day[]" id="pay_day" class="form-control">
              <option value="0">Day</option>
              {$days}
            </select>
          </div>
          <div class="col-md-3">
            <select name="pay_month[]" id="pay_month" class="form-control">
              <option value="0">Month</option>
              {$months}
            </select>
          </div>
          <div class="col-md-3">
            <input type="text" name="year[]" placeholder="Year" value="{$hijri_time[2]}" class="form-control">
          </div>
          <div class="col-md-3">
            <input type="text" name="niyaz_qty[]" placeholder="Qty" value="{$data['niyaz_qty']}" class="form-control">
          </div>
        </div>
HTML;
      }
    }
    if ($records)
      echo json_encode(array('RECORDS' => $records, 'DATES' => $div_ele));
    else
      $records = FALSE;
    break;

  case 'takhmeen_hub_receipt':
    $thali_id = $_POST['fId'];
    $year = $_POST['year'];
    $last_yr = ($year - 1).'-'.(substr($year, -2) - 1);
    $five_rcpt = $cls_family->get_past_five_transactions($thali_id, $year);
    $takh_amount = $cls_family->get_takhmeen_record($thali_id, FALSE, "AND `year` = '$year'");
    $last_yr_takh_amount = $cls_family->get_takhmeen_record($thali_id, FALSE, "AND `year` = '$last_yr'");
    $paid_amount = $cls_family->get_paid_amount($thali_id, $year);
    $last_yr_paid_amount = $cls_family->get_paid_amount($thali_id, $last_yr);
    $pending_amount = $takh_amount[0]['amount'] - $paid_amount;
    $pending_amount = ($pending_amount > 0) ? 'Rs. ' . number_format($pending_amount, 2) : 'CLEAR';
    $last_yr_pending_amount = $last_yr_takh_amount[0]['amount'] - $last_yr_paid_amount;
    $last_yr_pending_amount = ($last_yr_pending_amount > 0) ? 'Rs. ' . number_format($last_yr_pending_amount, 2) : 'CLEAR';
    $rtn_array = array('TAKHMEEN_AMOUNT' => 'Rs. ' . number_format($takh_amount[0]['amount'], 2), 'PAID_AMOUNT' => 'Rs. ' . number_format($paid_amount, 2), 'PENDING_AMOUNT' => $pending_amount, 'LAST_YEAR_PENDING_AMOUNT' => $last_yr_pending_amount, 'FIVE_RECEIPT' => json_encode($five_rcpt));
    echo json_encode($rtn_array);
    break;

  case 'TAKHMEEN_CALCULATION':
    $thali_id = strtoupper($_POST['thali_id']);
    $months = (int) $_POST['months'];
    $year = $_POST['year'];
    $check = $cls_family->check_thali_ID_exist($thali_id);
    if ($check == 1) {
      $takh_amount = $cls_family->get_takhmeen_record($thali_id, FALSE, " AND `year` LIKE '$year'");
      if ($takh_amount) {
        $kisht = $cls_user->get_general_settings();
        $kisht = $kisht[0]['default_kisht'];
        $rupess = number_format((($takh_amount[0]['amount'] / $kisht) * $months), 2);
        $form = <<<FORM
              <form method='post' role='form' class='form-inline'>
                <div class='form-group'>
                <label class='control-label col-md-4'>Total amount Rs:</label>
                  <div class='col-md-4'>
                    <input type='text' name='takhmeen_amount' class='form-control' value='{$rupess}'>
                  </div>
                <input type='hidden' name='takhmeen_thali' value='{$thali_id}'>
                <input type='hidden' name='takhmeen_year' value='{$year}'>
                  <div class='col-md-4'>
                    <input type='submit' name='update_takh_amt' value='Update Takhmeen' class='btn btn-success'>
                  </div>
                </div>
              </form>
FORM;
      }
      $response = $takh_amount ? $form : 'Takhmeen amount not specified';
    } else {
      $response = 'Invalid thali id';
    }
    echo $response;
    break;

}
?>
