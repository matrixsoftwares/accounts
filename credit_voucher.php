<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.receipt.php");
$cls_receipt = new Mtx_Receipt();
$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
require_once 'daily_cash_entry.php';
$pg_link = 'credit_voucher';
$user_id = $_SESSION[USER_ID];
if(isset($_POST['credit'])){
  $cmd = 'credit';
  $result = $cls_receipt->add_voucher($cmd, $_POST['name'], $_POST['heads'], $_POST['description'], $_POST['amount'], $_POST['type'], $_POST['bankname'], $_POST['cheque'], $_POST['dob'], $user_id);
  if($result){
    $_SESSION[SUCCESS_MESSAGE] = 'Credit Voucher added successfully';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while processing Credit Voucher';
  }
}
$heads = $cls_receipt->get_account_heads();
$title = 'Credit Voucher';
$active_page = 'account';

require_once 'includes/header.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Credit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12">&nbsp;</div>

      <!-- Center Bar -->
      <div class="col-md-12">
        <form method="post" role="form" class="form-horizontal">
            <div class="form-group">
              <label class="control-label col-md-3">Name</label>
              <div class="col-md-4">
                <input type="text" name="name" id="name" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Account Heads</label>
              <div class="col-md-4">
                <select class="form-control" name="heads" id="heads">
                  <option>--Select One--</option>
                  <?php if($heads){foreach($heads as $head){?>
                  <option value="<?php echo $head['head']?>"><?php echo $head['head'];?></option>
                  <?php }} ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Description</label>
              <div class="col-md-4">
                <input type="text" name="description" id="description" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Amount</label>
              <div class="col-md-4">
                <input type="text" name="amount" id="amount" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Type</label>
              <div class="col-md-4">
                <input type="radio" id="radio" name="type" value="cash" checked >&nbsp;Cash&emsp;
                <input type="radio" id="radio" name="type" value="cheque">&nbsp;Cheque&emsp;
                <input type="radio" id="radio" name="type" value="neft">&nbsp;NEFT
              </div>
            </div>
            <div id="cheque_detail" style="display: none">
              <div></div>
              <div class="form-group" >
                <label class="control-label col-md-3">Bank Name</label>
                <div class="col-md-4">
                  <input type="text" name="bankname" id="bankname" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3" id="cheque_type">Cheque No.</label>
                <div class="col-md-4">
                  <input type="text" name="cheque" id="cheque" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Date</label>
                <div class="col-md-4">
                    <input type="date" name="dob" class="form-control" id="dob" placeholder="">
                </div>
              </div>
            </div>
            <div class="form-group col-md-offset-4">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-3">
                <button type="submit" class="btn btn-success" name="credit" id="credit">Add</button>
              </div>
            </div>
        </form>
      </div>
      <!-- /Center Bar -->
    </div>
    <!-- /Content -->
    </section>
  </div>
<script>
  //for radio buttons
    $("input:radio[name=type]").click(function() {
    var radio = $(this).val();
    switch(radio) {
      case 'cheque':
        $('#cheque_detail').show();
        $('#cheque_type').text('Cheque No.');
        break;
      case 'neft':
        $('#cheque_detail').show();
        $('#cheque_type').text('NEFT No.');
        break;
      default:
        $('#cheque_detail').hide();
    }
  });
  $('#credit').click(function(){
    var name = $('#name').val();
    var heads = $('#heads').val();
    var desc = $('#description').val();
    var amt = $('#amount').val();
    var errors = '';
    var validate = true;
    if(name == ''){
      errors += 'Please enter name\n';
      validate = false;
    }
    if(heads == '0'){
      errors += 'Please select account heads\n';
      validate = false;
    }
    if(desc == ''){
      errors += 'Please enter description\n';
      validate = false;
    }
    if(amt == ''){
      errors += 'Please enter an amount\n';
      validate = false;
    }
    if(validate == false){
      alert(errors);
      return validate;
    }
  });
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>