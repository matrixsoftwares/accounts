<?php
$ary_pages = array(0, 1, 44);
if(in_array($page_number, $ary_pages)) {
  $result = $cls_receipt->daily_cash_entry_exist();
  if(!$result) {
    $_SESSION[ERROR_MESSAGE] = 'Please enter daily cash to make a payment...';
    header('Location: daily_cash.php');
    exit();
  }
}
?>