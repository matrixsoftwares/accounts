<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.product.php');
$cls_product = new Mtx_product();

$result = $cls_product->get_inventory_latest_price_list();

$title = "Latest Pricelist of Inventory";
$active_page = "menu";

require_once 'includes/header.php';

$page_number = INVENTORY_REPORTS;
require_once 'page_rights.php';
?>

<link href="asset/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="asset/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="asset/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>

<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Maedat</a></li>
        <li><a href="#">Latest Price List</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="pull-right">
            <a href="print_inventory_latest_pricelist.php" target="_blank" class="btn btn-primary btn-xs">Print</a>
          </div>
          <br><br>
          <div class="box">
            <div class="box-body">
              <?php
                include('includes/inc.inventory_latest_price_list.php');
              ?>
            </div>
          </div>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript">
  $(function () {
    $("#example1").DataTable();
  });
</script>
<?php
include('includes/footer.php');
?>