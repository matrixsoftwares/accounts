<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.user.php');
$cls_user = new Mtx_User();

if(isset($_GET['cmd']) && $_GET['cmd'] == 'update') {
  $query = "UPDATE `users` SET `active` = '$_GET[status]' WHERE `id` = '$_GET[id]'";
  $result = $database->query($query);
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'Record updated successfully.';
    header('Location: user_manage.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Sorry! try again.';
    header('Location: user_manage.php');
    exit();
  }
}
if (isset($_POST['user_update'])) {
  $result = $cls_user->add_user($_POST['name'], $_POST['email'], $_POST['mobile'],$_POST['user_id'],$_POST['password']);
  if ($result)
    $_SESSION[SUCCESS_MESSAGE] = 'User has been added successfully.';
  else
    $_SESSION[ERROR_MESSAGE] = 'Sorry! try again.';
}
$users = $cls_user->get_all_user();
$title = 'Add user';
$active_page = 'settings';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="pull-right">
            <a href="user_add.php" class="btn btn-sm btn-success">Add User</a>
          </div>
          <div class="form-group">&nbsp;</div>

          <form method="post" role="form" class="form-horizontal">
            <table class="table table-bordered table-condensed">
              <thead>
                <tr>
                  <th>Sr No.</th>
                  <th>Full Name</th>
                  <th>Email ID</th>
                  <th>Mobile</th>
                  <th>User ID</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php if($users){$i = 1; foreach($users as $user){?>
                <tr <?php echo ($user['active'] == 0) ? 'class="danger"' : ''; ?>>
                  <td><?php echo $i++;?></td>
                  <td><?php echo $user['full_name'];?></td>
                  <td><?php echo $user['email'];?></td>
                  <td><?php echo $user['mobile'];?></td>
                  <td><a href="user_update.php?userid=<?php echo $user['username'];?>"><?php echo $user['username'];?></a></td>
                  <td><input type="checkbox" name="active[]" id="action<?php echo $user['id']; ?>" onclick="check(<?php echo $user['id']; ?>)" <?php echo ($user['active'] == 1) ? 'checked' : ''; ?>></td>
                </tr>
                <?php } } else { ?>
                <tr>
                  <td colspan="3">No users to show.</td>
                </tr>
                <?php }?>
              </tbody>
            </table>
          </form>
        </div>
        <!-- /Center Bar -->
        <script>
        function check(val){
          var doc = document.getElementById('action'+val).checked;

          if(doc == true) doc = 1;
          else doc = 0;
          window.location.href='user_manage.php?id='+val+'&status='+doc+'&cmd=update';
        }
          $('#user_add').click(function() {
            var name = $('#name').val();
            var email = $('#email').val();
            var mobile = $('#mobile').val();
            var user_id = $('#user_id').val();
            var pwd = $('#password').val();
            var error = 'Following error(s) are occurred\n\n';
            var validate = true;
            if (name == '')
            {
              error += 'Please enter User name\n';
              validate = false;
            }
            if (email == '')
            {
              error += 'Please enter email\n';
              validate = false;
            }
            if (mobile == '')
            {
              error += 'Please enter mobile\n';
              validate = false;
            }
            if (user_id == '')
            {
              error += 'Please enter your desired user id\n';
              validate = false;
            }
            if (pwd == '')
            {
              error += 'Please enter password\n';
              validate = false;
            }
            if (validate == false)
            {
              alert(error);
              return validate;
            }
          });
          $('#FileNo').keyup(function() {
            var FileNo = $(this).val().toUpperCase();
            $(this).val(FileNo);
          });

          $('#user_id').change(function() {
            var userid = $('#user_id').val();
            $.ajax({
              url: "ajax.php",
              type: "GET",
              data: 'userid=' + userid + '&cmd=check_user_exist',
              success: function(data)
              {
                if(data == '1'){
                  alert('User Id already exists');
                  return false;
                } else {
                  return true;
                }
              }
            });
          });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>