<?php
include 'session.php';
$pg_link = 'add_bill';
require_once('classes/class.database.php');
require_once('classes/class.billbook.php');
require_once('classes/class.product.php');
require_once('classes/class.receipt.php');
require_once('classes/class.user.php');
$user_id = $_SESSION[USER_ID];
$cls_billbook = new Mtx_BillBook();
$cls_receipt = new Mtx_Receipt();
$cls_product = new Mtx_Product();
$cls_user = new Mtx_User();

if ($_GET['cmd'] == 'show_bills') {
  $bills = $cls_billbook->get_all_account_bill($_GET['bid']);
  $details = $cls_billbook->get_bills_from_bill_details($bills['id']);
} else {
  $bills = $cls_billbook->get_direct_bills($_GET['bid']);
  $details = $cls_billbook->get_direct_purchase_bills_from_bill_details($_GET['bid']);
}
$heads = $cls_receipt->get_account_heads();
$count = count($details);
$setting = $cls_user->get_general_settings();
$db_tanzeem_name = $setting[0]['tanzeem_name'];
$title = 'Bill Details';
$active_page = 'account';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print Bill Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
    <style type="text/css">
      @media all {
        body{font-size: 14px; }
        .page-break  { display: none; }
      }

      @media print {
        .page-break  { display: block; page-break-before: always; }
      }
    </style>
  </head>
  <body onload="window.print();">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

  <!-- Center Bar -->
  <div class="col-md-8">
    <div style="text-align: center;" class="col-md-12">
          <img src="includes/logo.jpg" width="200px">
        </div>
        <div class="col-md-12" style="text-align: center;">
          <h2><strong><?php echo $db_tanzeem_name; ?></strong></h2>
        </div>

        <div class="clearfix"></div>
      <?php
      if ($bills['BillNo'] != 0)
        $billNo = $bills['BillNo'];
      else
        $billNo = $bills['id'];
      ?>
      <div class="col-md-6 pull-left">
        <p class="form-control-static"><strong class="col-md-6">Bill No. </strong><?php echo $billNo; ?></p>
      </div>

      <div class="col-md-6 pull-right">
          <p class="form-control-static"><strong class="col-md-6">Account Heads: </strong><?php echo $bills['acct_heads']; ?></p>
      </div>
        <div class="clearfix"></div>
      <?php if($_GET['cmd'] == 'show_direct_bills') { ?>
      <div class="col-md-6 pull-left">
          <p class="form-control-static"><strong class="col-md-6">Bill Date: </strong><?php echo date('Y-m-d', $bills['bill_date']); ?></p>
      </div>

      <div class="col-md-6 pull-right">
          <p class="form-control-static"><strong class="col-md-6">Menu Date: </strong><?php echo $bills['menu_date']; ?></p>
      </div>
      <?php } ?>
        <div class="clearfix"></div>
      <div class="col-md-6 pull-left">
          <p class="form-control-static"><strong class="col-md-6">Shop Name: </strong><?php echo $bills['ShopName']; ?></p>
      </div>
    <div class="clearfix"></div>
    <style>
      ul li {
        width: 100px;
      }
      ul li:first-child {
        margin-left: 50px;
      }
    </style>
    <div class="col-md-12">
      
      <ul style="display: inline-flex; list-style-type: none;">
        <li>Item Name</li>
        <li>Quantity</li>
        <li>Unit Price</li>
      </ul>
    </div>
      <?php
      $i = 0;
      foreach ($details as $detail) {
        ?>
        <div class="col-md-12 pull-left">
          <?php if ($i == 0)
          echo '<span style="margin-top: 8px;" class="col-md-2">Items</span>';
        else
          echo '<span class="form-control-static">&nbsp;</span>';
        ?>
          <div class="col-md-4">
            <p class="form-control-static"><?php echo ucwords($detail['item_name']); ?></p>
          </div>
          <div class="col-md-2">
            <p class="form-control-static" id="quantity<?php echo $i; ?>"><?php echo $detail['quantity']; ?></p>
          </div>
          <div class="col-md-2">
            <p class="form-control-static" id="unit<?php echo $i; ?>"><?php echo $detail['unit']; ?></p>
          </div>
          <div class="col-md-2">
            <p class="form-control-static"><?php echo number_format($detail['price'], 2); ?></p>
          </div>
        </div>
  <?php $i++;
}
?>

    <div class="clearfix"></div>
      <div class="col-md-6 pull-left">
        <p class="form-control-static"><strong class="col-md-6">VAT Charges: </strong><?php echo $bills['VAT']; ?></p>
      </div>

      <div class="col-md-6 pull-right">
          <p class="form-control-static"><strong class="col-md-6">Discount: </strong><?php echo number_format($bills['Discount'], 2); ?></p>
      </div>

      <div class="col-md-6 pull-left">
        <p class="form-control-static"><strong class="col-md-6">Other Charges: </strong><?php echo number_format($bills['other_charges'], 2); ?></p>
      </div>

      <div class="col-md-6 pull-right">
          <p class="form-control-static" id="grand_amount"><strong class="col-md-6">Net Amount: </strong><?php echo number_format($bills['amount'], 2); ?></p>
      </div>

      <div class="col-md-6 pull-left">
          <p class="form-control-static"><strong class="col-md-6">Description: </strong><?php echo $bills['description']; ?></p>
      </div>

      <div class="col-md-6 pull-right">
          <p class="form-control-static"><strong class="col-md-6">Contact Person: </strong><?php echo $bills['contact_person']; ?></p>
      </div>

      <div class="col-md-6 pull-left">
          <p class="form-control-static"><strong class="col-md-6">Make Payment: </strong><?php echo ($bills['paid'] == 1) ? 'Yes' : 'No'; ?></p>
      </div>

      <?php
      if ($bills['paid'] != 0) {
        if ($bills['payment_type'] == 'cash' || $bills['payment_type'] == '')
          $type = 'Cash';
        if ($bills['payment_type'] == 'cheque')
          $type = 'Cheque';
        if ($bills['payment_type'] == 'neft')
          $type = 'Neft';
      } else $type = '';
      ?>
      <div class="col-md-6 pull-right" style="display: <?php echo ($bills['paid'] == 1) ? 'block' : 'none'; ?>">
          <p class="form-control-static"><strong class="col-md-6">Payment Mode: </strong><?php echo $type; ?></p>
      </div>
    <div class="col-md-6 pull-left" style="display: <?php if (isset($type) && ($type == 'Cheque' || $type == 'Neft'))
             echo 'block';
           else
             echo 'none';
      ?>">
        <div></div>

        <div class="col-md-6 pull-right">
            <p class="form-control-static"><strong class="col-md-6">Bank Name: </strong><?php echo $bills['bankname']; ?></p>
        </div>

        <div class="col-md-6 pull-left">
            <p class="form-control-static"><strong class="col-md-6">Cheque No.: </strong><?php echo $bills['cheque_no']; ?></p>
        </div>

        <div class="col-md-6 pull-right">
            <p class="form-control-static"><strong class="col-md-6">Date: </strong><?php echo $bills['cheque_date']; ?></p>
        </div>
      </div>
  </div>
  <!-- /Center Bar -->

      <!-- Right Bar -->

      <!-- /Right Bar -->

    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
