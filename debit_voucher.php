<?php
include('session.php');
$pg_link = 'debit_voucher';
require_once("classes/class.database.php");
require_once("classes/class.receipt.php");
require_once("classes/class.billbook.php");
$cls_receipt = new Mtx_Receipt();
$cls_billbook = new Mtx_BillBook();
$user_id = $_SESSION[USER_ID];
$post = FALSE;
if (isset($_POST['debit'])) {
  $post = TRUE;
  // get the form id
  $form_id = $_POST['form_id'];
  //process the form if not already processed
  $query = "SELECT `return_id` FROM `forms_processed` where `form_id` LIKE '$form_id'";
  $result = $database->query_fetch_full_result($query);

  if (!$result) {
    
    $cmd = 'debit';
    $result = $cls_receipt->add_voucher($cmd, $_POST['shop'], $_POST['heads'], $_POST['description'], $_POST['amount'], $_POST['type'], $_POST['bankname'], $_POST['cheque'], $_POST['dob'], $user_id, $_POST['form_id'], $_POST['contact'], $_POST['ref_no']);
    if ($result != '') {
      // insert the row into the forms processed table
      $time = time();
      $query = "INSERT INTO forms_processed VALUES ('$form_id','$result','$time')";
      $database->query($query);

      $_SESSION[SUCCESS_MESSAGE] = 'Debit voucher added successfully.<br>To print the voucher please click on print button to proceed...';
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Errors encountered while processing the debit voucher!';
    }
  } else {
    return $result[0]['return_id'];
  }
}

$form_id = '';
for ($i = 0; $i < 6; $i++) {
  $d = rand(1, 30) % 2;
  $form_id .= $d ? chr(rand(65, 90)) : chr(rand(48, 57));
}

$form_id = md5(time() . $form_id);
$shops = $cls_billbook->get_shops();
$heads = $cls_receipt->get_account_heads();
$title = 'Debit Voucher';
$active_page = 'account';


require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Debit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div class="form-group">
              <label class="control-label col-md-3">Shop</label>
              <div class="col-md-4">
                <select name="shop" id="shop" class="form-control">
                  <option value="">--Select Shop--</option>
                  <?php foreach ($shops as $shop) { ?>
                    <option value="<?php echo $shop['id']; ?>"><?php echo $shop['ShopName']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Account Heads</label>
              <div class="col-md-4">
                <select class="form-control" name="heads" id="heads">
                  <option value="">--Select One--</option>
                  <?php
                  if ($heads) {
                    foreach ($heads as $head) {
                      ?>
                      <option value="<?php echo $head['head'] ?>"><?php echo $head['head']; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Description</label>
              <div class="col-md-4">
                <input type="text" name="description" id="description" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Amount</label>
              <div class="col-md-4">
                <input type="text" name="amount" id="amount" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Contact Person</label>
              <div class="col-md-4">
                <input type="text" name="contact" id="contact" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Ref. No.</label>
              <div class="col-md-4">
                <input type="text" id="ref_no" name="ref_no" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Type</label>
              <div class="col-md-4">
                <input type="radio" id="radio" name="type" value="cash" checked >&nbsp;Cash&emsp;
                <input type="radio" id="radio" name="type" value="cheque">&nbsp;Cheque&emsp;
                <input type="radio" id="radio" name="type" value="neft">&nbsp;NEFT
              </div>
            </div>
            <div id="cheque_detail" style="display: none">
              <div></div>
              <div class="form-group" >
                <label class="control-label col-md-3">Bank Name</label>
                <div class="col-md-4">
                  <input type="text" name="bankname" id="bankname" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3" id="cheque_type">Cheque No.</label>
                <div class="col-md-4">
                  <input type="text" name="cheque" id="cheque" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Date</label>
                <div class="col-md-4">
                  <input type="date" name="dob" class="form-control" id="dob" placeholder="">
                </div>
              </div>
            </div>
            <div class="form-group col-md-offset-4">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-3">
                <input type="hidden" name="form_id" value="<?php echo $form_id; ?>">
                <button type="submit" class="btn btn-success" name="debit" id="debit">Add</button>
                <a class="btn btn-primary <?php echo !$post ? 'disabled' : ''; ?>" id="print">Print</a>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /Center Bar -->
    </section>
  </div>
  <!-- /Content -->
    
<script>
  $('#print').click(function() {
    var vid = '<?php echo $result; ?>';
    window.open('printDebit.php?vid='+vid);
  });
  //for radio buttons
  $("input:radio[name=type]").click(function() {
    var radio = $(this).val();
    switch (radio) {
      case 'cheque':
        $('#cheque_detail').show();
        $('#cheque_type').text('Cheque No.');
        break;
      case 'neft':
        $('#cheque_detail').show();
        $('#cheque_type').text('NEFT No.');
        break;
      default:
        $('#cheque_detail').hide();
    }
  });
  $('#debit').click(function() {
    var name = $('#name').val();
    var heads = $('#heads').val();
    var desc = $('#description').val();
    var amt = $('#amount').val();
    var errors = '';
    var validate = true;
    if (name == '') {
      errors += 'Please enter name\n';
      validate = false;
    }
    if (heads == '') {
      errors += 'Please select account heads\n';
      validate = false;
    }
    if (desc == '') {
      errors += 'Please enter description\n';
      validate = false;
    }
    if (amt == '') {
      errors += 'Please enter an amount\n';
      validate = false;
    }
    if (validate == false) {
      alert(errors);
      return validate;
    }
  });
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>
