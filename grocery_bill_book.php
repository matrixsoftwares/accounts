<?php
include 'session.php';
$pg_link = 'grocery_bill_book';
require_once('classes/class.database.php');
require_once('classes/class.billbook.php');
$cls_billbook = new Mtx_BillBook();

$bills = $cls_billbook->get_all_account_bill();

$title = 'List of Bills';
$active_page = 'account';


require_once 'includes/header.php';

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Bill Books</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <table class="table table-hover table-condensed table-bordered">
            <thead>
            <th>Bill No</th>
            <th>Shop Name</th>
            <th>Type of Payment</th>
            <th class="text-right">Amount</th>
            <th>Date</th>
            </thead>
            <tbody>
              <?php
              if (is_array($bills)) {
                $i = 1;
                foreach ($bills as $bill) {
                  if($bill['cancel'] == 1) $cls = 'class="alert-danger"';
                  elseif($bill['paid'] == 0) $cls = 'class="alert-warning"';
                  else $cls = '';
                  ?>
                  <tr <?php echo $cls; ?>>
                    <td><a href="upd_bill_details.php?cmd=show_bills&bid=<?php echo $bill['id']; ?>" target="_blank"><?php echo $bill['BillNo']; ?></a></td>
                    <td><?php echo $bill['ShopName'] ?></td>
                    <td><?php echo $bill['payment_type'] ?></td>
                    <td class="text-right"><?php echo number_format($bill['amount'], 2); ?></td>
                    <td><?php echo date('d F, Y', $bill['timestamp']); ?></td>
                  </tr>
                <?php }
              } else {
                ?>
                <tr>
                  <td colspan="5" class="alert-danger">No records found.</td>
                </tr>
      <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>